# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 11:11:22 2018

@author: sdavila
"""


from time import time
#from PLCTools import output_bytes

from embeddedstructs import Struct,Type,Format
from mcp_sunpower_h import  tMcpMsgHdr,tMcpPollCmdSendPollDataTlv,cL2DomainAddrLen,tMcpPollCmdTlv,\
                    cMcpPollDiscover,cMcpPollFlagDeviceInfo,cMcpMsgIdPollCmdTlv,\
                    cMcpProtocolVersionSeven,tMcpAssociationTlv,tMcpAssociationLoadTlv,\
                    tMcpAssociationLoadData,cMcpAssocCmdLoad,cMcpMsgIdAssociation,\
                    tMcpPollRspTlv,tMcpPollRspDevInfoTlv,cMcpMsgIdPollRspTlv,cMcpPollDataDeviceInfo,\
                    tMcpAssociationSquelchTlv,tMcpAssociationSquelchData,cMcpAssocCmdSquelch
from mcp_v6_h import  app_msg_header,cMcpMsgIdMulticastContainer,cMcpMsgIdPollContainer,\
            mcp_v6_poll_response_message_t,mcp_v6_poll_message_t,MCP_V6_POLL_MESSAGE_BODY_SIZE,\
            MCP_V6_POLL_RESPONSE_MESSAGE_BODY_SIZE
from mde_ids_h import MDE_header_t,mde_id_FW_upgrade_start,mde_id_FW_upgrade_write_page,\
        mde_id_get_firmware_information,mde_id_read_telemetry_level1,mde_id_FW_upgrade_finish_and_check,\
        mde_id_FW_write_metadata,mde_id_FW_read_metadata,mde_id_DMIR_upgrade_start,mde_id_ECDC_payload
from mcp_mde_device_h import MDE_FW_information_t
from mcp_mde_telemetry_h import MDE_data_telemetry_level1_t,MDE_data_telemetry_level2_t
from mcp_mde_image_h import mde_payload_EAC_request_image_start_t,\
        MDE_FLASH_UPGRADE_START_ERASE_AREA,MDE_FLASH_UPGRADE_START_NO_ERASE, \
        mde_payload_EAC_request_write_image_hunk_t, MDE_IMAGE_PAGE_HUNK_BYTES, \
        mde_payload_PLD_response_to_image_check_t,MDE_IMAGE_CONTRACT_MAX_BYTES, \
        mde_payload_EAC_request_write_metadata_t,mde_payload_EAC_request_FW_image_meta_t,\
        mde_payload_PLD_response_read_ANY_metadata_t,\
        MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE,MDE_BANK_SELECT_FIRMWARE_BANK_ACTIVE_NOW,\
        mde_payload_EAC_request_model_start_t,DM_AREA_TYPE_INIT_RECORD
        
from screv_h import sSC_revision_v2,sSCCmdUpgrade_t
#from plc_poll_c import tMCPV6Hdr,tTlvHdr,tTLVL1TelemetryPkt
#from enphase_command_c import tPollCmdRequestV6
                            

ENPH_MODULE_ADDRESS_MASK   =         0x3FFF
ENPH_ADDRESSING_MODE_MASK    =       0xC000
ENPH_ADDRESS_MODE_UNICAST     =      0x0000 # L2 directed
ENPH_ADDRESS_MODE_BCAST_DIRECTED  =  0x4000 # L2 McastMod
ENPH_ADDRESS_MODE_BCAST_MODZERO =    0x8000 #L2 BcastCoord
ENPH_ADDRESS_MODE_BCAST_DOMAINZERO = 0xC000 # L2 BcastAll

eSC_INTF_UPGRD_DATA             = 5
eSC_INTF_UPGRD_COMMIT           = 6
SPM_FW_PACKET_SIZE = 128


Directed = 0
McastMod = 1
BcastCoord = 2
BcastAll = 3

class tMcpAddressHdr(Struct):
	_format = Format.BigEndian
	domainAddr = Type.UnsignedByte[cL2DomainAddrLen]
	modAddr = Type.UnsignedShort
    
    
def hex_digit(n):
    if n > 9:
        return chr(ord('A') + n-10)
    return chr(ord('0') + n)

def hex2digit(c):
    if c.isdigit():
        return ord(c) - ord('0')
    x = ord(c.upper())
    if (x < ord('A')) or (x > ord('F')):
        raise Exception("Invalid HEX character: "+str(c))
    
    return x - ord('A') + 10

# bytes to BCD string
def unpack_bcd(barray):
    outs = ''
    for c in barray:
        s1 = hex_digit((c & 0xF0)>>4)
        s2 =  hex_digit(c & 0x0F)
        outs += s1
        outs += s2
    return outs

#BCD string to bytes
#    pack_bcd(pMcpAddr.domainAddr,domainAddr,0,len(domainAddr))

def pack_bcd(arr,ss,offset,count):
    i = 0
    offset *= 2
    count *= 2
    while True:
        if offset+i+1 >= len(ss):  # are we out of string?
            return
        if i >= count:
            return
        arr[int(i/2)] = hex2digit(ss[offset+i]) << 4 | hex2digit(ss[offset+i+1])
        i += 2

def unpack_chars(barray):
    outs = ''
    for c in barray:
        if c == 0:
            return outs
        outs += chr(c)
    return outs

def unpack_partnum(pn_struct):
    bcd = unpack_bcd(pn_struct.idb)
    
    pn = bcd[:3]+'-'+bcd[3:]+'-r'+str(pn_struct.manuRev)
    if (pn_struct.dMajor != 0) or (pn_struct.dMinor != 0) or (pn_struct.dMaint != 0):
        pn += "-v"+str(pn_struct.dMajor)+"."+str(pn_struct.dMinor)+"."+str(pn_struct.dMaint)
    
    return pn

def unpack_millijoules(arrayu16):
    mj = 0
    for v in arrayu16:
        mj = (mj << 16) + v
    
    return mj


def set_array_bits(array,offsets):
    for offset in offsets:
        byte_selected = int((offset)/8)
        bit_selected = 1 << (offset % 8)
        mask = 0xFF & (~bit_selected)
        array[byte_selected] = 0xFF & ((array[byte_selected] & mask) | bit_selected)
        

def get_next_missing_hunk(missing_hunks,curr_seg): # Starts at -1
    
    if missing_hunks is None or type(missing_hunks) is not list:
        return -1
    
    while True:
        curr_seg += 1
        byte_offset = int(curr_seg/8)
        if byte_offset >= len(missing_hunks):
            return -1 # end of the road
        bit_offset = curr_seg % 8
        if missing_hunks[byte_offset] & (1 << bit_offset) != 0:
            return curr_seg

#/*
#* Enphase message checksum code
#*/
#static uint16_t
def mcpUtilFletcher8BitCsum(byte_array,arr_len=None):
    a = 0
    b = 0
    i = 0
    
    if arr_len is None:
        arr_len = len(byte_array)
    
    for c in byte_array:
        if i >= arr_len:
            break
        a += byte_array[i]
        b += a
        i += 1
        
    return ((a & 0xFF) << 8) + (b & 0xFF)

##static uint16_t
def mcpUtilMcpMsgCsum(byte_array):
    s = tMcpMsgHdr()
    s = tMcpMsgHdr(byte_array[:len(s.encode())])
    return mcpUtilFletcher8BitCsum(byte_array[2:],s.msgLen-2)

#/* This formats a search/discover/idall packet All unassociated, unsquelched PCUs send their devInfo in a ~3second (up to 24 devices) window
#* with some automatic collision avoidance
#*  Assumes a header to the E_ASIC of tMcpAddressHdr that is used to format the Enphase L2 header */
#int32_t enphase_pack_poll_command_search(uint8_t *packet)
    
def enphase_pack_poll_command_search():
    pMcpAddr = tMcpAddressHdr()
    pMcpAddr._format = Format.BigEndian
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pMcpPollCmd = tMcpPollCmdTlv()
    pMcpPollCmd._format = Format.BigEndian
    pMcpPollCmdSendPollDataTlv = tMcpPollCmdSendPollDataTlv()
    pMcpPollCmdSendPollDataTlv._format = Format.BigEndian

    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)
    
    pMcpPollCmd.msgTime = int(time())
    pMcpPollCmd.pollReqFlags = cMcpPollFlagDeviceInfo
    pMcpPollCmd.sequenceNumber = 1
    pMcpPollCmd.tlvCnt = 1
    
#   // setup the tMcpPollCmdSendPollDataTlv structure
    pMcpPollCmdSendPollDataTlv.tlvHdr.length = len(pMcpPollCmdSendPollDataTlv.encode())
    pMcpPollCmdSendPollDataTlv.tlvHdr.type = cMcpPollDiscover

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpPollCmd.encode()) + len(pMcpPollCmdSendPollDataTlv.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1 # Does the E_ASIC MAC do this ???
    pMcpMsgHdr.ackdSeqNum = 1 # what when where
    packet = pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pMcpPollCmdSendPollDataTlv.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pMcpPollCmdSendPollDataTlv.encode()
    
    return packet


#// send a squelch command to a single PCU. BC to it with the embedded SERIAL_NUMBER
def enphase_pack_squelch(devSerialNum, squelchTimeInSecs):
    if type(devSerialNum) is not str:
        raise Exception("devSerialNum argument must be BCD str.")
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pAssocHdr = tMcpAssociationTlv()
    pMcpAssocSquelch = tMcpAssociationSquelchTlv()
    pCmdAssocSquelchTlv = tMcpAssociationSquelchData() # just to get length!
    pMcpAddr.modAddr =((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DIRECTED);

#   // setup the tMcpAssociationTlv structure
    pMcpAssocSquelch.tlvHdr.type = cMcpAssocCmdSquelch
    pMcpAssocSquelch.tlvHdr.length = len(pCmdAssocSquelchTlv.encode())
    pAssocHdr.tlvCnt = 1

#   // setup the tMcpAssociationSquelch structure
    pMcpAssocSquelch.squelch.seconds = squelchTimeInSecs
    pack_bcd(pMcpAssocSquelch.squelch.devSerialNum.byteArr,devSerialNum,0,len(devSerialNum))
#    pMcpAssocSquelch.squelch.devSerialNum =  devSerialNum

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssocSquelch.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    
    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocSquelch.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocSquelch.encode()
    
    return packet

#// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches devSerial and return devInfo
    
def enphase_extract_payload(packet,msg_id=None,mde_id=None):
    pMCPV6msg = mcp_v6_poll_response_message_t(packet)
    if msg_id is not None and pMCPV6msg.header.msg_id != msg_id:
        raise Exception("Expected msg_id = "+str(msg_id)+ " but got "+str(pMCPV6msg.header.msg_id))
    mde_start_pos = len(pMCPV6msg.encode())-MCP_V6_POLL_RESPONSE_MESSAGE_BODY_SIZE
    pMDEHeader = MDE_header_t()
    mde_hdr_size = len(pMDEHeader.encode())
    payload_start = mde_start_pos+mde_hdr_size
    pMDEHeader.set_data(packet[mde_start_pos:payload_start])
    if mde_id is not None and pMDEHeader.mde_id != mde_id:
        raise Exception("Expected mde_id = "+str(mde_id)+ " but got "+str(pMDEHeader.mde_id))
    payload_end = payload_start + pMDEHeader.mde_len
    return packet[payload_start:payload_end]

def SPM_unpack_version(packet):
    return sSC_revision_v2(packet[1:])

def SPM_pack_FW_segment(bdata,seg,bypass=False):
    pos = seg * SPM_FW_PACKET_SIZE
    if not bypass and pos >= len(bdata):
        return None
    pSPMupg = sSCCmdUpgrade_t()
    pSPMupg.dev = eSC_INTF_UPGRD_DATA
    pSPMupg.data.address = pos
    if not bypass:
        psize = min(len(bdata[pos:]),SPM_FW_PACKET_SIZE)
        pSPMupg.data.no_of_valid_bytes = psize
        for i in range(psize):
            pSPMupg.data.data_bytes[i] = bdata[pos+i]
    else:
        pSPMupg.data.no_of_valid_bytes = 0
        
    return pSPMupg.encode()

def enphase_generic_poll(domainAddr,modAddr,mde_id):
    if type(domainAddr) is not str:
        raise Exception("domainAddr argument must be BCD str.")
    pMcpAddr = tMcpAddressHdr()
    pPollRqst = mcp_v6_poll_message_t()
    
    pack_bcd(pMcpAddr.domainAddr,domainAddr,0,len(domainAddr))
    
#    pMcpAddr.domainAddr = domainAddr
    pMcpAddr.modAddr = (modAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST
    
    pPollRqst.header.msg_id = cMcpMsgIdPollContainer
#    pPollRqst.mde_bitmap[1] = 0x40
    set_array_bits(pPollRqst.mde_bitmap,[mde_id])
    
    p1 = pMcpAddr.encode()
    p2 = pPollRqst.encode()
    packet = p1 + p2
    
    return packet[:-MCP_V6_POLL_MESSAGE_BODY_SIZE]


def enphase_pack_ECDC(domainAddr,modAddr,payload):
    if type(domainAddr) is not str:
        raise Exception("domainAddr argument must be BCD str.")
    pMcpAddr = tMcpAddressHdr()
    pPollRqst = mcp_v6_poll_message_t()
    
    pack_bcd(pMcpAddr.domainAddr,domainAddr,0,len(domainAddr))
    
#    pMcpAddr.domainAddr = domainAddr
    pMcpAddr.modAddr = (modAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST
    
    pPollRqst.header.msg_id = cMcpMsgIdPollContainer
#    pPollRqst.mde_bitmap[1] = 0x40
    set_array_bits(pPollRqst.mde_bitmap,[mde_id_ECDC_payload])
    
    for i in range(len(payload)):
        pPollRqst.body[i] = payload[i]
    
    p1 = pMcpAddr.encode()
    p2 = pPollRqst.encode()
    packet = p1 + p2
    
    return packet


def enphase_pack_v6_telemetry_1(domainAddr,modAddr):
    return enphase_generic_poll(domainAddr,modAddr,mde_id_read_telemetry_level1)
    
#// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches devSerial and return devInfo
def enphase_pack_v6_fw_info(domainAddr,modAddr):
    return enphase_generic_poll(domainAddr,modAddr,mde_id_get_firmware_information)

#// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches devSerial and return devInfo
def enphase_pack_association(domainAddr, devSerialNum, modAddr):
    if type(domainAddr) is not str:
        raise Exception("domainAddr argument must be BCD str.")
    if type(domainAddr) is not str:
        raise Exception("devSerialNum argument must be BCD str.")
    pMcpAddr = tMcpAddressHdr() 
    pMcpAddr._format = Format.BigEndian
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pAssocHdr = tMcpAssociationTlv()
    pAssocHdr._format = Format.BigEndian
    pMcpAssoc = tMcpAssociationLoadTlv()
    pMcpAssoc._format = Format.BigEndian
    pMcpAssociationLoadData = tMcpAssociationLoadData()
    pMcpAssociationLoadData._format = Format.BigEndian

#   // setup Message header to E_ASIC to format L2 frame header -
    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)

#   // setup the tMcpAssociationTlv structure

    pMcpAssoc.tlvHdr.type = cMcpAssocCmdLoad
    pMcpAssoc.tlvHdr.length = len(pMcpAssociationLoadData.encode())
    pAssocHdr.tlvCnt = 1

#   // setup the tMcpAssociationDiscover structure
    pack_bcd(pMcpAssoc.load.domainAddr,domainAddr,0,len(domainAddr))
#    pMcpAssoc.load.domainAddr = domainAddr
    pack_bcd(pMcpAssoc.load.devSerialNum,devSerialNum,0,len(devSerialNum))
#    pMcpAssoc.load.devSerialNum = devSerialNum
    pMcpAssoc.load.modAddr = modAddr

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssoc.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssoc.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssoc.encode()
    
    return packet

def enphase_pack_v6_write_metadata(data):
    pMetaWrite = mde_payload_EAC_request_write_metadata_t()
        
    pMcpAddr = tMcpAddressHdr()
    pMcpAddr.modAddr = BcastAll << 14
    
    pAppHdr = app_msg_header()
    pAppHdr.msg_id = cMcpMsgIdMulticastContainer
    
    pMDEHdr = MDE_header_t()
    pMDEHdr.mde_id = mde_id_FW_write_metadata
    pMDEHdr.mde_len = len(pMetaWrite.encode())
    
    barr = bytes([MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE])
    
    arr = [0 for i in range(len(pMetaWrite.encode())-1)]
    pack_bcd(arr,data,0,len(data))
    barr += bytes(arr)
    pMetaWrite.set_data(bytes(barr))
    
#    output_bytes("Metadata packet",pMetaWrite.encode())
    
    return pMcpAddr.encode() + pAppHdr.encode() + pMDEHdr.encode() + pMetaWrite.encode()


def enphase_pack_v6_request_metadata(domainAddr,modAddr,bank=MDE_BANK_SELECT_FIRMWARE_BANK_ACTIVE_NOW):
    if type(domainAddr) is not str:
        raise Exception("domainAddr argument must be BCD str.")
    pMetaRequest = mde_payload_EAC_request_FW_image_meta_t()
    pMetaRequest.firmware_bank = bank
        
    pMcpAddr = tMcpAddressHdr()
    pack_bcd(pMcpAddr.domainAddr,domainAddr,0,len(domainAddr))
#    pMcpAddr.domainAddr = domainAddr
    pMcpAddr.modAddr = (modAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST
    
    pAppHdr = app_msg_header()
    pAppHdr.msg_id = cMcpMsgIdMulticastContainer
    
    pMDEHdr = MDE_header_t()
    pMDEHdr.mde_id = mde_id_FW_read_metadata
    pMDEHdr.mde_len = len(pMetaRequest.encode())

    return pMcpAddr.encode() + pAppHdr.encode() + pMDEHdr.encode() + pMetaRequest.encode()


def enphase_unpack_v6_metadata(packet):
    payload = enphase_extract_payload(packet)
    return mde_payload_PLD_response_read_ANY_metadata_t(payload)


def enphase_pack_v6_start_fw_upgrade(data,missing_hunks=None):
    if len(data) < 1:
        raise Exception("No data to send")
    pMcpAddr = tMcpAddressHdr()
    pAppHdr = app_msg_header()
    pMDEHdr = MDE_header_t()
    pFWupgStart = mde_payload_EAC_request_image_start_t()
    
    hunks = []

    if missing_hunks is not None:
        pFWupgStart.header.MDE_upgrade_start_type = MDE_FLASH_UPGRADE_START_NO_ERASE
    else:
        pFWupgStart.header.MDE_upgrade_start_type = MDE_FLASH_UPGRADE_START_ERASE_AREA
    
    num_hunks = int((len(data)/2-1)/MDE_IMAGE_PAGE_HUNK_BYTES) + 1
    num_bytes = int((num_hunks-1)/8) + 1
    if num_bytes > MDE_IMAGE_CONTRACT_MAX_BYTES:
        raise Exception("Contract size is too big: "+str(num_bytes)+" Max is: "+str(MDE_IMAGE_CONTRACT_MAX_BYTES))
    last_byte = 0
    
    for i in range(num_hunks % 8):
        last_byte <<= 1
        last_byte |= 1
    
    pFWupgStart.header.EAC_contract_length_bytes = num_bytes
    print("num_hunks=",num_hunks,"num_bytes=",num_bytes)
    
    for i in range(num_bytes):
        if missing_hunks is not None and i < len(missing_hunks):
            pFWupgStart.EAC_hunks_contract[i] = missing_hunks[i]
            hunks.append(missing_hunks[i])
        else:
            pFWupgStart.EAC_hunks_contract[i] = 0xFF
            hunks.append(0xFF)
    if missing_hunks is None and last_byte != 0:
        pFWupgStart.EAC_hunks_contract[num_bytes-1] = last_byte
        hunks[num_bytes-1] = last_byte
        
        
    
    pAppHdr.msg_id = cMcpMsgIdMulticastContainer
    pMDEHdr.mde_id = mde_id_FW_upgrade_start
    mde_len = len(pFWupgStart.encode()) - MDE_IMAGE_CONTRACT_MAX_BYTES + num_bytes
    pMDEHdr.mde_len = mde_len
    
    pMcpAddr.modAddr = BcastAll << 14
    
    return pMcpAddr.encode() + pAppHdr.encode() + pMDEHdr.encode() + pFWupgStart.encode()[:mde_len],hunks

def enphase_pack_v6_start_DMIR_upgrade(data,bank,missing_hunks=None):
    if len(data) < 1:
        raise Exception("No data to send")
    pMcpAddr = tMcpAddressHdr()
    pAppHdr = app_msg_header()
    pMDEHdr = MDE_header_t()
    pDMIRupgStart = mde_payload_EAC_request_model_start_t()
    
    hunks = []

    if missing_hunks is not None:
        pDMIRupgStart.header.MDE_upgrade_start_type = MDE_FLASH_UPGRADE_START_NO_ERASE
    else:
        pDMIRupgStart.header.MDE_upgrade_start_type = MDE_FLASH_UPGRADE_START_ERASE_AREA
    
    num_hunks = int((len(data)/2-1)/MDE_IMAGE_PAGE_HUNK_BYTES) + 1
    num_bytes = int((num_hunks-1)/8) + 1
    if num_bytes > MDE_IMAGE_CONTRACT_MAX_BYTES:
        raise Exception("Contract size is too big: "+str(num_bytes)+" Max is: "+str(MDE_IMAGE_CONTRACT_MAX_BYTES))
    last_byte = 0
    
    for i in range(num_hunks % 8):
        last_byte <<= 1
        last_byte |= 1
        
    #Note:  The data model ID is embedded in the header.  Extract it!
    
    arr = [0]
    pack_bcd(arr,data,9,1)
    
    data_model_ID = arr[0]
    
    pDMIRupgStart.header.EAC_contract_length_bytes = num_bytes
    pDMIRupgStart.header.MDE_FW_bank = bank
    pDMIRupgStart.header.MDE_data_model_ID = data_model_ID
    pDMIRupgStart.header.MDE_model_area_type = DM_AREA_TYPE_INIT_RECORD
    
    print("num_hunks=",num_hunks,"num_bytes=",num_bytes)
    
    for i in range(num_bytes):
        if missing_hunks is not None and i < len(missing_hunks):
            pDMIRupgStart.EAC_hunks_contract[i] = missing_hunks[i]
            hunks.append(missing_hunks[i])
        else:
            pDMIRupgStart.EAC_hunks_contract[i] = 0xFF
            hunks.append(0xFF)
    if missing_hunks is None and last_byte != 0:
        pDMIRupgStart.EAC_hunks_contract[num_bytes-1] = last_byte
        hunks[num_bytes-1] = last_byte
        
        
    
    pAppHdr.msg_id = cMcpMsgIdMulticastContainer
    pMDEHdr.mde_id = mde_id_DMIR_upgrade_start
    mde_len = len(pDMIRupgStart.encode()) - MDE_IMAGE_CONTRACT_MAX_BYTES + num_bytes
    pMDEHdr.mde_len = mde_len
    
    pMcpAddr.modAddr = BcastAll << 14
    
    return pMcpAddr.encode() + pAppHdr.encode() + pMDEHdr.encode() + pDMIRupgStart.encode()[:mde_len],hunks
    
def enphase_pack_v6_hunk(data,seq,mde_id=mde_id_FW_upgrade_write_page):
    if len(data) < 1:
        raise Exception("No data to send.")
    pMcpAddr = tMcpAddressHdr()
    pAppHdr = app_msg_header()
    pMDEHdr = MDE_header_t()
    pWriteHunk = mde_payload_EAC_request_write_image_hunk_t()
    
    num_hunks = int((len(data)/2-1)/MDE_IMAGE_PAGE_HUNK_BYTES) + 1
    if seq >= num_hunks:
        raise Exception("Sequence out of range.")
    
    pWriteHunk.hunk_sequence_number = seq
    
    pack_bcd(pWriteHunk.hunk_data,data,seq*MDE_IMAGE_PAGE_HUNK_BYTES,MDE_IMAGE_PAGE_HUNK_BYTES)
    
#    for i in range(MDE_IMAGE_PAGE_HUNK_BYTES):
#        pWriteHunk.hunk_data[i] = data[seq*MDE_IMAGE_PAGE_HUNK_BYTES + i]
    
    pAppHdr.msg_id = cMcpMsgIdMulticastContainer
    pMDEHdr.mde_id = mde_id
    pMDEHdr.mde_len = len(pWriteHunk.encode())
    
    pMcpAddr.modAddr = BcastAll << 14
    
    return pMcpAddr.encode() + pAppHdr.encode() + pMDEHdr.encode() + pWriteHunk.encode()

def enphase_pack_v6_poll_missing_hunks(domainAddr,modAddr,mde_id=mde_id_FW_upgrade_finish_and_check):
    return enphase_generic_poll(domainAddr,modAddr,mde_id)

    

def enphase_unpack_v6_missing_hunks(packet,missing_hunks):
    payload = enphase_extract_payload(packet)
    pResponse = mde_payload_PLD_response_to_image_check_t(payload)
    if pResponse.header.contract_length > MDE_IMAGE_CONTRACT_MAX_BYTES:
        raise Exception("Contract size exceeds maximum.")
    for i in range(pResponse.header.contract_length):
        if missing_hunks is None:
            missing_hunks = []
        if i >= len(missing_hunks):
            missing_hunks.append(0)
        missing_hunks[i] |= pResponse.EAC_hunks_contract[i]
    return missing_hunks


#// unpack a tMcpPollRspDevInfoTlv response.
#// What will the LIB actually received from the E_ASIC with a response???
#// Does the LIB/MIME need to the domainAddr or the modAddr if used???
#// for now, assume we start with a tMcpMsgHdr
#// any checking of the data packet (CRC/CHKSUM) will have been done higher up...
#// we are only doing one TLV per request for now
def enphase_unpack_deviceInfo_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv._format = Format.BigEndian
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspDevInfoTlv = tMcpPollRspDevInfoTlv()
    pRspDevInfoTlv._format = Format.BigEndian

    pMcpMsgHdr.set_data(packet[:pMcpMsgHdr_size])
    pPollRspTlv.set_data(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    pRspDevInfoTlv.set_data(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:])


    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv):
        raise Exception("Improper packet for devInfo_tl - cMcpMsgIdPollRspTlv: "+str(pMcpMsgHdr.msgId)+" <> " + str(cMcpMsgIdPollRspTlv))
    if (pPollRspTlv.pollRspFlags != cMcpPollFlagDeviceInfo):
        raise Exception("Improper packet for devInfo_tlv - cMcpPollFlagDeviceInfo: "+str(pPollRspTlv.pollRspFlags)+" <> " + str(cMcpPollFlagDeviceInfo))
    if (pRspDevInfoTlv.tlvHdr.type != cMcpPollDataDeviceInfo):
        raise Exception("Improper packet for devInfo_tlv - cMcpPollDataDeviceInfo: "+str(pRspDevInfoTlv.tlvHdr.type)+" <> " + str(cMcpPollDataDeviceInfo))

    return pRspDevInfoTlv,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus



 #// Unpack interval PollRspTlv
def enphase_unpack_v6_telemetry1(packet):
    payload = enphase_extract_payload(packet)
    return MDE_data_telemetry_level1_t(payload)
 #// Unpack interval PollRspTlv
def enphase_unpack_v6_telemetry2(packet):
    payload = enphase_extract_payload(packet)
    return MDE_data_telemetry_level2_t(payload)

def enphase_unpack_v6_fwinfo(packet):
    payload = enphase_extract_payload(packet)
    return MDE_FW_information_t(payload)

    
    
   


#def enphase_pack_unassociate_all():
#    pMcpAddr = tMcpAddressHdr()
#    pMcpAddr._format = Format.BigEndian
#    pMcpMsgHdr = tMcpMsgHdr()
#    pMcpMsgHdr._format = Format.BigEndian
#    pAssocHdr = tMcpAssociationTlv()
#    pAssocHdr._format = Format.BigEndian
#    pMcpAssocForgetAll = tMcpAssociationForgetAllTlv()
#    pMcpAssocForgetAll._format = Format.BigEndian
#
##   // setup Message header to E_ASIC to format L2 frame header -
#    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)
#
##   // setup the tMcpAssociationTlv structure
#    pMcpAssocForgetAll.tlvHdr.type = cMcpAssocCmdForgetAll
#    pMcpAssocForgetAll.tlvHdr.length = 0
#    pAssocHdr.tlvCnt = 1
#
##   // setup the tMcpMsgHdr structure
#    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
#    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
#    pMcpMsgHdr.rmsgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssocForgetAll.encode()) - 1 # subtract 1 since the varaible data has at least 1
#    pMcpMsgHdr.msgSeq = 1
#    pMcpMsgHdr.ackdSeqNum = 1
#    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocForgetAll.encode()
#    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet);
#    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocForgetAll.encode()
#    
#    return packet

def enphase_pack_unassociate_specific():
    pass

