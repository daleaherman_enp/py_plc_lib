# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 11:06:56 2018

@author: sdavila
"""

import serial
import serial.tools as tools

SCCMDHEADERV2_SEQUENCE_START = 0x80
SCCMDHEADERV2_SEQUENCE_START_MASK = 0x80
SCCMDHEADERV2_SEQUENCE_CONT = 0x40
SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK  = 0x0F

eSC_INTF_CMD_VERSION2           = 0xAA
eSC_INTF_WRITE_PLC              = 1
ePLC_INTF_WRITE_TX              = eSC_INTF_WRITE_PLC
SC_E_ASIC_PLC = 0x06
PLC_FLAGS_SYNCHRONOUS_PLC =  0x01
eSC_INTF_READ_PLC              = 2

xmit_flags = 0




crc16_table =[
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
]

def crc16ccitt(pBuf, buf_len, init):

    loopIndex = 0
    crc16temp = init
    while loopIndex < buf_len:
        crc16temp = (crc16temp >> 8) ^ crc16_table[(crc16temp ^ pBuf[loopIndex]) & 0xff];
        loopIndex+=1
    return crc16temp


class PLCException(Exception):
    pass

def comport_list():
    
    ports = []

    i = 0
    while True:
        try:
            port = tools.list_ports.comports().pop(i).device
            ports.append(port)
            i += 1
        except:
            break;
            
    ports.sort()
    return i,ports

class plc_channel:
    
    def __init__(self, port,speed=115200,debug=False):
        self.buf = []  # mock
        self.port = port;
        self.speed = speed
        self.debug = debug
        self.channel = serial.Serial(port,baudrate=self.speed);
        
    def close(self):
        self.channel.close()
        
    def write(self,byte_array):
        if type(byte_array) is not bytes:
            raise PLCException("write requires a byte array")
        if self.debug:
            print(self.channel.name,self.channel.baudrate,": Prepping",len(byte_array),"bytes...")
            output_bytes("write",byte_array)
        n = self.channel.write(byte_array)
        self.channel.flush()
        return n
            
    def get_buf(self):
        return b''.join(self.buf)
        
    def read(self):
        return self.channel.read()
    
    def send_packet(self,byte_array):
        if type(byte_array) != bytes:
            raise PLCException("Method xmit requires a byte array as input: "+str(byte_array))
        ibytes = byte_array
        
        payload = eSC_INTF_WRITE_PLC.to_bytes(1,"little") + PLC_FLAGS_SYNCHRONOUS_PLC.to_bytes(1,"little") + ibytes
        
        pheader = eSC_INTF_CMD_VERSION2.to_bytes(1,"little") + SC_E_ASIC_PLC.to_bytes(1,"little") + xmit_flags.to_bytes(2,"little")
        
        mask = SCCMDHEADERV2_SEQUENCE_START
        
        num = 0
        
        while len(payload) > 0:
            # pheader + sequence + packetSize
            segments_left = int((len(payload)+127)/128) - 1
            payload_size = min(len(payload),128)
            if self.debug:
                print("PAYLOAD SIZE",payload_size)
            ibytes = pheader + (segments_left | mask).to_bytes(1,"little") + (payload_size-2).to_bytes(1,"little") + payload[:payload_size]
            mask = SCCMDHEADERV2_SEQUENCE_CONT
            payload = payload[payload_size:]
            num += self.send(ibytes)
            
        return num
        
        
    def recv_packet(self):
        retries = 32
        
        payload = b''
        last_segments_left = 0
        
        decobbed = b''
        
        while True:
            obytes = self.recv()
            if self.debug:
                output_bytes("recv_packet",obytes)
            decobbed += obytes
            if (obytes[0] != eSC_INTF_CMD_VERSION2) or (obytes[1] != SC_E_ASIC_PLC):
                retries -= 1
                print("Warning: Unexpected packet received: " , obytes.hex())
                if retries == 0:
                    print("Error:  Too many malformed packets received")
                    return b''
                continue
            if (obytes[4] & SCCMDHEADERV2_SEQUENCE_START) == SCCMDHEADERV2_SEQUENCE_START:
                if (obytes[6] != ePLC_INTF_WRITE_TX) and (obytes[6] != eSC_INTF_READ_PLC):
                    print("Warning: Malformed packet received: " , obytes.hex())
                    retries -= 1
                    if retries == 0:
                        print("Error:  Too many malformed packets received")
                        return b''
                    continue
                payload = obytes[8:] # skip over sSCCmdHeaderV2_t + 2 lead bytes
                last_segments_left = segments_left = obytes[4] & SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK
            else:
                segments_left = obytes[4] & SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK
                if segments_left != last_segments_left - 1:
                    print("Warning: Segment received not in sequence.  Got",segments_left,"but expected",last_segments_left - 1)
                payload += obytes[6:] # skip over sSCCmdHeaderV2_t only
                last_segments_left = segments_left

            if segments_left == 0:
                break
            
        if len(decobbed) < 3:   
             raise PLCException("Received packet is too small:",len(payload),"bytes.")
        crc = crc16ccitt(decobbed,len(decobbed)-2,0xFFFF)
        if (crc & 0xFF != decobbed[len(decobbed)-2]) or ((crc >>8) & 0xFF != decobbed[len(decobbed)-1]):
            raise PLCException("Invalid CRC received")
            
        if self.debug:
            output_bytes("Payload",payload[:-2])
        return payload[:-2]
        
    def send(self,byte_array): # COBiffy
        if type(byte_array) != bytes:
            raise PLCException("Method send requires a byte array as input: "+str(byte_array))
        ibytes = byte_array
        crc = crc16ccitt(ibytes,len(ibytes),0xFFFF)
        ibytes += (crc & 0xFF).to_bytes(1,byteorder="little")
        ibytes += ((crc >> 8) & 0xFF).to_bytes(1,byteorder="little")
        if self.debug:
            print(ibytes.hex())
            print(len(ibytes))
        nonZeroes = 1
        cobs = b'\x00'
        temp = b''

        for c in ibytes:
            if c != 0:
                nonZeroes += 1
                temp += c.to_bytes(1,'big')
                continue
            cobs += nonZeroes.to_bytes(1,'big')
            cobs += temp
            temp = b''
            nonZeroes = 1
            
        cobs += nonZeroes.to_bytes(1,'big')
        cobs += temp
        cobs += b'\x00'
        if len(cobs) > 257:
            if self.debug:
                print()
                print(ibytes.hex())
                print()
                print(cobs.hex())
            raise PLCException("Send size exceeds 257 bytes",len(ibytes),len(cobs))
            
        if count_zeroes(cobs) != 2:
            raise PLCException("Too many zeroes!")
        if cobs[0] != 0 or cobs[len(cobs)-1] != 0:
            raise PLCException("Zeroes in the wrong places!")
            
        return self.write(cobs)
    
    def recv(self): # unCOBiffy
        outs = b''
        c = 3
        pending = False

        while c != 0:
            c = int.from_bytes(self.read(),byteorder='little')
            
        while True:
            nextz = int.from_bytes(self.read(),byteorder='little')
            if nextz == 0:
                return outs
            if pending:
                outs += b'\x00'
            while nextz > 1:
                outs += self.read()
                nextz -= 1
            pending = True
        
        return None;


def output_bytes(title,bs):
        print("\n",title,"---------------------------")
        i = 0
        for c in bs:
            if i % 16 == 0:
                print("")
            i += 1
            print("%02x " %c,end="")
        print("\n",title,"---------------------------")
        print(len(bs),"bytes.")
    
    
def count_zeroes(byte_array):
    count = 0
    for c in byte_array:
        if c==0:
            count += 1
    return count
        
if __name__ == "__dodo__":
    import random
    
    plc = plc_channel("COM1");
    
    try:
    
        for i in range(10):
            ibytes = b''
            r = random.randint(1,252)
            for j in range(r):
               ibytes += random.randint(0,255).to_bytes(1,byteorder="little")

            plc.send_packet(ibytes)
            obytes = plc.recv_packet()
            if ibytes != obytes:
                    print()
                    print()
                    print(ibytes.hex())
                    print("Are the arrays equal?",ibytes==obytes,"length=",len(ibytes),"Zeroes=",count_zeroes(ibytes))
                    print(obytes.hex())
            else:
                print(".",end="")
                
    finally:
       
        plc.close()
        
def tohex(c):
    if c == 'a':
        return 10
    if c == 'b':
        return 11
    if c == 'c':
        return 12
    if c == 'd':
        return 13
    if c == 'e':
        return 14
    if c == 'f':
        return 15
    return int(c)
    
        
def cvt(token):
    return (tohex(token[0]) << 4 | tohex(token[1])).to_bytes(1,"big")
        
if __name__ == "__main__":
    
        y =  "aa 06 00 00 80 25 01 01 00 00 00 00 00 00 c0 00 b4 7f 00 1b 07 22 01 01 5b 91 89 ea 00 01 01 01 01 00 0b 00 00 00 00 00 00 00 00 a7 39".split()
        print(y)
        arr = b''
        for b in y:
            arr += cvt(b)
            
        print(arr.hex())
        print("%04x" % crc16ccitt(arr[:-2],len(arr[:-2]),0xFFFF))

            
        

