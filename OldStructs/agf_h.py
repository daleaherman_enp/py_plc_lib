# ---------------------------------------------------------
#
# This file is auto-generated from C header file: agf.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Type,Format
from mcp_enphase_h import uint8_t

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

cAgfFunctionTypeNotUsed_1 = 14
cAgfFunctionTypeTV = 10
cAgfFunctionTypeNumOf = 22
cAgfCookieVariantNotUsed = 0
cAgfCookieVariantNumOf = 8
cAgfUnsedFunctions = 3
cAgfFunctionTypeVVAR = 1
cAgfCookieVariantManifest = 1
cAgfCookieVariantVVAR = 2
cAgfDefaultVariantCnt = 8

# CONSTANTS NOT USED IN THIS FILE
AGF_H_ = True
PCU_FREQUENCY = 50000000.0
LINE_FREQUENCY_NA = 60.0
LINE_FREQUENCY_EU = 50.0
LINE_HIGH_FREQUENCY = 120.0
LINE_LOW_FREQUENCY = 30.0
MAX_POWER = 285.0
mAgfFuncNotUsed = ((agfFunc==cAgfFunctionTypeNotUsed_1) or (agfFunc==cAgfFunctionTypeTV) or (agfFunc>=cAgfFunctionTypeNumOf))
mAgfFuncTrxNotUsed = ((agfFunc==cAgfFunctionTypeTV) or (agfFunc==cAgfFunctionTypeNotUsed_1))
mAgfCookieNotUsed = ((agfVersion==cAgfCookieVariantNotUsed) or (agfVersion>=cAgfCookieVariantNumOf))
cAgfCookieSlotsUsed = (cAgfFunctionTypeNumOf+cAgfCookieVariantNumOf-cAgfUnsedFunctions)
cAgfFunctionVersion = 1
cAgfFirstFunctionIdx = cAgfFunctionTypeVVAR
cAgfFirstCookieIdx = cAgfCookieVariantManifest
cAgfFirstCookiePtsIdx = cAgfCookieVariantVVAR
cAgfVvarVariantCnt = 4
cAgfVvarVariant11 = 0
cAgfVvarVariant12 = 1
cAgfVvarVariant13 = 2
cAgfVvarVariant14 = 3
cAgfVwVariantCnt = 1
cAgfVwVariant51 = 0
cAgfVw52VariantCnt = 1
cAgfVw52Variant52 = 1
cAgfWpVariantCnt = 1
cAgfWpVariant41 = 0
cAgfTvVariantCnt = 1
cAgfTvVariant31 = 0
cAgfFwVariantCnt = 1
cAgfFwVariant21 = 0
cAgfFw22VariantCnt = 1
cAgfFw22Variant22 = 1
cAgfInvVariantCnt = 2
cAgfInvVariant2 = 0
cAgfInvVariant4 = 1
cAgfRecordSuptPcu = 0x01
cAgfRecordSuptAcb = 0x02
cAgfRecordSuptNsrb = 0x04
cAgfLdStringLen = 16
cAgfFunctionTypeCookie = 0
cAgfFunctionTypeFRT = 2
cAgfFunctionTypeVRT = 3
cAgfFunctionTypeFPF = 4
cAgfFunctionTypePRL = 5
cAgfFunctionTypePLP = 6
cAgfFunctionTypeVW = 7
cAgfFunctionTypeINV2 = 8
cAgfFunctionTypeWP = 9
cAgfFunctionTypeFW = 11
cAgfFunctionTypeSS = 12
cAgfFunctionTypeISLND = 13
cAgfFunctionTypeIAC = 15
cAgfFunctionTypeVECT = 16
cAgfFunctionTypeROCOF = 17
cAgfFunctionTypeACAVE = 18
cAgfFunctionTypeVW52 = 19
cAgfFunctionTypeFW22 = 20
cAgfFunctionTypeWVAR = 21
cAgfCookieVariantFRT = 3
cAgfCookieVariantVRT = 4
cAgfCookieVariantVW = 5
cAgfCookieVariantVW52 = 6
cAgfCookieVariantWVAR = 7
# ---------------------------------------------------------
#
# STRUCTS 
#
# ---------------------------------------------------------



#	tAgfSupportedFuncData

class tAgfSupportedFuncData(Struct):
	_format = Format.BigEndian
	type = Type.UnsignedByte
	version = Type.UnsignedByte
	char*nameUpper = Type.Char
	char*description = Type.Char
	variantCount = Type.UnsignedByte
	variantStartIdx = Type.UnsignedByte
	char*variantNames = Type.Char[cAgfDefaultVariantCnt]
	deviceSupported = Type.UnsignedByte
	cookieVariantSupported = Type.UnsignedByte
