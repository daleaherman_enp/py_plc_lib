# ---------------------------------------------------------
#
# This file is auto-generated from C header file: enphase_packet.h 
#
# ---------------------------------------------------------
from embeddedstructs import Struct,Type,Format


# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

__PVSLIB_ENPHASE_PACKET_H = True
ENPHASE_MAX_PACKET = 768
# ---------------------------------------------------------
#
# STRUCTS 
#
# ---------------------------------------------------------



#	tIntervalLongData

class tIntervalLongData(Struct):
	_format = Format.BigEndian
	longlongjoulesProduced = Type.UnsignedLong
	longlongjoulesUsed = Type.UnsignedLong
	longlongsumLeadingVAr = Type.UnsignedLong
	longlongsumLaggingVAr = Type.UnsignedLong
