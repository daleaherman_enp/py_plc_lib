# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 13:42:24 2018

@author: sdavila
"""


from time import time

from embeddedstructs import Struct,Type,Format
from mcp_sunpower_h import  tMcpMsgHdr,tMcpPollCmdTlv,tMcpPollCmdSendPollDataTlv,cMcpPollFlagDeviceInfo,cMcpPollDiscover,\
                            cMcpMsgIdPollCmdTlv,cMcpProtocolVersionSeven,cL2DomainAddrLen,cMcpAssocCmdSquelch,\
                            tMcpAssociationTlv,tMcpAssociationSquelchTlv,tMcpAssociationSquelchData,cMcpMsgIdAssociation,\
                            tMcpAssociationLoadTlv,tMcpAssociationLoadData,cMcpAssocCmdLoad,cMcpMfgCmdStart,cMcpMfgCmdStop, \
                            tMcpMfgTlv,tMcpMfgStartData,cMcpMsgIdMfg,tMcpTlvHeader,cMcpPollSendPollData,tMcpMfgLoadData ,\
                            cMcpMfgCmdLoad,tMcpPollRspTlv,tMcpPollRspDevInfoTlv,cMcpMsgIdPollRspTlv,cMcpPollDataDeviceInfo,\
                            tMcpPollRspAgfRecListTlv,tMcpAgfLoadedRecStatus,cMcpPollFlagAgfInfo,cMcpPollDataAgfInfo,\
                            tMcpPollRspIntervalTlv,cMcpPollFlagInterval,cMcpPollDataInterval,tMcpPollRspCounterTlv,\
                            cMcpPollFlagCounters,cMcpPollDataCounters,tMcpPollRspConditionTlv,cMcpPollFlagCondition,\
                            cMcpPollDataCondition,tMcpPollCmdSendGroupMaskTlv,cMcpPollSetGroupMask,tMcpAssociationForgetAllTlv,\
                            cMcpAssocCmdForgetAll,tMcpAgfRecords,tMcpAgfRecHeader,cMcpMsgIdAgfRecords,tMcpAgfExpRecordsTlv,\
                            cAgfFunctionTypeNumOf,cMcpMsgIdExpAgfRecordTlv,tMcpPollCmdConditionAcknowledgeTlv,\
                            cMcpPollConditionAcknowledge
#from plc_poll_c import tMCPV6Hdr,tTlvHdr,tMDETelemtryL1Data,tTLVL1TelemetryPkt
from enphase_command_c import tPollCmdRequestV6
                            

ENPH_MODULE_ADDRESS_MASK   =         0x3FFF
ENPH_ADDRESSING_MODE_MASK    =       0xC000
ENPH_ADDRESS_MODE_UNICAST     =      0x0000 # L2 directed
ENPH_ADDRESS_MODE_BCAST_DIRECTED  =  0x4000 # L2 McastMod
ENPH_ADDRESS_MODE_BCAST_MODZERO =    0x8000 #L2 BcastCoord
ENPH_ADDRESS_MODE_BCAST_DOMAINZERO = 0xC000 # L2 BcastAll


class tMcpAddressHdr(Struct):
	_format = Format.BigEndian
	domainAddr = Type.UnsignedByte[cL2DomainAddrLen]
	modAddr = Type.UnsignedShort
    
    
def hex_digit(n):
    if n > 9:
        return chr(ord('A') + n-10)
    return chr(ord('0') + n)

def unpack_bcd(barray):
    outs = ''
    for c in barray:
        s1 = hex_digit((c & 0xF0)>>4)
        s2 =  hex_digit(c & 0x0F)
        outs += s1
        outs += s2
    return outs


def unpack_chars(barray):
    outs = ''
    for c in barray:
        if c == 0:
            return outs
        outs += chr(c)
    return outs

def unpack_partnum(pn_struct):
    bcd = unpack_bcd(pn_struct.idb)
    
    pn = bcd[:3]+'-'+bcd[3:]+'-r'+str(pn_struct.manuRev)
    if (pn_struct.dMajor != 0) or (pn_struct.dMinor != 0) or (pn_struct.dMaint != 0):
        pn += "-v"+str(pn_struct.dMajor)+"."+str(pn_struct.dMinor)+"."+str(pn_struct.dMaint)
    
    return pn

def unpack_millijoules(arrayu16):
    mj = 0
    for v in arrayu16:
        mj = (mj << 16) + v
    
    return mj



#/*
#* Enphase message checksum code
#*/
#static uint16_t
def mcpUtilFletcher8BitCsum(byte_array,arr_len=None):
    a = 0
    b = 0
    i = 0
    
    if arr_len is None:
        arr_len = len(byte_array)
    
    for c in byte_array:
        if i >= arr_len:
            break
        a += byte_array[i]
        b += a
        i += 1
        
    return ((a & 0xFF) << 8) + (b & 0xFF)

#static uint16_t
def mcpUtilMcpMsgCsum(byte_array):
    s = tMcpMsgHdr()
    s = tMcpMsgHdr(byte_array[:len(s.encode())])
    return mcpUtilFletcher8BitCsum(byte_array[2:],s.msgLen-2)

#/* This formats a search/discover/idall packet All unassociated, unsquelched PCUs send their devInfo in a ~3second (up to 24 devices) window
#* with some automatic collision avoidance
#*  Assumes a header to the E_ASIC of tMcpAddressHdr that is used to format the Enphase L2 header */
#int32_t enphase_pack_poll_command_search(uint8_t *packet)
    
def enphase_pack_poll_command_search():
    pMcpAddr = tMcpAddressHdr()
    pMcpAddr._format = Format.BigEndian
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pMcpPollCmd = tMcpPollCmdTlv()
    pMcpPollCmd._format = Format.BigEndian
    pMcpPollCmdSendPollDataTlv = tMcpPollCmdSendPollDataTlv()
    pMcpPollCmdSendPollDataTlv._format = Format.BigEndian

    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)
    
    pMcpPollCmd.msgTime = int(time())
    pMcpPollCmd.pollReqFlags = cMcpPollFlagDeviceInfo
    pMcpPollCmd.sequenceNumber = 1
    pMcpPollCmd.tlvCnt = 1
    
#   // setup the tMcpPollCmdSendPollDataTlv structure
    pMcpPollCmdSendPollDataTlv.tlvHdr.length = len(pMcpPollCmdSendPollDataTlv.encode())
    pMcpPollCmdSendPollDataTlv.tlvHdr.type = cMcpPollDiscover

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpPollCmd.encode()) + len(pMcpPollCmdSendPollDataTlv.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1 # Does the E_ASIC MAC do this ???
    pMcpMsgHdr.ackdSeqNum = 1 # what when where
    packet = pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pMcpPollCmdSendPollDataTlv.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pMcpPollCmdSendPollDataTlv.encode()
    
    return packet


#// send a squelch command to a single PCU. BC to it with the embedded SERIAL_NUMBER
def enphase_pack_squelch(devSerialNum, squelchTimeInSecs):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pAssocHdr = tMcpAssociationTlv()
    pMcpAssocSquelch = tMcpAssociationSquelchTlv()
    pCmdAssocSquelchTlv = tMcpAssociationSquelchData() # just to get length!
    pMcpAddr.modAddr =((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DIRECTED);

#   // setup the tMcpAssociationTlv structure
    pMcpAssocSquelch.tlvHdr.type = cMcpAssocCmdSquelch
    pMcpAssocSquelch.tlvHdr.length = len(pCmdAssocSquelchTlv.encode())
    pAssocHdr.tlvCnt = 1

#   // setup the tMcpAssociationSquelch structure
    pMcpAssocSquelch.squelch.seconds = squelchTimeInSecs
    pMcpAssocSquelch.squelch.devSerialNum =  devSerialNum

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssocSquelch.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    
    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocSquelch.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocSquelch.encode()
    
    return packet

#// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches devSerial and return devInfo
def enphase_pack_v6_telemetry_1(domainAddr,modAddr):
    pMcpAddr = tMcpAddressHdr()
    pPollRqst = tPollCmdRequestV6()
    
    pMcpAddr.domainAddr = domainAddr
    pMcpAddr.modAddr = (modAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST
    
    pPollRqst.msg_id = 50
    pPollRqst.mde_bitmap[1] = 0x40
    
    return pMcpAddr.encode() + pPollRqst.encode()
    
    

#// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches devSerial and return devInfo
def enphase_pack_association(domainAddr, devSerialNum, modAddr):
    pMcpAddr = tMcpAddressHdr() 
    pMcpAddr._format = Format.BigEndian
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pAssocHdr = tMcpAssociationTlv()
    pAssocHdr._format = Format.BigEndian
    pMcpAssoc = tMcpAssociationLoadTlv()
    pMcpAssoc._format = Format.BigEndian
    pMcpAssociationLoadData = tMcpAssociationLoadData()
    pMcpAssociationLoadData._format = Format.BigEndian

#   // setup Message header to E_ASIC to format L2 frame header -
    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)

#   // setup the tMcpAssociationTlv structure

    pMcpAssoc.tlvHdr.type = cMcpAssocCmdLoad
    pMcpAssoc.tlvHdr.length = len(pMcpAssociationLoadData.encode())
    pAssocHdr.tlvCnt = 1

#   // setup the tMcpAssociationDiscover structure
    pMcpAssoc.load.domainAddr = domainAddr
    pMcpAssoc.load.devSerialNum = devSerialNum
    pMcpAssoc.load.modAddr = modAddr

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssoc.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssoc.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssoc.encode()
    
    return packet

#// Manuf command that either starts or stops power, stopping erases LTE
def enphase_pack_start_stop_power_now(devSerialNum, bStart):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMfg = tMcpMfgTlv()
    pCmdStartData = tMcpMfgStartData()

#   // setup Message header to E_ASIC to format L2 frame header -
    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)

#   // setup the tMcpMfgTlv structure
    pMcpMfg.devSerialNum = devSerialNum
    pMcpMfg.tlvCnt = 1

#   // setup the tMcpMfgStartData structure
    if bStart:
        pCmdStartData.tlvHdr.type = cMcpMfgCmdStart
    else:
        pCmdStartData.tlvHdr.type = cMcpMfgCmdStop
    pCmdStartData.tlvHdr.length = len(pCmdStartData.encode())

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdMfg
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpMfg.encode()) + len(pCmdStartData.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pMcpMfg.encode()[:-1] + pCmdStartData.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpMfg.encode()[:-1] + pCmdStartData.encode()
    
    return packet
   
#// issue a request to send poll data to a single PCU using unicast/domain/logical address
#// can be one or more of cMcpPollFlagDeviceInfo, cMcpPollFlagImageInfo, cMcpPollFlagAgfInfo, cMcpPollFlagInterval, cMcpPollFlagCondition, cMcpPollFlagCounters, cMcpPollFlagPhaseAware, etc
def enphase_pack_poll_request_data_tlv(domainAddr, logicalAddr, reqFlags,devSerialNum):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpPollCmd = tMcpPollCmdTlv()
    pPollCmdSendPollDataTlv = tMcpPollCmdSendPollDataTlv()
    pMcpTlvHeader = tMcpTlvHeader()

    if devSerialNum is None:
        pMcpAddr.domainAddr = domainAddr
        pMcpAddr.modAddr = ((logicalAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)
        pPollCmdSendPollDataTlv.devSerialNum = 0
    else:
        pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)
        pPollCmdSendPollDataTlv.devSerialNum = devSerialNum

#   // setup the tMcpPollCmdTlv structure
    pMcpPollCmd.msgTime = int(time())
    pMcpPollCmd.pollReqFlags = reqFlags
    pMcpPollCmd.sequenceNumber = 1
    pMcpPollCmd.tlvCnt = 1

#   // setup the tMcpPollCmdSendPollDataTlv structure
    pPollCmdSendPollDataTlv.tlvHdr.length = len(pPollCmdSendPollDataTlv.encode()) - len(pMcpTlvHeader.encode())
    pPollCmdSendPollDataTlv.tlvHdr.type = cMcpPollSendPollData

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdPollCmdTlv
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpPollCmd.encode()) + len(pPollCmdSendPollDataTlv.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pPollCmdSendPollDataTlv.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet =  pMcpAddr.encode()
    packet += pMcpMsgHdr.encode() 
    packet += pMcpPollCmd.encode()[:-1] 
    packet += pPollCmdSendPollDataTlv.encode()
    
    return packet
    
#// Mfg command to load module PV data during EOL
def enphase_pack_load_pv_data(devSerialNum, moduleData):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMfg = tMcpMfgTlv()
    pCmdLoadData = tMcpMfgLoadData()

#   // setup Message header to E_ASIC to format L2 frame header -
    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DIRECTED)

#   // setup the tMcpMfgTlv structure
    pMcpMfg.devSerialNum = devSerialNum
    pMcpMfg.tlvCnt = 1

#   // setup the tMcpMfgStartData structure
    pCmdLoadData.tlvHdr.type = cMcpMfgCmdLoad
    pCmdLoadData.tlvHdr.length =len(pCmdLoadData.encode())
    pCmdLoadData.moduleEntry =  moduleData

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdMfg
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpMfg.encode()) + len(pCmdLoadData.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pMcpMfg.encode() + pCmdLoadData.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpMfg.encode() + pCmdLoadData.encode()
    
    return packet

#// unpack a tMcpPollRspDevInfoTlv response.
#// What will the LIB actually received from the E_ASIC with a response???
#// Does the LIB/MIME need to the domainAddr or the modAddr if used???
#// for now, assume we start with a tMcpMsgHdr
#// any checking of the data packet (CRC/CHKSUM) will have been done higher up...
#// we are only doing one TLV per request for now
def enphase_unpack_deviceInfo_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv._format = Format.BigEndian
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspDevInfoTlv = tMcpPollRspDevInfoTlv()
    pRspDevInfoTlv._format = Format.BigEndian

    pMcpMsgHdr = tMcpMsgHdr(packet[:pMcpMsgHdr_size])
    pPollRspTlv = tMcpPollRspTlv(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    pRspDevInfoTlv = tMcpPollRspDevInfoTlv(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:-1])

    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv) or (pPollRspTlv.pollRspFlags != cMcpPollFlagDeviceInfo) or (pRspDevInfoTlv.tlvHdr.type != cMcpPollDataDeviceInfo):
        raise Exception("Improper packet for devIndo_tlv")

    return pRspDevInfoTlv,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus

#// unpack a tMcpPollRspAgfRecListTlv response.
#// What will the LIB actually received from the E_ASIC with a response???
#// Does the LIB/MIME need to the domainAddr or the modAddr if used???
#// for now, assume we start with a tMcpMsgHdr
#// any checking of the data packet (CRC/CHKSUM) will have been done higher up...
#// we are only doing one TLV per request for now
def enphase_unpack_agfInfo_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspAgfRecListTlv = tMcpPollRspAgfRecListTlv()
    pRspAgfRecListTlv_size = len(pRspAgfRecListTlv.encode())
    pMcpAgfLoadedRecStatus = tMcpAgfLoadedRecStatus()
    pMcpAgfLoadedRecStatus_size = len(pMcpAgfLoadedRecStatus.encode())

    pMcpMsgHdr = tMcpMsgHdr(packet[:pMcpMsgHdr_size])
    pPollRspTlv = tMcpPollRspTlv(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    pRspAgfRecListTlv = tMcpPollRspAgfRecListTlv(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:pMcpMsgHdr_size+pPollRspTlv_size-1+pRspAgfRecListTlv_size])

#   // should only have one response
    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv) or (pPollRspTlv.pollRspFlags != cMcpPollFlagAgfInfo) or (pRspAgfRecListTlv.tlvHdr.type != cMcpPollDataAgfInfo):
        raise Exception("Improper packet for tMcpPollRspAgfRecListTlv")
    length = (pRspAgfRecListTlv.tlvHdr.length) / pMcpAgfLoadedRecStatus_size
    offset = pMcpMsgHdr_size+pPollRspTlv_size-1+pRspAgfRecListTlv_size - pMcpAgfLoadedRecStatus_size
    agfRecStatus = [] # returns a list of pMcpAgfLoadedRecStatus records
    for i in range(length):
        agfRecStatus.append(tMcpAgfLoadedRecStatus(packet[offset:offset+pMcpAgfLoadedRecStatus_size]))
        
    return agfRecStatus,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus
        

#// Unpack interval PollRspTlv
def enphase_unpack_interval_data_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspIntervalTlv = tMcpPollRspIntervalTlv()

    pMcpMsgHdr = tMcpMsgHdr(packet[:pMcpMsgHdr_size])
    pPollRspTlv = tMcpPollRspTlv(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    print("Buffer is",len(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:]),"bytes")
    pRspIntervalTlv = tMcpPollRspIntervalTlv(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:-1]) #pMcpMsgHdr_size+pPollRspTlv_size-1+pRspIntervalTlv_size])

    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv) or (pPollRspTlv.pollRspFlags != cMcpPollFlagInterval) or (pRspIntervalTlv.tlvHdr.type != cMcpPollDataInterval):
        raise Exception("Improper packet for tMcpPollRspIntervalTlv")

    return pRspIntervalTlv,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus
    
#// Unpack counter data poll response
def enphase_unpack_counter_data_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspCounterTlv = tMcpPollRspCounterTlv()
    pRspCounterTlv_size = len(pRspCounterTlv.encode())

    pMcpMsgHdr = tMcpMsgHdr(packet[:pMcpMsgHdr_size])
    pPollRspTlv = tMcpPollRspTlv(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    pRspCounterTlv = tMcpPollRspCounterTlv(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:pMcpMsgHdr_size+pPollRspTlv_size-1+pRspCounterTlv_size])

    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv) or (pPollRspTlv.pollRspFlags != cMcpPollFlagCounters) or (pRspCounterTlv.tlvHdr.type != cMcpPollDataCounters):
        raise Exception("Improper packet for tMcpPollRspCounterTlv")

    return pRspCounterTlv,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus


def enphase_unpack_conditionData_tlv(packet):
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr_size = len(pMcpMsgHdr.encode())
    pPollRspTlv = tMcpPollRspTlv()
    pPollRspTlv_size = len(pPollRspTlv.encode())
    pRspCondDataTlv = tMcpPollRspConditionTlv()
    pRspCondDataTlv_size = len(pRspCondDataTlv.encode())

    pMcpMsgHdr = tMcpMsgHdr(packet[:pMcpMsgHdr_size])
    pPollRspTlv = tMcpPollRspTlv(packet[pMcpMsgHdr_size:pMcpMsgHdr_size+pPollRspTlv_size])
    pRspCondDataTlv = tMcpPollRspConditionTlv(packet[pMcpMsgHdr_size+pPollRspTlv_size-1:pMcpMsgHdr_size+pPollRspTlv_size-1+pRspCondDataTlv_size])

    if (pMcpMsgHdr.msgId != cMcpMsgIdPollRspTlv) or (pPollRspTlv.pollRspFlags != cMcpPollFlagCondition) or (pRspCondDataTlv.tlvHdr.type != cMcpPollDataCondition):
        raise Exception("Improper packet for tMcpPollRspConditionTlv")
    return pRspCondDataTlv,pPollRspTlv.msgTime,pPollRspTlv.pollRspStatus


def enphase_pack_set_groupMask(domainAddr, logicalAddr, groupMask):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpPollCmd = tMcpPollCmdTlv()
    pPollCmdSendGroupDataTlv = tMcpPollCmdSendGroupMaskTlv()
    pMcpTlvHeader = tMcpTlvHeader()

    pMcpAddr.domainAddr =  domainAddr
    pMcpAddr.modAddr = (logicalAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST

    pMcpPollCmd.msgTime = int(time())
    pMcpPollCmd.pollReqFlags = cMcpPollFlagDeviceInfo
    pMcpPollCmd.sequenceNumber = 1
    pMcpPollCmd.tlvCnt = 1

#   // setup the tMcpPollCmdSendGroupMaskTlv structure
    pPollCmdSendGroupDataTlv.tlvHdr.length = len(pPollCmdSendGroupDataTlv.encode()) - len(pMcpTlvHeader.encode())
    pPollCmdSendGroupDataTlv.tlvHdr.type = cMcpPollSetGroupMask
    pPollCmdSendGroupDataTlv.grpMask = groupMask
    
#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdPollCmdTlv
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen =len(pMcpMsgHdr.encode()) + len(pMcpPollCmd.encode()) + len(pPollCmdSendGroupDataTlv.encode()) - 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pPollCmdSendGroupDataTlv.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet);

    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pPollCmdSendGroupDataTlv.encode()
    return packet

def enphase_pack_unassociate_all():
    pMcpAddr = tMcpAddressHdr()
    pMcpAddr._format = Format.BigEndian
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpMsgHdr._format = Format.BigEndian
    pAssocHdr = tMcpAssociationTlv()
    pAssocHdr._format = Format.BigEndian
    pMcpAssocForgetAll = tMcpAssociationForgetAllTlv()
    pMcpAssocForgetAll._format = Format.BigEndian

#   // setup Message header to E_ASIC to format L2 frame header -
    pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)

#   // setup the tMcpAssociationTlv structure
    pMcpAssocForgetAll.tlvHdr.type = cMcpAssocCmdForgetAll
    pMcpAssocForgetAll.tlvHdr.length = 0
    pAssocHdr.tlvCnt = 1

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAssociation
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.rmsgLen = len(pMcpMsgHdr.encode()) + len(pAssocHdr.encode()) + len(pMcpAssocForgetAll.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocForgetAll.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet);
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAssocHdr.encode()[:-1] + pMcpAssocForgetAll.encode()
    
    return packet

def enphase_pack_unassociate_specific():
    pass

def enphase_pack_tlv_agf(domainAddr, logicalAddress, groupMask, agf_type,agf_version,agf_key,payload):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pAgfRecords = tMcpAgfRecords()
    pAgfRecordHdr = tMcpAgfRecHeader()

    if domainAddr is None:
#      // setup Message header to E_ASIC to format L2 frame header -
#      // Unassociated will respond to DOMAIN ZERO broadcast
        pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)
    else:
        pMcpAddr.domainAddr = domainAddr
        pMcpAddr.modAddr = ((logicalAddress & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)

    pAgfRecords.count = 1
    pAgfRecords.grpMask = groupMask
    pAgfRecords.msgTime = int(time())

    pAgfRecordHdr.type = agf_type
    pAgfRecordHdr.version = agf_version
    pAgfRecordHdr.key = agf_key
    pAgfRecordHdr.recLength = len(payload)
    pAgfRecordHdr.recCsum = mcpUtilFletcher8BitCsum(payload)

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdAgfRecords
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAgfRecords.encode()) + len(pAgfRecordHdr.encode()) + len(payload) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pAgfRecords.encode()[:-1] + pAgfRecordHdr.encode() + payload
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet);
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAgfRecords.encode()[:-1] + pAgfRecordHdr.encode() + payload
    
    return packet


def enphase_pack_exp_tlv_agf(domainAddr, logicalAddress, groupMask, pAgfRecordHeaderList):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pAgfExpRecordsTlv = tMcpAgfExpRecordsTlv()

    if domainAddr is None:
        pMcpAddr.modAddr = ((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO)
    else:
        pMcpAddr.domainAddr = domainAddr
        pMcpAddr.modAddr = ((logicalAddress & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)
    pAgfExpRecordsTlv.count = cAgfFunctionTypeNumOf
    pAgfExpRecordsTlv.grpMask = groupMask
    pAgfExpRecordsTlv.msgTime = int(time())
    
    payload = b''
    
    i = 0
    for r in pAgfRecordHeaderList:
        if i >= cAgfFunctionTypeNumOf:
            break
        payload += r.encode()
        i += 0
    
    pAgfExpRecordsTlv.count = i
        
    pMcpMsgHdr.msgId = cMcpMsgIdExpAgfRecordTlv
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pAgfExpRecordsTlv.encode()) + len(payload) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pAgfExpRecordsTlv.encode()[:-1] + payload
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet);
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pAgfExpRecordsTlv.encode()[:-1] + payload
    return packet

def enphase_pack_condition_poll_ack(seqNum, domain,  logicalAddress):
    pMcpAddr = tMcpAddressHdr()
    pMcpMsgHdr = tMcpMsgHdr()
    pMcpPollCmd = tMcpPollCmdTlv()
    pConditionAck = tMcpPollCmdConditionAcknowledgeTlv()
    pMcpTlvHeader = tMcpTlvHeader()

    pMcpAddr.domainAddr = domain
    pMcpAddr.modAddr = ((logicalAddress & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST)

#   // setup the tMcpPollCmdTlv structure
    pMcpPollCmd.msgTime = int(time())
    pMcpPollCmd.pollReqFlags = cMcpPollFlagCondition
    pMcpPollCmd.sequenceNumber = 1
    pMcpPollCmd.tlvCnt = 1

    pConditionAck.tlvHdr.length = len(pConditionAck.encode()) - len(pMcpTlvHeader.encode())
    pConditionAck.tlvHdr.type = cMcpPollConditionAcknowledge
    pConditionAck.sequenceNumber = seqNum

#   // setup the tMcpMsgHdr structure
    pMcpMsgHdr.msgId = cMcpMsgIdPollCmdTlv
    pMcpMsgHdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven
    pMcpMsgHdr.msgLen = len(pMcpMsgHdr.encode()) + len(pMcpPollCmd.encode()) + len(pConditionAck.encode()) - 1 # subtract 1 since the varaible data has at least 1
    pMcpMsgHdr.msgSeq = 1
    pMcpMsgHdr.ackdSeqNum = 1
    packet = pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pConditionAck.encode()
    pMcpMsgHdr.msgCsum = mcpUtilMcpMsgCsum(packet)
    packet = pMcpAddr.encode() + pMcpMsgHdr.encode() + pMcpPollCmd.encode()[:-1] + pConditionAck.encode()
    return packet
