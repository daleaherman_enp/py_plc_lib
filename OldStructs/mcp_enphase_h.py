# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_enphase.h 
#
# ---------------------------------------------------------

#from embeddedstructs import Struct,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# CONSTANTS NOT USED IN THIS FILE
_mcp_enphase_h_ = True
false = 0
true = 1
EXIT_SUCCESS = 0
EXIT_FAILURE = 1
cAppMsgLen = 496
ENP_OK = 0
ENP_ERROR = (-1)
#mMax(a,b) = (((a)>(b))?(a):(b))
#mMin(a,b) = (((a)<(b))?(a):(b))
# ---------------------------------------------------------
#
# STRUCTS 
#
# ---------------------------------------------------------

