# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_sunpower.h 
#
#  Fixed manually by s.davila 8/31/2018
# ---------------------------------------------------------

from embeddedstructs import Struct,Type,Format
from mcp_counters_h import tAuroraDetailedCounters,tPlcCounters,tControllerCnts,tMcpSsiMeasures

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

cImageFormatScrProcloadBinLen = 65536
cImageFormatScrRecLen = 16
cMcpMsgIdNumOf = 40
cL2FrameDataSize = 108
cMcpPollDataDeviceInfo = 0
cMcpPollDataImageInfo = 1
cMcpPollDataAgfInfo = 2
cMcpPollDataInterval = 3
cMcpPollDataCondition = 4
cMcpPollDataCounters = 5
cMcpPollDataPhaseAware = 6
cMcpPollDataSecurity = 7
cMcpPollRspStatusAgfConfig = 0
cMcpPollRspStatusAgfUpdated = 1
cMcpPollRspStatusAgfChecked = 2
cMcpPollRspStatusAgfMatch = 3
cMcpPollRspStatusAgfLoadErr = 4
cMcpPollRspStatusAgfBadType = 5
cMcpPollRspStatusAgfUnExpRec = 6
cMcpPollRspStatusConditionsChanged = 7
cMcpPollFlagRspStatusAgfLoadErr = (1<<cMcpPollRspStatusAgfLoadErr)
cMcpPollFlagRspStatusAgfBadType = (1<<cMcpPollRspStatusAgfBadType)
cMcpPollFlagRspStatusAgfUnExpRec = (1<<cMcpPollRspStatusAgfUnExpRec)
cMcpPollFlagRspStatusConditionsChanged = (1<<cMcpPollRspStatusConditionsChanged)
cMcpAgfRecStatusSupported = 0
cMcpAgfRecStatusLoaded = 1
cMcpAgfRecStatusMetaV1 = 2
cMcpAgfRecStatusMetaV2 = 3
cMcpAgfRecStatusVersionMatch = 4
cMcpAgfRecStatusKeyMatch = 5
cMcpAgfRecStatusCsumMatch = 6
cMcpAgfRecStatusLengthMatch = 7
cMcpAgfRecStatus_Undef_0100 = 8
cMcpAgfRecStatus_Undef_0200 = 9
cMcpAgfRecStatus_Undef_0400 = 10
cMcpAgfRecStatus_Undef_0800 = 11
cMcpAgfRecStatusLoadError = 12
cMcpAgfRecStatusBadRxSize = 13
cMcpAgfRecStatusBadRxCsum = 14
cMcpAgfRecStatusBadRxRec = 15
cMcpAgfRecStatusFlagVersionMatch = ((1<<cMcpAgfRecStatusVersionMatch))
cMcpAgfRecStatusFlagKeyMatch = ((1<<cMcpAgfRecStatusKeyMatch))
cMcpAgfRecStatusFlagCsumMatch = ((1<<cMcpAgfRecStatusCsumMatch))
cMcpAgfRecStatusFlagLengthMatch = ((1<<cMcpAgfRecStatusLengthMatch))
cMcpAgfRecStatusFlagLoadError = ((1<<cMcpAgfRecStatusLoadError))
cMcpAgfRecStatusFlagBadRxSize = ((1<<cMcpAgfRecStatusBadRxSize))
cMcpAgfRecStatusFlagBadRxCsum = ((1<<cMcpAgfRecStatusBadRxCsum))
cMcpAgfRecStatusFlagBadRxRec = ((1<<cMcpAgfRecStatusBadRxRec))
cMcpPwrMpptUnlocked = 0
cMcpPwrSkipCycles = 1
cPartNumIdLen = 4
cAsicIdSize = 8
cSerialNumBytesLen = 6
cFHashLen = 16
#cMcpFastpathDataSize = (cL2FrameDataSize-sizeof(tMcpMsgHdr)-sizeof(tMcpIeHdr)-sizeof(uint32_t)-4)
# SD 08/31/18
tMcpMsgHdr_size = 8
tMcpIeHdr_size = 4
cMcpFastpathDataSize = (cL2FrameDataSize-tMcpMsgHdr_size-tMcpIeHdr_size-4-4)
cSunpowerPvNameLen = 20
cSunpowerPvModelLen = 8
cSunpowerPvManufLen = 8
cL2DomainAddrLen = 6
cMcpImgDirIdxNumOf = 6
cSecDataSize = 128
# SD 08/31/18
#u_int16_t = uint16_t
#u_int32_t = uint32_t

# CONSTANTS NOT USED IN THIS FILE
_mcp_sunpower_h_ = True
PACKING = True
cPartNumInStrFmtLen = 24
cSerialNumStrLen = 25
cMacAddrStrLen = 18
cIpAddrStrLen = 16
cAsicIdStrLen = 25
cImageFormatTchLgBinMaxSz = 131072
cImageFormatScrLgProcloadBinLen = 126976
cImageFormatScrProcloadNumRecs = (cImageFormatScrProcloadBinLen/cImageFormatScrRecLen)
cMcpProtocolVersionFour = 4
cMcpProtocolVersionFive = 5
cMcpProtocolVersionSix = 6
cMcpProtocolVersionSeven = 7
cMcpNumberOf = cMcpMsgIdNumOf
cMD5DigestLen = 16
cMcpImgInfoFlagsAllMatchingDevs = 0x80
cMcpImgInfoFlagsMetadataUpgOnly = 0x40
cMcpImgInfoFlagsParmToOffline = 0x20
cMcpImgInfoFlagsImgRspShort = 0x10
cMcpImgInfoRepCntMsk = 0x03
cMcpFpAgfInvTvlCnt = 2
cMcpPollFlagDeviceInfo = ((1<<cMcpPollDataDeviceInfo))
cMcpPollFlagImageInfo = ((1<<cMcpPollDataImageInfo))
cMcpPollFlagAgfInfo = ((1<<cMcpPollDataAgfInfo))
cMcpPollFlagInterval = ((1<<cMcpPollDataInterval))
cMcpPollFlagCondition = ((1<<cMcpPollDataCondition))
cMcpPollFlagCounters = ((1<<cMcpPollDataCounters))
cMcpPollFlagPhaseAware = ((1<<cMcpPollDataPhaseAware))
cMcpPollFlagSecurity = ((1<<cMcpPollDataSecurity))
cMcpPcuCtlFlagUndef_0x0001 = 0x0001
cMcpPcuCtlFlagPwrProdOff = 0x0002
cMcpPcuCtlFlagClrGfi = 0x0004
cMcpPcuCtlFlagReboot = 0x0008
cMcpPcuCtlFlagCmdAlert = 0x0010
cMcpPcuCtlFlagManuTestMode = 0x0020
cMcpPcuCtlFlagNSyncOff = 0x0040
cMcpPcuCtlFlagUndef_0x0080 = 0x0080
cMcpPcuCtlFlagUndef_0x0100 = 0x0100
cMcpPcuCtlFlagUndef_0x0200 = 0x0200
cMcpPcuCtlFlagUndef_0x0400 = 0x0400
cMcpPcuCtlFlagUndef_0x0800 = 0x0800
cMcpPcuCtlFlagUndef_0x1000 = 0x1000
cMcpPcuCtlFlagUndef_0x2000 = 0x2000
cMcpPcuCtlFlagUndef_0x4000 = 0x4000
cMcpPcuCtlFlagUndef_0x8000 = 0x8000
cMcpPcuCtlFlagNumOf = 16
cMcpPollFlagRspStatusAgfConfig = (1<<cMcpPollRspStatusAgfConfig)
cMcpPollFlagRspStatusAgfUpdated = (1<<cMcpPollRspStatusAgfUpdated)
cMcpPollFlagRspStatusAgfChecked = (1<<cMcpPollRspStatusAgfChecked)
cMcpPollFlagRspStatusAgfMatch = (1<<cMcpPollRspStatusAgfMatch)
cMcpPollFlagClearOnAck = (cMcpPollFlagRspStatusAgfLoadErr|cMcpPollFlagRspStatusAgfBadType|cMcpPollFlagRspStatusAgfUnExpRec|cMcpPollFlagRspStatusConditionsChanged)
cDevInfoFlagsMaskAssoc = 0x0003
cDevInfoFlagsMaskProtocol = 0x003C
cDevInfoFlagsShiftProtocol = 2
cMcpAgfRecStatusFlagSupported = ((1<<cMcpAgfRecStatusSupported))
cMcpAgfRecStatusFlagLoaded = ((1<<cMcpAgfRecStatusLoaded))
cMcpAgfRecStatusFlagMetaV1 = ((1<<cMcpAgfRecStatusMetaV1))
cMcpAgfRecStatusFlagMetaV2 = ((1<<cMcpAgfRecStatusMetaV2))
cMcpAgfRecStatusFlag_Undef_0100 = ((1<<cMcpAgfRecStatus_Undef_0100))
cMcpAgfRecStatusFlag_Undef_0200 = ((1<<cMcpAgfRecStatus_Undef_0200))
cMcpAgfRecStatusFlag_Undef_0400 = ((1<<cMcpAgfRecStatus_Undef_0400))
cMcpAgfRecStatusFlag_Undef_0800 = ((1<<cMcpAgfRecStatus_Undef_0800))
cMcpAgfRecStatusFlagExpChecked = (cMcpAgfRecStatusFlagVersionMatch|cMcpAgfRecStatusFlagKeyMatch|cMcpAgfRecStatusFlagCsumMatch|cMcpAgfRecStatusFlagLengthMatch)
cMcpAgfRecStatusFlagBadRxFlags = (cMcpAgfRecStatusFlagLoadError|cMcpAgfRecStatusFlagBadRxSize|cMcpAgfRecStatusFlagBadRxCsum|cMcpAgfRecStatusFlagBadRxRec)
cMcpPwrFlagMpptUnlocked = ((1<<cMcpPwrMpptUnlocked))
cMcpPwrFlagSkipCycles = ((1<<cMcpPwrSkipCycles))
cMcpPcuCondFlag_0_AcVoltageOOSP1 = 0x00000001
cMcpPcuCondFlag_0_AcVoltageOOSP2 = 0x00000002
cMcpPcuCondFlag_0_AcVoltageOOSP3 = 0x00000004
cMcpPcuCondFlag_0_AcFreqOOR = 0x00000008
cMcpPcuCondFlag_0_GridInstability = 0x00000010
cMcpPcuCondFlag_0_DCVoltageTooLow = 0x00000020
cMcpPcuCondFlag_0_DCVoltageTooHigh = 0x00000040
cMcpPcuCondFlag_0_SkippedCycles = 0x00000080
cMcpPcuCondFlag_0_GFITripped = 0x00000100
cMcpPcuCondFlag_0_ForcedPwrProdOff = 0x00000200
cMcpPcuCondFlag_0_CriticalTemp = 0x00000400
cMcpPcuCondFlag_0_OverTemp = 0x00000800
cMcpPcuCondFlag_0_AlertActive = 0x00001000
cMcpPcuCondFlag_0_RunningOnAC = 0x00002000
cMcpPcuCondFlag_0_GridGone = 0x00004000
cMcpPcuCondFlag_0_BadFlashImage = 0x00008000
cMcpPcuCondFlag_0_UnexpectedReset = 0x00010000
cMcpPcuCondFlag_0_CommandedReset = 0x00020000
cMcpPcuCondFlag_0_PowerOnReset = 0x00040000
cMcpPcuCondFlag_0_HardwareError = 0x00080000
cMcpPcuCondFlag_0_HardwareWarning = 0x00100000
cMcpPcuCondFlag_0_DCPowerTooLow = 0x00200000
cMcpPcuCondFlag_0_BridgeFault = 0x00400000
cMcpPcuCondFlag_0_MpptUnlocked = 0x00800000
cMcpPcuCondFlag_0_GridDCILo = 0x01000000
cMcpPcuCondFlag_0_GridDCIHi = 0x02000000
cMcpPcuCondFlag_0_ROCOF = 0x04000000
cMcpPcuCondFlag_0_ACVAvg = 0x08000000
cMcpPcuCondFlag_0_DCRLow = 0x10000000
cMcpPcuCondFlag_0_AcMonitorErr = 0x20000000
cMcpPcuCondFlag_0_ArcFaultTripped = 0x40000000
cMcpPcuCondFlag_0_Undef_0x80000000 = 0x80000000
cMcpPcuCondFlag_1_Undef_0x00000001 = 0x00000001
cMcpPcuCondFlag_1_Undef_0x00000002 = 0x00000002
cMcpPcuCondFlag_1_Undef_0x00000004 = 0x00000004
cMcpPcuCondFlag_1_Undef_0x00000008 = 0x00000008
cMcpPcuCondFlag_1_Undef_0x00000010 = 0x00000010
cMcpPcuCondFlag_1_Undef_0x00000020 = 0x00000020
cMcpPcuCondFlag_1_Undef_0x00000040 = 0x00000040
cMcpPcuCondFlag_1_Undef_0x00000080 = 0x00000080
cMcpPcuCondFlag_1_Undef_0x00000100 = 0x00000100
cMcpPcuCondFlag_1_Undef_0x00000200 = 0x00000200
cMcpPcuCondFlag_1_Undef_0x00000400 = 0x00000400
cMcpPcuCondFlag_1_Undef_0x00000800 = 0x00000800
cMcpPcuCondFlag_1_Undef_0x00001000 = 0x00001000
cMcpPcuCondFlag_1_Undef_0x00002000 = 0x00002000
cMcpPcuCondFlag_1_Undef_0x00004000 = 0x00004000
cMcpPcuCondFlag_1_Undef_0x00008000 = 0x00008000
cMcpPcuCondFlag_1_Undef_0x00010000 = 0x00010000
cMcpPcuCondFlag_1_Undef_0x00020000 = 0x00020000
cMcpPcuCondFlag_1_Undef_0x00040000 = 0x00040000
cMcpPcuCondFlag_1_Undef_0x00080000 = 0x00080000
cMcpPcuCondFlag_1_Undef_0x00100000 = 0x00100000
cMcpPcuCondFlag_1_Undef_0x00200000 = 0x00200000
cMcpPcuCondFlag_1_Undef_0x00400000 = 0x00400000
cMcpPcuCondFlag_1_Undef_0x00800000 = 0x00800000
cMcpPcuCondFlag_1_Undef_0x01000000 = 0x01000000
cMcpPcuCondFlag_1_ACVoltageLow = 0x02000000
cMcpPcuCondFlag_1_ACVoltageHigh = 0x04000000
cMcpPcuCondFlag_1_ACFrequencyLow = 0x08000000
cMcpPcuCondFlag_1_ACFrequencyHigh = 0x10000000
cMcpPcuCondFlag_1_AGFPowerLimiting = 0x20000000
cMcpPcuCondFlag_1_DcBridgeHwProtection = 0x40000000
cMcpPcuCondFlag_1_Undef_0x80000000 = 0x80000000
cMcpPcuUnlatchedFlag_TempPwrLimit = 0x00000001
cMcpPcuUnlatchedFlag_SoftStartPwrLimit = 0x00000002
cMcpPcuUnlatchedFlag_FpfPwrLimit = 0x00000004
cMcpPcuUnlatchedFlag_VrmsPwrLimit = 0x00000008
cMcpPcuUnlatchedFlag_Inv2PwrLimit = 0x00000010
cMcpPcuUnlatchedFlag_Fw21PwrLimit = 0x00000020
cMcpPcuUnlatchedFlag_Vw51PwrLimit = 0x00000040
cMcpPcuUnlatchedFlag_DcLowPwrLimit = 0x00000080
cMcpPcuUnlatchedFlag_Wp41Enable = 0x00000100
cMcpPcuUnlatchedFlag_Tv31Enable = 0x00000200
cMcpPcuUnlatchedFlag_FpfEnable = 0x00000400
cMcpPcuUnlatchedFlag_VvarEnable = 0x00000800
cMcpPcuUnlatchedFlag_Inv2Enable = 0x00001000
cMcpPcuUnlatchedFlag_Fw21Enable = 0x00002000
cMcpPcuUnlatchedFlag_Vw51Enable = 0x00004000
cMcpPcuUnlatchedFlag_Undef_0x00008000 = 0x00008000
C_MCP_PCU_COND_FLAG_0_STRINGS = {"AcVoltageOOSP1","AcVoltageOOSP2","AcVoltageOOSP3","AcFreqOOR","GridInstability","DCVoltageTooLow","DCVoltageTooHigh","SkippedCycles","GFITripped","ForcedPwrProdOff","CriticalTemp","OverTemp","AlertActive","RunningOnAC","GridGone","BadFlashImage","UnexpectedReset","CommandedReset","PowerOnReset","HardwareError","HardwareWarning","DCPowerTooLow","BridgeFault","MpptUnlocked","GridDCILo","GridDCIHi","ROCOF","ACVAvg","DCRLow","AcMonitorErr","ArcFaultTripped","Undef_0x80000000"}
C_MCP_PCU_COND_FLAG_1_STRINGS = {"Undef_0x00000001","Undef_0x00000002","Undef_0x00000004","Undef_0x00000008","Undef_0x00000010","Undef_0x00000020","Undef_0x00000040","Undef_0x00000080","Undef_0x00000100","Undef_0x00000200","Undef_0x00000400","Undef_0x00000800","Undef_0x00001000","Undef_0x00002000","Undef_0x00004000","Undef_0x00008000","Undef_0x00010000","Undef_0x00020000","Undef_0x00040000","Undef_0x00080000","Undef_0x00100000","Undef_0x00200000","Undef_0x00400000","Undef_0x00800000","Undef_0x01000000","ACVoltageLow","ACVoltageHigh","ACFrequencyLow","ACFrequencyHigh","AGFPowerLimiting","DcBridgeHwProtection","Undef_0x80000000"}
C_MCP_PCU_UNLATCHED_FLAG_VALID_MASK = 0x0000FFFF
C_MCP_PCU_UNLATCHED_FLAG_STRINGS = {"TempPwrLimit","SoftStartPwrLimit","FpfPwrLimit","VrmsPwrLimit","Inv2PwrLimit","Fw21PwrLimit","Vw51PwrLimit","DcLowPwrLimit","Wp41Enable","Tv31Enable","FpfEnable","VvarEnable","Inv2Enable","Fw21Enable","Vw51Enable","Undef_0x00008000",}
cnPDevTypeNone = 0
cnPDevTypePcu = 1
cnPDevTypePcu2 = 2
cnPDevTypeCmu = 3
cnPDevTypeEmu = 4
cnPDevTypePmu = 5
cnPDevTypeAmu = 6
cnPDevTypeTherm = 7
cnPDevTypeRgm = 8
cnPDevTypeZbrptr = 9
cnPDevTypeEim = 10
cnPDevTypeAcb = 11
cnPDevTypeNumOf = 12
cMcpMsgIdNone = 0
cMcpMsgIdAckInterval = 1
cMcpMsgIdAckDevInfo = 2
cMcpMsgIdAckSimple = 3
cMcpMsgIdPoll = 4
cMcpMsgIdScan = 5
cMcpMsgIdDomainCtl = 6
cMcpMsgIdInterval = 7
cMcpMsgIdDevInfo = 8
cMcpMsgIdImgInventory = 9
cMcpMsgIdImgInfo = 10
cMcpMsgIdImgReq = 11
cMcpMsgIdImgRsp = 12
cMcpMsgIdDebug = 13
cMcpMsgIdTripPointInfo = 14
cMcpMsgIdAckTripPointInfo = 15
cMcpMsgIdPlcCfg = 16
cMcpMsgIdSecInfo = 17
cMcpMsgIdAckSecInfo = 18
cMcpMsgIdMsgErr = 19
cMcpMsgIdAgfAdcData = 20
cMcpMsgIdAgfAdcInfo = 21
cMcpMsgIdAgfAdcReq = 22
cMcpMsgIdIntervalIe = 23
cMcpMsgIdAckIntervalIe = 24
cMcpMsgIdEventIe = 25
cMcpMsgIdAckEventIe = 26
cMcpMsgIdMsgCapIe = 27
cMcpMsgIdFastpath = 28
cMcpMsgIdPaCapture = 29
cMcpMsgIdPaInfo = 30
cMcpMsgIdDevInfoIe = 31
cMcpMsgIdNonSecure = 32
cMcpMsgIdSysConfig = 33
cMcpMsgIdPollCmdTlv = 34
cMcpMsgIdPollRspTlv = 35
cMcpMsgIdAgfRecords = 36
cMcpMsgIdExpAgfRecordTlv = 37
cMcpMsgIdAssociation = 38
cMcpMsgIdMfg = 39
cMcpImageTypeNone = 0
cMcpImageTypeProcLoad = 1
cMcpImageTypePwrCondTbl = 2
cMcpImageTypeManuData = 3
cMcpImageTypeParm = 4
cMcpImageTypeParm0 = 5
cMcpImageTypeParm1 = 6
cMcpImageTypeNumOf = 7
cMcpFpIdNone = 0
cMcpFpIdAgfInvMsg = 1
cMcpFpIdAgfInvTvlMsg = 2
cMcpFpIdNumOf = 3
cMcpFpTlvInvNone = 0
cMcpFpTlvInvPcu = 1
cMcpFpTlvInvAcb = 2
cMcpFpTlvInvNumOf = 3
cMcpPollDataNumberOf = 8
cMcpPollCmdNone = 0
cMcpPollDiscover = 1
cMcpPollCmdControlFlags = 2
cMcpPollConditionAcknowledge = 3
cMcpPollSetGroupMask = 4
cMcpPollSendPollData = 5
cMcpPollCmdNumberOf = 6
cMcpPollRspStatusNumberOf = 8
cMcpNewAssocStatusNone = 0
cMcpNewAssocStatusSquelch = 1
cMcpNewAssocStatusAssociated = 2
cMcpNewAssocStatusNumOf = 3
cMcpImgDirIdxPwrCondTbl = 0
cMcpImgDirIdxProcLoad0 = 1
cMcpImgDirIdxProcLoad1 = 2
cMcpImgDirIdxParm0 = 3
cMcpImgDirIdxParm1 = 4
cMcpImgDirIdxManuData = 5
cMcpImgRunningImg0Img1Bad = 0
cMcpImgRunningImg1Img0Bad = 1
cMcpImgRunningImg0Img1Ok = 2
cMcpImgRunningImg1Img0Ok = 3
cMcpImgRunningIndNumOf = 4
cMcpAgfRecStatusNumberOf = 16
cMcpAgfMeta_V1 = 1
cMcpAgfMeta_V2 = 2
cMcpAgfMetaNumOf = 3
cAgfFunctionTypeCookie = 0
cAgfFunctionTypeVVAR = 1
cAgfFunctionTypeFRT = 2
cAgfFunctionTypeVRT = 3
cAgfFunctionTypeFPF = 4
cAgfFunctionTypePRL = 5
cAgfFunctionTypePLP = 6
cAgfFunctionTypeVW = 7
cAgfFunctionTypeINV2 = 8
cAgfFunctionTypeWP = 9
cAgfFunctionTypeTV = 10
cAgfFunctionTypeFW = 11
cAgfFunctionTypeSS = 12
cAgfFunctionTypeISLND = 13
cAgfFunctionTypeNotUsed_1 = 14
cAgfFunctionTypeIAC = 15
cAgfFunctionTypeVECT = 16
cAgfFunctionTypeROCOF = 17
cAgfFunctionTypeACAVE = 18
cAgfFunctionTypeVW52 = 19
cAgfFunctionTypeFW22 = 20
cAgfFunctionTypeWVAR = 21
cAgfFunctionTypeNumOf = 22
cMcpAssocCmdNone = 0
cMcpAssocCmdSquelch = 1
cMcpAssocCmdLoad = 2
cMcpAssocCmdForgetAll = 3
cMcpAssocCmdForgetSpecific = 4
cMcpAssocCmdNumberOf = 5
cMcpMfgCmdStart = 0
cMcpMfgCmdLoad = 1
cMcpMfgCmdStop = 2
cMcpMfgCmdClearAgfRecords = 3
cMcpMfgCmdNumberOf = 4
# ---------------------------------------------------------
#
# STRUCTS 
#
# ---------------------------------------------------------



#	tPartNumber

class tPartNumber(Struct):
	_format = Format.BigEndian
	idb = Type.UnsignedByte[cPartNumIdLen]
	manuRev = Type.UnsignedByte
	dMajor = Type.UnsignedByte
	dMinor = Type.UnsignedByte
	dMaint = Type.UnsignedByte


#	tAsicId

class tAsicId(Struct):
	_format = Format.BigEndian
	asicId = Type.UnsignedByte[cAsicIdSize]


#	tSerialNumber

class tSerialNumber(Struct):
	_format = Format.BigEndian
	byteArr = Type.UnsignedByte[cSerialNumBytesLen]
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte


#	tMcpIeHdr

class tMcpIeHdr(Struct):
	_format = Format.BigEndian
	version = Type.UnsignedByte
	rsvd_ie = Type.UnsignedByte
	len = Type.UnsignedShort


#	tMcpMsgHdr

class tMcpMsgHdr(Struct):
	_format = Format.BigEndian
	msgCsum = Type.UnsignedShort
	msgLen = Type.UnsignedShort
	ctrlAndMcpProtoVers = Type.UnsignedByte
	msgId = Type.UnsignedByte
	msgSeq = Type.UnsignedByte
	ackdSeqNum = Type.UnsignedByte


#	tMcpAckSimpleMsg

class tMcpAckSimpleMsg(Struct):
	_format = Format.BigEndian
	ackStatus = Type.UnsignedByte
	fillb3 = Type.UnsignedByte
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte


#	tImageIMeta

class tImageIMeta(Struct):
	_format = Format.BigEndian
	imagePartNum = Type.Struct(tPartNumber)
	imageType = Type.UnsignedByte
	compatNum = Type.UnsignedByte
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte
	imgfHash = Type.UnsignedByte[cFHashLen]


#	tImageTMeta

class tImageTMeta(Struct):
	_format = Format.BigEndian
	targetLen = Type.UnsignedLong
	targetCrc = Type.UnsignedShort
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte


#	tImageTargetMeta

class tImageTargetMeta(Struct):
	_format = Format.BigEndian
	crcForMetadata = Type.UnsignedShort
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte
	loadTimeValHi = Type.Long
	loadTimeValLo = Type.UnsignedLong
	imeta = Type.Struct(tImageIMeta)
	tmeta = Type.Struct(tImageTMeta)


#	tMcpImageReqMsg

class tMcpImageReqMsg(Struct):
	_format = Format.BigEndian
	devPartNum = Type.Struct(tPartNumber)
	offset = Type.UnsignedLong
	count = Type.UnsignedShort
	devType = Type.UnsignedByte
	fillb1 = Type.UnsignedByte
	imeta = Type.Struct(tImageIMeta)


#	tMcpImageInfoMsg

class tMcpImageInfoMsg(Struct):
	_format = Format.BigEndian
	devPartNum = Type.Struct(tPartNumber)
	devType = Type.UnsignedByte
	isBcastDl = Type.UnsignedByte
	forceReload = Type.UnsignedByte
	flagsAndRepCnt = Type.UnsignedByte
	meta = Type.Struct(tImageTargetMeta)


#	tMcpImageDataToFlash

class tMcpImageDataToFlash(Struct):
	_format = Format.BigEndian
	offset = Type.UnsignedLong
	count = Type.UnsignedShort
	devType = Type.UnsignedByte
	imgDataOffsetLen2Add = Type.UnsignedByte
	imeta = Type.Struct(tImageIMeta)


#	tImageIMetaShort

class tImageIMetaShort(Struct):
	_format = Format.BigEndian
	imagePartNum = Type.Struct(tPartNumber)
	imageType = Type.UnsignedByte
	compatNum = Type.UnsignedByte
	fillb2 = Type.UnsignedByte
	fillb1 = Type.UnsignedByte


#	tMcpImageDataToFlashShort

class tMcpImageDataToFlashShort(Struct):
	_format = Format.BigEndian
	offset = Type.UnsignedLong
	count = Type.UnsignedShort
	devType = Type.UnsignedByte
	imgDataOffsetLen2Add = Type.UnsignedByte
	imetaShort = Type.Struct(tImageIMetaShort)


#	tMcpImageRspMsg

class tMcpImageRspMsg(Struct):
	_format = Format.BigEndian
	devPartNum = Type.Struct(tPartNumber)
	offset = Type.UnsignedLong
	count = Type.UnsignedShort
	devType = Type.UnsignedByte
	fillb1 = Type.UnsignedByte
	imeta = Type.Struct(tImageIMeta)


#	tMcpImageRspShortMsg

class tMcpImageRspShortMsg(Struct):
	_format = Format.BigEndian
	devPartNum = Type.Struct(tPartNumber)
	offset = Type.UnsignedLong
	count = Type.UnsignedShort
	devType = Type.UnsignedByte
	fillb1 = Type.UnsignedByte
	imetaShort = Type.Struct(tImageIMetaShort)


#	tMcpFastpathTlv

class tMcpFastpathTlv(Struct):
	_format = Format.BigEndian
	type = Type.UnsignedByte
	length = Type.UnsignedByte


#	tMcpFastpathIe

class tMcpFastpathIe(Struct):
	_format = Format.BigEndian
	ieHdr = Type.Struct(tMcpIeHdr)
	grpMask = Type.UnsignedLong
	fpId = Type.UnsignedByte
	tlvCnt = Type.UnsignedByte
	dynSeqNum = Type.UnsignedShort
	fpData = Type.UnsignedByte[cMcpFastpathDataSize]


#	tMcpTlvHeader

class tMcpTlvHeader(Struct):
	_format = Format.BigEndian
	type = Type.UnsignedByte
	length = Type.UnsignedShort


#	tMcpAgfRecHeader

class tMcpAgfRecHeader(Struct):
	_format = Format.BigEndian
	type = Type.UnsignedByte
	version = Type.UnsignedByte
	recLength = Type.UnsignedShort
	key = Type.UnsignedShort
	recCsum = Type.UnsignedShort


#	tMcpAgfExpRecordsTlv

class tMcpAgfExpRecordsTlv(Struct):
	_format = Format.BigEndian
	msgTime = Type.UnsignedLong
	grpMask = Type.UnsignedLong
	count = Type.UnsignedByte
	value = Type.UnsignedByte[1]


#	tMcpAgfRecord

class tMcpAgfRecord(Struct):
	_format = Format.BigEndian
	recordHeader = Type.Struct(tMcpAgfRecHeader)
	recordData = Type.UnsignedByte[1]


#	tMcpAgfRecords

class tMcpAgfRecords(Struct):
	_format = Format.BigEndian
	msgTime = Type.UnsignedLong
	grpMask = Type.UnsignedLong
	count = Type.UnsignedByte
	value = Type.UnsignedByte[1]


#	tMcpPollCmdControlFlagsTlv

class tMcpPollCmdControlFlagsTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	controlFlags = Type.UnsignedShort
	periodInSecs = Type.UnsignedShort


#	tMcpPollCmdConditionAcknowledgeTlv

class tMcpPollCmdConditionAcknowledgeTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	sequenceNumber = Type.UnsignedByte


#	tMcpPollCmdSendPollDataTlv

class tMcpPollCmdSendPollDataTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	devSerialNum = Type.Struct(tSerialNumber)


#	tMcpPollCmdSendGroupMaskTlv

class tMcpPollCmdSendGroupMaskTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	devSerialNum = Type.Struct(tSerialNumber)
	grpMask = Type.UnsignedLong


#	tMcpPollCmdDiscoverTlv

class tMcpPollCmdDiscoverTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	fill = Type.UnsignedByte


#	tMcpPollCmdTlv

class tMcpPollCmdTlv(Struct):
	_format = Format.BigEndian
	msgTime = Type.UnsignedLong
	pollReqFlags = Type.UnsignedShort
	sequenceNumber = Type.UnsignedByte
	tlvCnt = Type.UnsignedByte
	tlvData = Type.UnsignedByte[1]


#	tMcpPollRspTlv

class tMcpPollRspTlv(Struct):
	_format = Format.BigEndian
	msgTime = Type.UnsignedLong
	pollRspFlags = Type.UnsignedShort
	pollRspStatus = Type.UnsignedShort
	sequenceNumber = Type.UnsignedByte
	tlvCnt = Type.UnsignedByte
	tlvData = Type.UnsignedByte[1]


#	tEEPROM_PV_MODULE_ENTRY

class tEEPROM_PV_MODULE_ENTRY(Struct):
	_format = Format.BigEndian
	pv_name = Type.UnsignedByte[cSunpowerPvNameLen]
	pv_model = Type.UnsignedByte[cSunpowerPvModelLen]
	pv_manuf = Type.UnsignedByte[cSunpowerPvManufLen]


#	tMcpPollRspDevInfo

class tMcpPollRspDevInfo(Struct):
	_format = Format.BigEndian
	devSerialNum = Type.Struct(tSerialNumber)
	devPartNum = Type.Struct(tPartNumber)
	devAssemblyNum = Type.Struct(tPartNumber)
	asicId = Type.Struct(tAsicId)
	moduleEntry = Type.Struct(tEEPROM_PV_MODULE_ENTRY)
	domainAddr = Type.UnsignedByte[cL2DomainAddrLen]
	devType = Type.UnsignedByte
	modAddr = Type.UnsignedShort
	grpMask = Type.UnsignedLong
	rxEmuSsi = Type.UnsignedShort
	flags = Type.UnsignedShort


#	tMcpPollRspDevInfoTlv

class tMcpPollRspDevInfoTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	devInfo = Type.Struct(tMcpPollRspDevInfo)


#	tImageMetaData

class tImageMetaData(Struct):
	_format = Format.BigEndian
	procImgIdxRunning = Type.UnsignedByte
	metaBadBits = Type.UnsignedByte
	imageBadBits = Type.UnsignedByte
	imgCount = Type.UnsignedByte
	meta = Type.Struct(tImageTargetMeta)[cMcpImgDirIdxNumOf]


#	tMcpPollRspImageInfoTlv

class tMcpPollRspImageInfoTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	imd = Type.Struct(tImageMetaData)


#	tMcpAgfLoadedRecStatus

class tMcpAgfLoadedRecStatus(Struct):
	_format = Format.BigEndian
	recordHeader = Type.Struct(tMcpAgfRecHeader)
	recStatus = Type.UnsignedShort


#	tMcpPollRspAgfRecListTlv

class tMcpPollRspAgfRecListTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	recList = Type.Struct(tMcpAgfLoadedRecStatus)[1]


#	tMcpPcuData

class tMcpPcuData(Struct):
	_format = Format.BigEndian
	pwrConvErrSecs = Type.UnsignedByte
	pwrConvMaxErrCycles = Type.UnsignedByte
	producedMilliJoules = Type.UnsignedShort[3]
	acVoltageINmV = Type.UnsignedLong
	acFrequencyINClkCycles = Type.UnsignedLong
	dcVoltageINmV = Type.UnsignedLong
	dcCurrentINmA = Type.UnsignedLong
	usedMilliJoules = Type.UnsignedShort[3]
	sumMilliLeadingVAr = Type.UnsignedShort[3]
	sumMilliLaggingVAr = Type.UnsignedShort[3]


#	tMcpPcuPwrIntervalData

class tMcpPcuPwrIntervalData(Struct):
	_format = Format.BigEndian
	temperature = Type.Byte
	flags = Type.UnsignedByte
	onTime = Type.UnsignedLong
	pwr = Type.Struct(tMcpPcuData)


#	tMcpPollRspIntervalTlv

class tMcpPollRspIntervalTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	intervalData = Type.Struct(tMcpPcuPwrIntervalData)


#	tMcpConditionData

class tMcpConditionData(Struct):
	_format = Format.BigEndian
	cmdSequenceNumber = Type.UnsignedByte
	lastMsgTime = Type.UnsignedLong
	latchedConditions = Type.UnsignedLong[2]
	fill = Type.UnsignedByte[4]
	unlatchedConditions = Type.UnsignedLong


#	tMcpPollRspConditionTlv

class tMcpPollRspConditionTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	conditionData = Type.Struct(tMcpConditionData)


#	tMcpCounterData

class tMcpCounterData(Struct):
	_format = Format.BigEndian
	modemCounters = Type.Struct(tAuroraDetailedCounters)
	frameCounters = Type.Struct(tPlcCounters)
	msgCounter = Type.Struct(tControllerCnts)
	ssiMeasures = Type.Struct(tMcpSsiMeasures)


#	tMcpPollRspCounterTlv

class tMcpPollRspCounterTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	counters = Type.Struct(tMcpCounterData)


#	tPaInfo

class tPaInfo(Struct):
	_format = Format.BigEndian
	phaseDelta = Type.UnsignedShort
	phaseBin = Type.UnsignedByte
	phaseAwarenessSeqNum = Type.UnsignedByte


#	tMcpPollRspPhaseAwareTlv

class tMcpPollRspPhaseAwareTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	paData = Type.Struct(tPaInfo)


#	tSecurityData

class tSecurityData(Struct):
	_format = Format.BigEndian
	blob = Type.UnsignedShort[cSecDataSize]


#	tMcpPollRspSecurityTlv

class tMcpPollRspSecurityTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	secData = Type.Struct(tSecurityData)


#	tMcpAssociationLoadData

class tMcpAssociationLoadData(Struct):
	_format = Format.BigEndian
	devSerialNum = Type.Struct(tSerialNumber)
	domainAddr = Type.UnsignedByte[cL2DomainAddrLen]
	modAddr = Type.UnsignedShort


#	tMcpAssociationLoadTlv

class tMcpAssociationLoadTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	load = Type.Struct(tMcpAssociationLoadData)


#	tMcpAssociationForgetData

class tMcpAssociationForgetData(Struct):
	_format = Format.BigEndian
	domainAddr = Type.UnsignedByte[cL2DomainAddrLen]


#	tMcpAssociationForgetTlv

class tMcpAssociationForgetTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	forget = Type.Struct(tMcpAssociationForgetData)


#	tMcpAssociationForgetAllTlv

class tMcpAssociationForgetAllTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)


#	tMcpAssociationSquelchData

class tMcpAssociationSquelchData(Struct):
	_format = Format.BigEndian
	devSerialNum = Type.Struct(tSerialNumber)
	seconds = Type.UnsignedShort


#	tMcpAssociationSquelchTlv

class tMcpAssociationSquelchTlv(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	squelch = Type.Struct(tMcpAssociationSquelchData)


#	tMcpAssociationTlv

class tMcpAssociationTlv(Struct):
	_format = Format.BigEndian
	tlvCnt = Type.UnsignedByte
	tlvData = Type.UnsignedByte[1]


#	tMcpMfgStartData

class tMcpMfgStartData(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	fill = Type.UnsignedByte[1]


#	tMcpMfgGenericData

class tMcpMfgGenericData(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	fill = Type.UnsignedByte[1]


#	tMcpMfgLoadData

class tMcpMfgLoadData(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	moduleEntry = Type.Struct(tEEPROM_PV_MODULE_ENTRY)


#	tMcpMfgStopData

class tMcpMfgStopData(Struct):
	_format = Format.BigEndian
	tlvHdr = Type.Struct(tMcpTlvHeader)
	fill = Type.UnsignedByte[1]


#	tMcpMfgTlv

class tMcpMfgTlv(Struct):
	_format = Format.BigEndian
	devSerialNum = Type.Struct(tSerialNumber)
	tlvCnt = Type.UnsignedByte
	tlvData = Type.UnsignedByte[1]
