# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 15:50:07 2018

@author: sdavila
"""

import numbers
import os
import winsound

defines = {}
structs = {}
unions = {}
#g_defines = {}
#g_structs = {}
#g_unions = {}
typedefs = {}

frequency = 2500
duration = 200

namedTypes = {
    "char" : "Char",
    "int8_t" : "Byte",
    "int16_t" : "Short",
    "u_int16_t" : "UnsignedShort",
    "int32_t" : "Long",
    "u_int32_t" : "UnsignedLong",
    "u_int8_t" : "UnsignedByte",
    "bool" : "Byte",
    "in_port_t" : "UnsignedShort",
    "ssize_t" : "UnsignedShort",
    "time_t" : "UnsignedLong",
    "uint8_t" : "UnsignedByte",
    "uint16_t" : "UnsignedShort",
    "uint32_t" : "UnsignedLong",
    "uint64_t" : "UnsignedLongLong"
    }

choices = [
    'Char' ,
    'Byte' ,
    'UnsignedByte',
    'Int',
    'UnsignedInt',
    'Short',
    'UnsignedShort' ,
    'Long',
    'UnsignedLong',
    'String',
    'PascalString' ,
    'Pointer' ,
    'Float' ,
    'Double' ,
    'LongLong' ,
    'UnsignedLongLong'
    ]

common_sizes = {
    'Char':"(1)" ,
    'Byte':"(1)",
    'UnsignedByte':"(1)",
    'Int':"(4)",
    'UnsignedInt':"(4)",
    'Short':"(2)",
    'UnsignedShort':"(2)" ,
    'Long':"(4)",
    'UnsignedLong':"(4)",
    'Float':"(4)" ,
    'Double' :"(8)",
    'LongLong':"(8)" ,
    'UnsignedLongLong':"(8)"
    }


order = []  # ordered list of ids

def remove_comments(lines):
     pos = 0
     level = 0
     outlines = ''
     skiptoend = False
     fuse = False
     while pos < len(lines):
        if lines[pos] == '/' and lines[pos+1] == '*':
             level += 1
             pos += 2
             continue
        if lines[pos] == '*' and lines[pos+1] == '/':
             level -= 1
             if level < 0:
                 level = 0
             pos += 2
             continue
        if lines[pos] == '/' and lines[pos+1] == '/':
            skiptoend = True
            pos += 2
            continue
        if lines[pos] == '\\':
            fuse = True
            pos += 1
            continue
        if lines[pos] == '\n':
            skiptoend = False
        if not skiptoend and (level == 0):
            if lines[pos] == '\n' and fuse:
                fuse = False
                pos += 1
                continue
            outlines += lines[pos]
        pos += 1
     return outlines
 

def replace_keywords(token):
    if token not in ["None"]:
        return token
    winsound.Beep(frequency,duration)
    return token+"_"

def replace_sizeof(symbols,file,x): # replace 'sizeof(<struct_name>)' with '(<struct_name>._struct_size)'
    loc = x.find("sizeof")
    if loc < 0:
        return x
    loc2 = x[loc:].find("(")
    if loc2 < 0:
        return x
    loc3 = x[loc+loc2:].find(")")
    if loc3 < 0:
        return x
    
    structcall,dummy = resolve_type(symbols,x[loc+loc2+1:loc+loc2+loc3].strip(),file,0)
    if structcall.startswith("Struct(") or structcall.startswith("Union("): 
        loc4 = structcall.find(")")
        repl = "("+structcall[7:loc4].strip()+"()._struct_size)"
    else:
        repl = common_sizes[structcall]
    
    
    
    x = x[:loc] + repl + x[loc+loc2+loc3+1:]
    
    print(x)

    return replace_sizeof(symbols,file,x)

 
def process_preprocessor(line):
    global defines
    if not line.startswith("#define"):
        return
    in_tokens = line.split();
    tokens = []
    for token in in_tokens:
        tokens.append(replace_keywords(token))
        
    if len(tokens) < 2:
        return
    
    if tokens[1].find("(") >= 0:  # Skip C macros
        return
    
    if len(tokens) == 2:
        defines[tokens[1].strip()] = True
    if len(tokens) >= 3:
        rest = "".join(tokens[2:])
        defines[tokens[1].strip()] = rest
    if tokens[1].strip() not in order:
        order.append(tokens[1].strip())
        
def absorb_brackets(tokens):  # Ignore C++ methods inside structs
    while len(tokens) > 0:
        if tokens[0] == '}':
            return tokens[1:]
        if tokens[0] == '{':
            tokens = absorb_brackets(tokens[1:])
            continue
        tokens = tokens[1:]

def absorb_parens(tokens):  # Ignore C++ methods inside structs
    while len(tokens) > 0:
        if tokens[0] == ')':
            return tokens[1:]
        if tokens[0] == '(':
            tokens = absorb_parens(tokens[1:])
            continue
        tokens = tokens[1:]
    
 
def process_member(tokens,members,level):
    
    if tokens[0] in ["struct","union"]:
        tokens,member = process_typedef(tokens,level+1)
        members.append(member)
        return tokens
#        print("v")
#        tokens = tokens[1:]
#        while len(tokens) > 0:
#            if tokens[0].strip() == "{":
#                tokens = tokens[1:]
#                break
#            tokens = tokens[1:]
#        while len(tokens) > 0:
#            if tokens[0].strip() == "}":
#                tokens = tokens[1:]
#                break
#            tokens = process_member(tokens,members,level)
#        
#        while len(tokens) > 0:
#            if tokens[0].strip() == ';':
#                print("^")
#                return tokens[1:]
#            tokens = tokens[1:]
            
            
    member_type = tokens[0]
    print(member_type,"->",end="")
    tokens = tokens[1:]
    member_name = ''
    while len(tokens) > 0:
        print(tokens[0])
        if tokens[0].endswith("{"):
            return absorb_brackets(tokens[1:])
        if tokens[0].endswith("("):
            return absorb_parens(tokens[1:])
        if tokens[0] == ';':
            loc = member_name.strip().find("[")
            if  loc > 0:
                mname = member_name.strip()[:loc]
                msize = member_name.strip()[loc:]
            else:
                mname = member_name
                msize = None
            loc = member_name.strip().find(':')
            if loc > 0:  # A bitfield
                member_type += member_name[loc:].strip()
                mname = mname[:loc]
            if "(" not in mname and ")" not in mname: 
                members.append((mname,member_type,msize))
            return tokens[1:]
        member_name += tokens[0]
        tokens = tokens[1:]
    return tokens
    

anonymous = 0

def process_struct(tokens,level=0,union=False):
    global anonymous
    
    members = []
    
    sname = None
    
    while len(tokens) > 0:
        if tokens[0] == ':' and tokens[1] == "public":
            print(tokens[2],"******^^^^^^^^")
            members.append(("embed_"+tokens[2],tokens[2],None))
            tokens = tokens[3:]
            continue
        if tokens[0].strip() == "{":
            tokens = tokens[1:]
            break
        sname = tokens[0]
        tokens = tokens[1:]
        
    
    while len(tokens) > 0:
        if tokens[0].strip() == "}":
            tokens = tokens[1:]
            break
        tokens = process_member(tokens,members,level)
        
    while tokens[0] != ';':
        if tokens[0] not in ["__attribute__","((__packed__))","__attribute__((packed))","__extension__"]:
            sname = tokens[0]
        tokens = tokens[1:]
        
#    if tokens[0].strip() not in order:
#        order.append(tokens[0].strip())
#        
#    if union:
#        unions[tokens[0].strip()] = members;
#    else:
#        structs[tokens[0].strip()] = members
#    return tokens[2:]
        
    for tpl in members:  # Not a packable structure if it is a pointer type
        if tpl[0].strip().find("*") >= 0:  # If member name includes an '*'
            return tokens[1:],None
        if tpl[1].strip().find("*") >= 0: # If type includes a '*'
            return tokens[1:],None
        
    if level > 0:
        if union:
            embed = "Union_Anon_"+str(anonymous)+"_t"
            unions[embed] = members
        else:
            embed = "Struct_Anon_"+str(anonymous)+"_t"
            structs[embed] = members
        if embed not in order:  # And it shouldn't be
            order.append(embed)
        anonymous += 1
        return tokens[1:],(sname,embed,None)
        
    if sname.strip() not in order:
        order.append(sname.strip())
        
    if union:
        unions[sname.strip()] = members;
    else:
        structs[sname.strip()] = members
    return tokens[1:],None

def process_enum_member(tokens,inval,debug=False):
    
    pos = 0
    
    if debug:
        print("*",tokens[0],"*",tokens[1],"*",tokens[2],"*",tokens[3])
    
    while len(tokens) > 0:
        if tokens[pos] in ",}":
            break;
        pos += 1
        
    if tokens[0].strip() not in order:
        if debug:
            print(tokens[0],"<-")
        order.append(tokens[0].strip())
        
    if pos == 1:
        defines[tokens[0].strip()] = inval
        if tokens[1]  in ",":
            tokens = tokens[1:]
        return tokens[1:],inval+1
    
    if pos > 2 and tokens[1] == "=":
        valstr = "".join(tokens[2:pos])
        value = inval
        try:
            value = int(valstr)
        except:
            if valstr.strip() in defines:
                try:
                    value = int(defines[valstr.defines])
                except:
                    pass
        defines[tokens[0].strip()] = value
        tokens = tokens[pos:]
        if tokens[0] == ',':
            tokens = tokens[1:]
        return tokens,value + 1
        
    return tokens, inval

def process_enum(tokens):
    ename = None
    etype = None
    dots_found = False
    while len(tokens) > 0:
        if tokens[0].strip() == "{":
            tokens = tokens[1:]
            break
        if tokens[0].strip() == ":":
            dots_found = True
            tokens = tokens[1:]
            continue
        if not dots_found:
            ename = tokens[0].strip()
        else:
            etype = tokens[0].strip()
        
        tokens = tokens[1:]
        
    value = 0
    
    while len(tokens) > 0:
        if tokens[0].strip() == "}":
            if ename is not None:
                if etype is not None:
                    typedefs[ename] = etype
                else:
                    typedefs[ename] = "uint8_t"
            return tokens[1:]
        tokens,value = process_enum_member(tokens,value)

            
def process_typedef(tokens,level=0):
    if tokens[0].strip() == "struct":
        print("s",end="")
        return process_struct(tokens[1:],level)
    if tokens[0].strip() == "union":
        print("u",end="")
        return process_struct(tokens[1:],level,union=True)
    if tokens[0].strip() == "enum":
        print("e",end="")
        return process_enum(tokens[1:]),None
    
    pos = 0
    while tokens[pos] != ';':
        pos += 1
    typedefs[tokens[pos-1].strip()] = " ".join(tokens[0:pos-1])
    if tokens[pos-1].strip() not in order:
        order.append(tokens[pos-1].strip())
    return tokens[pos+1:],None # 0 is base, 1 is typename, 2 is ;


def resolve_type(symbols,intyp,infile,offset):
    tries = []
    tries.append(intyp)
    
    found = True
    
    while found:
        found = False
        for fn in symbols:
            if intyp in symbols[fn]["typedefs"]:
                if symbols[fn]["typedefs"][intyp] in tries:
                    continue
                found = True
                intyp = symbols[fn]["typedefs"][intyp]
                tries.append(intyp)
        
    for t in tries:
        loc = t.find(":")
        if loc < 0:
            if t in namedTypes:
                return namedTypes[t],offset
        elif t.startswith("uint") or t.startswith("int"):  #  This probably looks like uint32_t:8 or the like
            bits = int(t[loc+1:])
            loc1 = t.find("int")
            loc2 = t.find("_t")
            if loc2 < 0 or loc1 < 0:
                continue
            totbits = int(t[loc1+3:loc2])
            shift = totbits - offset - bits
            offset += bits
            bitfield_type = "BitField("+str(shift)+","+str(bits)+")"
            namedTypes[intyp] = bitfield_type
#            raise Exception("BitField",t)
            return bitfield_type,offset
        
        for fn in symbols:

            if t in symbols[fn]["structs"]:
                
                if fn not in symbols[infile]["depends"]:
                    symbols[infile]["depends"][fn] = []
                if t not in symbols[infile]["depends"][fn]:
                    symbols[infile]["depends"][fn].append(t)
                stype = "Struct("+t+")"
                namedTypes[intyp] = stype
                return stype,offset

            if t in symbols[fn]["unions"]:
                if fn not in symbols[infile]["depends"]:
                    symbols[infile]["depends"][fn] = []
                if t not in symbols[infile]["depends"][fn]:
                    symbols[infile]["depends"][fn].append(t)
                stype = "Union("+t+")"
                namedTypes[intyp] = stype
#                raise Exception("Union",t)
                return stype,offset
    
    print("From: "+infile+ " - What is "+intyp+"? (s for verbatim struct)")
    print("Your other choices are: ")
    print(choices)
    while True:
        ans = input("Pick a number (0 indexed): ")
        if ans.upper().strip() == 'S':
            stype = "Struct("+intyp+")"
            namedTypes[intyp] = stype
            return stype,offset
        try:
            ix = int(ans)
            namedTypes[intyp] = choices[ix]  # remember for later!
            return choices[ix],offset
        except:
            print("Invalid entry.  Try again.")
            
            
def extract_tokens(refs):
    
    tokens = []
    token = ''
    
    for c in refs:
        if c.isalnum() or c == "_":
            token += c
            continue
        if len(token) > 0:
            tokens.append(token)
        token = ''
        
    if len(token) > 0:
        tokens.append(token)
        
    return tokens
        
    
def resolve_referenced_symbols(symbols,file,refs,level=0):
    
    if refs == "header":
        raise Exception("Coco "+str(level)+":"+file+" -> "+refs)
    
    if type(refs) is list:
        for item in refs:
            resolve_referenced_symbols(symbols,file,item,level+1)
        return

    if type(refs) is tuple:  # Only used for members of unions and structs
        if len(refs) < 2:
            return
        for item in refs[1:]: # Skip the member name
            resolve_referenced_symbols(symbols,file,item,level+1)
        return
    
    if isinstance(refs,numbers.Number):
        return
    
    if type(refs) is str:
        tokens = extract_tokens(refs)
        for token in tokens:
            try:
                n = float(token)
                resolve_referenced_symbols(symbols,file,n,level+1)
            except:
                for fn in symbols:
                    if fn == file:
                        continue
                    if token in symbols[fn]["defines"]:
                        if fn not in symbols[file]["depends"]:
                            symbols[file]["depends"][fn] = []
                        if token not in symbols[file]["depends"][fn]:
                            symbols[file]["depends"][fn].append(token)
                        break  # First one wins
                    
#                found = False
#                for key in symbols[file]:
#                    if key == "depends":
#                        continue
#                    if token in symbols[file][key]:
#                        if file not in symbols[file]["depends"]:
#                            symbols[file]["depends"][file] = []
#                        if token not in symbols[file]["depends"][file]:
#                            symbols[file]["depends"][file].append(token)
#                        found = True
#                for fn in symbols:
#                    if found:
#                        break
#                    if fn == file:
#                        continue;
#                    for key in symbols[fn]:
#                        if key == "depends":
#                            continue
#                        if token in symbols[fn][key]:
#                            if fn not in symbols[file]["depends"]:
#                                symbols[file]["depends"][fn] = []
#                            if token not in symbols[file]["depends"][fn]:
#                                symbols[file]["depends"][fn].append(token)
#                            found = True
                                
def remove_casts(lines):  # Kludge
    lines = lines.replace("void","").replace("inline","").replace("__extension__","")
    for sz in ["8","16","32"]:
        for sg in ["","u"]:
            for prefix in [""," ","  ","   "]:
                for postfix in [""," ","  ","   "]:
                    target = "("+prefix+sg+"int"+sz+"_t"+postfix+")"
                    loccast = lines.find(target)
                    while loccast >= 0:
                        if lines[:loccast].strip().endswith("sizeof"):                        
                             return lines[:loccast+len(target)] + remove_casts(lines[loccast+len(target):])
                            
                        lines = lines[:loccast] + lines[loccast+len(target):]
                        loccast = lines.find(target)
    return lines
        
def process_h(files_dict,outdir):
    global defines,structs,unions,typedefs
    symbols = {}
    for indir in files_dict:
        for file in files_dict[indir]:
            print("\n",os.path.join(indir,file))
#            g_structs.update(structs)
            defines = {}
            structs = {}
            unions = {}
            typedefs = {}
            with open(os.path.join(indir,file)) as fp:
                lines = remove_comments(fp.read())
                lines = remove_casts(lines)
                lines = lines.split("\n")
                
                lines2 = ''
                for line in lines:
                    if line.strip().startswith("#"):
                        process_preprocessor(line.strip())
                    else:
                        lines2 += line + "\n"
        #        
                in_tokens = lines2.replace("volatile"," ").replace("="," = ").replace(","," , ").replace(";"," ; ").replace("{"," { ").replace("}"," } ").replace(":"," : ").split()
                
                tokens = []
                for token in in_tokens:
                    tokens.append(replace_keywords(token))
                
                while len(tokens) > 0:
                    if tokens[0].strip() == "typedef":
                        tokens,dummy = process_typedef(tokens[1:])
                    elif tokens[0].strip() in ["enum","struct"]:
                        tokens,dummy = process_typedef(tokens)
                    else:
                        tokens = tokens[1:]
                        


                    
            symbols[file] = {"defines":dict(defines),"structs":dict(structs),"unions":dict(unions),"typedefs":dict(typedefs),"depends":{}}
        
    for indir in files_dict:
        for file in files_dict[indir]:
            for key in symbols[file]:
                if key == "depends":
                    continue
                for v in symbols[file][key]:
                    tk = symbols[file][key][v]
                    resolve_referenced_symbols(symbols,file,tk)
                       
            
    for indir in files_dict:
        for file in files_dict[indir]:
            with open(os.path.join(outdir,file.replace(".","_")+".py"),"w") as wp:
#               LOOK AHEAD
                structsout = ''
                for o in order:
                    
                    if o in symbols[file]["unions"]:
                        structsout += "\n\n#\t"+o+"\n\n"
                        structsout += "class "+o+"(Union):\n"
                        structsout += "\t_format = Format.LittleEndian\n"
                        offset = 0 # For bitfields
                        for m in symbols[file]["unions"][o]:
                            structsout += "\t"+m[0]+" = Type."
                            rt,offset = resolve_type(symbols,m[1],file,offset)
                            if rt is not None:
                                structsout += rt
                            else:
                                raise Exception("Union Not resolved")
                                    
                            if m[2] is not None:
                                structsout += m[2].replace("][","*") # Replace multi-dimensional array
                            structsout += "\n"
                        
       
        
                    if o in symbols[file]["structs"]:
                        structsout += "\n\n#\t"+o+"\n\n"
                        structsout += "class "+o+"(Struct):\n"
                        structsout += "\t_format = Format.LittleEndian\n"
                        offset = 0 # For bitfields
                        for m in symbols[file]["structs"][o]:
                            structsout += "\t"+m[0]+" = Type."
                            rt,offset = resolve_type(symbols,m[1],file,offset)
                            if rt is not None:
                                structsout += rt
                            else:
                                raise Exception("Struct Not resolved")
                                    
                            if m[2] is not None:
                                structsout += m[2].replace("][","*") # Replace multi-dimensional array
                            structsout += "\n"
                        
                wp.write("# ---------------------------------------------------------\n")
                wp.write("#\n")
                wp.write("# This file is auto-generated from C header file: "+file+" \n")
                wp.write("#\n")
                wp.write("# ---------------------------------------------------------\n\n")
                wp.write("from embeddedstructs import Struct,Type,Format\n")
                
                for fn in symbols[file]["depends"]:
                    if fn == file:
                        continue
                    wp.write("from "+fn.replace(".","_")+" import ")
                    first = True
                    for s in symbols[file]["depends"][fn]:
                        if not first:
                            wp.write(",")
                        wp.write(s)
                        first = False
                    wp.write("\n")
                    
                wp.write("\n# ---------------------------------------------------------\n")
                wp.write("#\n")
                wp.write("# CONSTANTS \n")
                wp.write("#\n")
                wp.write("# ---------------------------------------------------------\n\n")
                if file in symbols[file]["depends"]:
                             
                    for o in symbols[file]["depends"][file]:
                        if o in symbols[file]["defines"]:
    #                        print(o,"=",symbols[file]["defines"][o])
                            wp.write(o+" = "+replace_sizeof(symbols,file,str(symbols[file]["defines"][o]))+"\n")
                            
                wp.write("\n# ---------------------------------------------------------\n")
                wp.write("#\n")
                wp.write("# CONSTANTS NOT USED IN THIS FILE\n")
                wp.write("#\n")
                wp.write("# ---------------------------------------------------------\n\n")
                            
                for s in symbols[file]["defines"]:
                    if file not in symbols[file]["depends"]:
                        wp.write(s+" = "+replace_sizeof(symbols,file,str(symbols[file]["defines"][s]))+"\n")
                        continue
#                    try:
                    if s in symbols[file]["depends"][file]:
                        continue  # ALready output
                    wp.write(s+" = "+replace_sizeof(symbols,file,str(symbols[file]["defines"][s]))+"\n")
#                    except:
#                        print("Problem file:",file,"symbol",s)
#                        if file not in symbols:
#                            raise Exception("symbols[file] not there")
#                        if "depends" not in symbols[file]:
#                            raise Exception("symbols[file][depends] not there")
#                        if file not in symbols[file]["depends"]:
#                            raise Exception("symbols[file][depends[file] not there")
#                        raise Exception("WTF?")
                        
                wp.write("\n# ---------------------------------------------------------\n")
                wp.write("#\n")
                wp.write("# STRUCTS\n")
                wp.write("#\n")
                wp.write("# ---------------------------------------------------------\n")
                         
                wp.write(structsout)


                        
def show_data():
                        
        for o in order:
            if o in defines:
                print(o,"=",defines[o])
    
        print("----")
        for o in order:
            if o in structs:
                print(o,"->",structs[o])
        print('-----')
        for o in order:
            if o in unions:
                print(o,"~~>",unions[o])
        print('-----')
        for o in order:
            if o in typedefs:
                print(o,"=>",typedefs[o])
                
 
def traverse(root_dir):
    packs = None
    for dirname,subs,flist in os.walk(root_dir):
        fpack = None
        for fname in flist:
            if not fname.endswith(".h"):
                continue
            if fname.endswith("_xml.h"):
                continue
            if fpack is None:
                fpack = []
            fpack.append(fname)
        if fpack is None:
            continue
        if packs is None:
            packs = {}
        packs[dirname] = fpack
    return packs
    
    
               
if __name__ == "__main__":
#    gens = {"C:\\github\\swift-der\\submodules\\data_model\\generated\\pcu":["sunspec_define.h"]}
#    hpps = {"C:\\github\\concerto_plc\\app\\PLComm":["macctrlmsgs.hpp","dll.h"]}
#    src = {"C:\\github\\swift-der\\app\\common_services\\src":["device_comm_core.cpp"]}
#    inters = {"C:\\github\\concerto_plc\\app\\Coord_MAC":["inter_coord_procedure.hpp"]}
#    macs = traverse("C:\\github\\concerto_plc\\app\\public")C:\Users\sdavila\Downloads\files.tar\files\plctools
    gens = {"C:\\Users\\sdavila\\Downloads\\files.tar\\files\\plctools":["plc_poll.c"]}
    hpps = {"C:\\Users\\sdavila\\Downloads\\files.tar\\files\\libcomms":["enphase_command.c"]}
    packs = traverse("C:\\github\\pvs_acpv\\src\\target\\include\\enphase")
#    packs = traverse("C:\\github\\swift-der\\submodules\\eac-mcp")
    packs.update(gens)
#    packs.update(inters)
    packs.update(hpps)
#    packs.update(src)
#    packs.update(macs)
    for d in packs:
        print("From",d)
        for f in packs[d]:
            print("\t",f)
    process_h(packs,"C:\\workspaces\\EnphasePLC\\Temp")
#    process_h(packs,"C:\\enphase_mcp_python")
#    process_h(["agf.h","agf_structs.h","mcp_enphase.h","mcp_counters.h","mcp_sunpower.h"],"C:\\)
#    show_data()

