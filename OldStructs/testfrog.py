# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 17:08:46 2018

@author: sdavila
"""

class Test:
    def __init__(self,foo,**kwargs):
        self._frog = "dog"
        for k,v in kwargs.items():
            setattr(self, k, v)

c = Test(_frog="cat")
print(c._frog)