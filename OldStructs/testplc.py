# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 13:33:56 2018

@author: dvt3
"""
from enphase_structs import enphase_pack_poll_command_search,enphase_unpack_deviceInfo_tlv,unpack_bcd,unpack_chars,unpack_partnum,\
                enphase_pack_poll_request_data_tlv,cMcpPollFlagInterval,enphase_unpack_interval_data_tlv,unpack_millijoules
from PLCTools import plc_channel

# test
#	idb = Type.UnsignedByte[cPartNumIdLen]
#	manuRev = Type.UnsignedByte
#	dMajor = Type.UnsignedByte
#	dMinor = Type.UnsignedByte
#	dMaint = Type.UnsignedByte

LINE_MEAS_CLOCK_FREQ	 = 50000000.0


if __name__ == "__main__":
    
    ch = plc_channel("COM9")
    
    try:
    
        packet = enphase_pack_poll_command_search()
        print(packet.hex())
        print(len(packet))
        print("Sending packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
    #    buf = ch.get_buf()
    #    print(len(buf))
    #    print(buf.hex())
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        print("Receive packet..")
        rpacket = ch.recv_packet()
        print("Unpacking...")
        devInfo,tm,status = enphase_unpack_deviceInfo_tlv(rpacket)
        print(rpacket.hex())
        print(len(rpacket))
        print("devSerialNum=",unpack_bcd(devInfo.devInfo.devSerialNum.byteArr))
        print("devPartNum=",unpack_partnum(devInfo.devInfo.devPartNum))
        print("devAssemblyNum=",unpack_partnum(devInfo.devInfo.devAssemblyNum))
        print("asicId=",unpack_bcd(devInfo.devInfo.asicId.asicId)) # = Type.Struct(tAsicId)
        print("pv_name=",unpack_chars(devInfo.devInfo.moduleEntry.pv_name))
        print("pv_model=",unpack_chars(devInfo.devInfo.moduleEntry.pv_model))
        print("pv_manuf=",unpack_chars(devInfo.devInfo.moduleEntry.pv_manuf))
        print("domainAddr=",unpack_bcd(devInfo.devInfo.domainAddr))# = Type.UnsignedByte[cL2DomainAddrLen]
        print("devType",devInfo.devInfo.devType)
        print("modAddr",devInfo.devInfo.modAddr)
        print("grpMask",devInfo.devInfo.grpMask)
        print("rxEmuSsi",devInfo.devInfo.rxEmuSsi)
        print("flags",devInfo.devInfo.flags)
        print(tm)
        print(status)
        
        packet = enphase_pack_poll_request_data_tlv(None, None, cMcpPollFlagInterval,devInfo.devInfo.devSerialNum)
        print(packet.hex())
        print(len(packet))
        print("Sending packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
    #    buf = ch.get_buf()
    #    print(len(buf))
    #    print(buf.hex())
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        print("Receive packet..")
        rpacket = ch.recv_packet()
        print("Unpacking...")
        pRspIntervalTlv,tm,status = enphase_unpack_interval_data_tlv(rpacket)
        print("Temp=",pRspIntervalTlv.intervalData.temperature)
        print("flags=",pRspIntervalTlv.intervalData.flags)
        print("onTime=",pRspIntervalTlv.intervalData.onTime)
#  	pwrConvErrSecs = Type.UnsignedByte
#	pwrConvMaxErrCycles = Type.UnsignedByte
#	producedMilliJoules = Type.UnsignedShort[3]
#	acVoltageINmV = Type.UnsignedLong
#	acFrequencyINClkCycles = Type.UnsignedLong
#	dcVoltageINmV = Type.UnsignedLong
#	dcCurrentINmA = Type.UnsignedLong
#	usedMilliJoules = Type.UnsignedShort[3]
#	sumMilliLeadingVAr = Type.UnsignedShort[3]
#	sumMilliLaggingVAr = Type.UnsignedShort[3]

        print("pwrConvErrSecs=",pRspIntervalTlv.intervalData.pwr.pwrConvErrSecs)
        print("pwrConvMaxErrCycles=",pRspIntervalTlv.intervalData.pwr.pwrConvMaxErrCycles)
        print("LTE=",unpack_millijoules(pRspIntervalTlv.intervalData.pwr.producedMilliJoules)/3600000.0)
        print("acVoltage=",pRspIntervalTlv.intervalData.pwr.acVoltageINmV/1000.0)
        if pRspIntervalTlv.intervalData.pwr.acFrequencyINClkCycles > 0.0:
            print("Frequency=",(LINE_MEAS_CLOCK_FREQ/pRspIntervalTlv.intervalData.pwr.acFrequencyINClkCycles))
        print("dcVoltage=",pRspIntervalTlv.intervalData.pwr.dcVoltageINmV/1000.0)
        print("dcCurrent=",pRspIntervalTlv.intervalData.pwr.dcCurrentINmA/1000.0)
        print("WH Consumed=",unpack_millijoules(pRspIntervalTlv.intervalData.pwr.usedMilliJoules)/3600000.0)
        print("sumLeadingVAr=",unpack_millijoules(pRspIntervalTlv.intervalData.pwr.sumMilliLeadingVAr)/3600000.0)
        print("sumLaggingVAr=",unpack_millijoules(pRspIntervalTlv.intervalData.pwr.sumMilliLaggingVAr)/3600000.0)

        
    except Exception as ex:
        raise ex
    finally:
        ch.close()
