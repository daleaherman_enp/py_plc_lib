# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 11:06:56 2018

@author: sdavila
"""

import serial
import time
import winsound
frequency = 1000
duration = 200

import serial.tools as tools
from enphase_h import cSerialNumBytesLen
from IQ8_PLC_Helpers import enphase_unpack_deviceInfo_tlv,enphase_pack_poll_command_search,\
                enphase_pack_squelch,unpack_bcd,unpack_partnum,unpack_chars,\
                enphase_pack_v6_start_fw_upgrade,get_next_missing_hunk,enphase_pack_v6_hunk,\
                enphase_pack_v6_poll_missing_hunks,enphase_unpack_v6_missing_hunks,\
                enphase_pack_v6_write_metadata,enphase_pack_v6_request_metadata, \
                enphase_unpack_v6_metadata,enphase_pack_v6_start_DMIR_upgrade,\
                enphase_pack_v6_fw_info,enphase_unpack_v6_fwinfo,\
                enphase_pack_ECDC,enphase_extract_payload,enphase_generic_poll,\
                enphase_unpack_v6_telemetry1,enphase_unpack_v6_telemetry2,SPM_unpack_version,SPM_pack_FW_segment
                
from mcp_mde_image_h import MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE
from mde_ids_h import mde_id_DMIR_finish_and_check,mde_id_read_telemetry_level1,\
                mde_id_read_telemetry_level2

SQUELCH_DELAY_PER_PCU = 2

SCCMDHEADERV2_SEQUENCE_START = 0x80
SCCMDHEADERV2_SEQUENCE_START_MASK = 0x80
SCCMDHEADERV2_SEQUENCE_CONT = 0x40
SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK  = 0x0F

eSC_INTF_CMD_VERSION2           = 0xAA
eSC_INTF_WRITE_PLC              = 1
ePLC_INTF_WRITE_TX              = eSC_INTF_WRITE_PLC
SC_E_ASIC_PLC = 0x06
SC_E_ASIC_MGMT = 0x07
PLC_FLAGS_SYNCHRONOUS_PLC =  0x01

eSC_INTF_READ_PLC              = 2
eSC_INTF_VERSION                = 8
eSC_INTF_UPGRD_COMMIT           = 6
eSC_INTF_READ_MGMT              = 7


ACK_TIMOUT = 1

xmit_flags = 0

# To retain order of members

import collections 

class OrderedClassMembers(type):
    @classmethod
    def __prepare__(self, name, bases):
        return collections.OrderedDict()

    def __new__(self, name, bases, classdict):
        classdict['__ordered__'] = [key for key in classdict.keys()
                if key not in ('__module__', '__qualname__')]
        return type.__new__(self, name, bases, classdict)





crc16_table =[
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
]

def crc16ccitt(pBuf, buf_len, init):

    loopIndex = 0
    crc16temp = init
    while loopIndex < buf_len:
        crc16temp = (crc16temp >> 8) ^ crc16_table[(crc16temp ^ pBuf[loopIndex]) & 0xff];
        loopIndex+=1
    return crc16temp


class PLCException(Exception):
    def __init__(self,ss):
        super().__init__(ss)
        
class PLCTimoutException(Exception):
    def __init__(self,ss):
        super().__init__(ss)


def comport_list():
    
    ports = []

    i = 0
    while True:
        try:
            port = tools.list_ports.comports().pop(i).device
            ports.append(port)
            i += 1
        except:
            break;
            
    ports.sort()
    return i,ports

def isValidAck(rpacket):
    return True

class plc_channel:
    
    def __init__(self, port,speed=115200,timeout=10,send_retries=3,debug=False):
        self.buf = []  # mock
        self.port = port;
        self.speed = speed
        self.timeout = timeout
        self.send_retries = send_retries
        self.debug = debug
        self.channel = serial.Serial(port,baudrate=self.speed,timeout=self.timeout);
        self.search_packet = enphase_pack_poll_command_search()
        
    def change_timeout(self,timeout):
        self.channel.timeout = timeout
        
    def restore_timeout(self):
        self.channel.timeout = self.timeout
        
    def close(self):
        self.channel.close()
        
    def write(self,byte_array):
        if type(byte_array) is not bytes:
            raise PLCException("write requires a byte array")
        if self.debug:
            print(self.channel.name,self.channel.baudrate,": Prepping",len(byte_array),"bytes...")
            output_bytes("write",byte_array)
        n = self.channel.write(byte_array)
        self.channel.flush()
        return n
            
    def get_buf(self):
        return b''.join(self.buf)
        
    def read(self):
        data = self.channel.read()
        if data is None or len(data) < 1:
            raise PLCTimoutException("Timed out after "+str(self.timeout)+" seconds.")
        return data
    
    def send_SPM_packet(self,byte_array):
        if type(byte_array) != bytes:
            raise PLCException("Method xmit requires a byte array as input: "+str(byte_array))
        for i in range(self.send_retries):

            ibytes = byte_array
            
            payload = eSC_INTF_WRITE_PLC.to_bytes(1,"little") + PLC_FLAGS_SYNCHRONOUS_PLC.to_bytes(1,"little") + ibytes
            pheader = eSC_INTF_CMD_VERSION2.to_bytes(1,"little") + SC_E_ASIC_MGMT.to_bytes(1,"little") + xmit_flags.to_bytes(2,"little")
            
            num = self.send(pheader+payload)
                
#            try:    
#                self.channel.timout = ACK_TIMOUT
#                rpacket = self.recv_packet()  # ACK
#                if not isValidAck(rpacket):
#                    raise PLCException("Expected PLC ACK")
#                self.channel.timeout = self.timeout
#                return num
#            except Exception as ex:
#                if type(ex) is PLCTimoutException:
#                    continue
#                else:
#                    raise ex
            return num
        
        self.channel.timeout = self.timeout
        return 0

    
    def send_packet(self,byte_array):
        if type(byte_array) != bytes:
            raise PLCException("Method xmit requires a byte array as input: "+str(byte_array))
        for i in range(self.send_retries):
            ibytes = byte_array
            
            payload = eSC_INTF_WRITE_PLC.to_bytes(1,"little") + PLC_FLAGS_SYNCHRONOUS_PLC.to_bytes(1,"little") + ibytes
            
            pheader = eSC_INTF_CMD_VERSION2.to_bytes(1,"little") + SC_E_ASIC_PLC.to_bytes(1,"little") + xmit_flags.to_bytes(2,"little")
            
            mask = SCCMDHEADERV2_SEQUENCE_START
            
            num = 0
            
            while len(payload) > 0:
                # pheader + sequence + packetSize
                segments_left = int((len(payload)+127)/128) - 1
                payload_size = min(len(payload),128)
                if self.debug:
                    print("PAYLOAD SIZE",payload_size)
                ibytes = pheader + (segments_left | mask).to_bytes(1,"little") + (payload_size-2).to_bytes(1,"little") + payload[:payload_size]
                mask = SCCMDHEADERV2_SEQUENCE_CONT
                if self.debug:
                    output_bytes("Pre-Send",payload[:payload_size])
                payload = payload[payload_size:]
                num += self.send(ibytes)
                
            try:    
                self.channel.timout = ACK_TIMOUT
                rpacket = self.recv_packet()  # ACK
                if not isValidAck(rpacket):
                    raise PLCException("Expected PLC ACK")
                self.channel.timeout = self.timeout
                return num
            except Exception as ex:
                if type(ex) is PLCTimoutException:
                    continue
                else:
                    raise ex
        
        self.channel.timeout = self.timeout
        return 0
        
        
    def recv_packet(self,rec_code=SC_E_ASIC_PLC,acceptable_read_codes=(ePLC_INTF_WRITE_TX,eSC_INTF_READ_PLC)):
        retries = 32
        
        payload = b''
        last_segments_left = 1
        
        decobbed = b''
        
        while True:
            
            obytes = self.recv()
            if self.debug:
                output_bytes("recv_packet: ",obytes)
            decobbed += obytes
            if (obytes[0] != eSC_INTF_CMD_VERSION2) or (obytes[1] != rec_code):
                retries -= 1
                print("Warning: Unexpected packet received: " , obytes.hex())
                if retries == 0:
                    print("Error:  Too many malformed packets received")
                    return b''
                continue
            if (obytes[4] & SCCMDHEADERV2_SEQUENCE_START) == SCCMDHEADERV2_SEQUENCE_START:
                if rec_code != SC_E_ASIC_PLC: # Not from PCU
                    last_segments_left = segments_left = 0
                elif obytes[6] not in acceptable_read_codes:
                    print("Warning: Malformed packet received: " , obytes.hex())
                    print("Read code received =",obytes[6])
                    retries -= 1
                    if retries == 0:
                        print("Error:  Too many malformed packets received")
                        return b''
                    continue
                else:
                    last_segments_left = segments_left = obytes[4] & SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK
                payload = obytes[8:] # skip over sSCCmdHeaderV2_t + 2 lead bytes
            else:
                if rec_code != SC_E_ASIC_PLC: # Not from PCU
                    last_segments_left = segments_left = 0
                else:
                    segments_left = obytes[4] & SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK
                if segments_left != last_segments_left - 1:
                    print("Warning: Segment received not in sequence.  Got",segments_left,"but expected",last_segments_left - 1)
                payload += obytes[6:] # skip over sSCCmdHeaderV2_t only
                last_segments_left = segments_left
            if self.debug:
                print("Segments left",last_segments_left)

            if segments_left == 0:
                if self.debug:
                    print("No segments left")
                break
            
        if len(decobbed) < 3:   
             raise PLCException("Received packet is too small:",len(payload),"bytes.")
        crc = crc16ccitt(decobbed,len(decobbed)-2,0xFFFF)
        if (crc & 0xFF != decobbed[len(decobbed)-2]) or ((crc >>8) & 0xFF != decobbed[len(decobbed)-1]):
            raise PLCException("Invalid CRC received")
            
        if self.debug:
            output_bytes("Payload",payload[:-2])
        return payload[:-2]
        
    def send(self,byte_array): # COBiffy
        if type(byte_array) != bytes:
            raise PLCException("Method send requires a byte array as input: "+str(byte_array))
        if self.debug:
            output_bytes("Send array:",byte_array)
        ibytes = byte_array
        crc = crc16ccitt(ibytes,len(ibytes),0xFFFF)
        ibytes += (crc & 0xFF).to_bytes(1,byteorder="little")
        ibytes += ((crc >> 8) & 0xFF).to_bytes(1,byteorder="little")
        if self.debug:
            print(ibytes.hex())
            print(len(ibytes))
        nonZeroes = 1
        cobs = b'\x00'
        temp = b''

        for c in ibytes:
            if c != 0:
                nonZeroes += 1
                temp += c.to_bytes(1,'big')
                continue
            cobs += nonZeroes.to_bytes(1,'big')
            cobs += temp
            temp = b''
            nonZeroes = 1
            
        cobs += nonZeroes.to_bytes(1,'big')
        cobs += temp
        cobs += b'\x00'
        if len(cobs) > 257:
            if self.debug:
                print()
                print(ibytes.hex())
                print()
                print(cobs.hex())
            raise PLCException("Send size exceeds 257 bytes",len(ibytes),len(cobs))
            
        if count_zeroes(cobs) != 2:
            raise PLCException("Too many zeroes!")
        if cobs[0] != 0 or cobs[len(cobs)-1] != 0:
            raise PLCException("Zeroes in the wrong places!")
            
        return self.write(cobs)
    
    def recv(self): # unCOBiffy
        outs = b''
        c = 3
        pending = False

        while c != 0:
            c = int.from_bytes(self.read(),byteorder='little')
            
        while True:
            if self.debug:
                print(".",end="")
            nextz = int.from_bytes(self.read(),byteorder='little')
            if nextz == 0:
                return outs
            if pending:
                outs += b'\x00'
            while nextz > 1:
                outs += self.read()
                nextz -= 1
            pending = True
        
        return None;
    
    
    
# -----------------------  Dev Board
        
    def GetSPMVersionInfo(self):
        self.send_SPM_packet(bytes([eSC_INTF_VERSION]))
        rpacket = self.recv_packet(SC_E_ASIC_MGMT,(ePLC_INTF_WRITE_TX,eSC_INTF_READ_PLC,eSC_INTF_READ_MGMT))
        return SPM_unpack_version(rpacket)
    
    def UpgradeSPMFirmware(self,bin_file):
        
        with open(bin_file,"rb") as fb:
            bdata = fb.read()
        
        i = 0
        tot = int(len(bdata)/128)+1
        while True:
            packet = SPM_pack_FW_segment(bdata,i)
#            output_bytes("FW pack",packet)
#            ans = input("Hey")
            if packet is None:
                break
#            time.sleep(0.1)
            print("Sent",self.send_SPM_packet(packet),"bytes for segment",str(i)+"/"+str(tot))
            rpacket = self.recv_packet(SC_E_ASIC_MGMT,(ePLC_INTF_WRITE_TX,eSC_INTF_READ_PLC,eSC_INTF_READ_MGMT)) # ACK
            if not isValidAck(rpacket):
                pass
            i += 1
        if i % 2 == 0:
           packet = SPM_pack_FW_segment(bdata,i,True)
#           time.sleep(0.1)
           print("Sent",self.send_SPM_packet(packet),"bytes for filler segment",str(i)+"/"+str(tot))
           rpacket = self.recv_packet(SC_E_ASIC_MGMT,(ePLC_INTF_WRITE_TX,eSC_INTF_READ_PLC,eSC_INTF_READ_MGMT)) # ACK
           if not isValidAck(rpacket):
                pass
            
        print("Committing SPM firmware...")
        self.send_SPM_packet(bytes([eSC_INTF_UPGRD_COMMIT]))
        rpacket = self.recv_packet(SC_E_ASIC_MGMT,(ePLC_INTF_WRITE_TX,eSC_INTF_READ_PLC,eSC_INTF_READ_MGMT))

    
# --------------------- FW Upload        
    def UpdateFW(self,swe_fname,tch_name,pcus,max_retries=5):

        print("Uploading firmware from",swe_fname)
        
 #        if True:
        
        with open(swe_fname) as fwp:
            bcd_data = fwp.read().replace("\n","")
            
        missing_hunks = None
        for retries in range(max_retries):
        
            packet, missing_hunks = enphase_pack_v6_start_fw_upgrade(bcd_data,missing_hunks)
#            print(len(packet))
            print("Sending Start Upgrade packet...")
            print("Sent",self.send_packet(packet),"bytes...")
    
            time.sleep(2)
            
#            input("Press C to continue ")
            
            seq = -1
            while True:
                seq = get_next_missing_hunk(missing_hunks,seq)
                if seq < 0:
                    break
                packet = enphase_pack_v6_hunk(bcd_data,seq)
                print("Retry:",retries,"Sending Hunk packet #",seq)
                print("Sent",self.send_packet(packet),"bytes...")
                
            missing_hunks = None
            
            for pcu in pcus:
            
                domainAddr,logicalID = pcu.GetUnicastAddressing()
                packet = enphase_pack_v6_poll_missing_hunks(domainAddr,logicalID)
#                print(len(packet))
#                print("Sending Poll Missing packets...")
                print("Sent",self.send_packet(packet),"bytes...")
               
                print("Receive Missing Hunks from",pcu.SerialNumber)
        #        self.debug=True
                rpacket = self.recv_packet()
#                output_bytes("Missing hunks",rpacket)
                missing_hunks = enphase_unpack_v6_missing_hunks(rpacket,missing_hunks)

            if missing_hunks is None:
                break
        
# ----------------------  METADATA Upload        
        print("Uploading MetaData from",tch_name)

        with open(tch_name) as fwp:
            lines = fwp.readlines()
            bcd_data = ''
            for line in lines:
                if line.strip().startswith("#"):
                    continue
                bcd_data += line.replace("\n","")
            
            
        packet = enphase_pack_v6_write_metadata(bcd_data)
        print("Sending Metadata Upgrade packet...")
        print("Sent",self.send_packet(packet),"bytes...")
        
        print("Requesting Metadata...")
        for pcu in pcus:
        
            domainAddr,logicalID = pcu.GetUnicastAddressing()
            packet = enphase_pack_v6_request_metadata(domainAddr,logicalID,bank=MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE)
    #        print(len(packet))
            print("Sent",self.send_packet(packet),"bytes...")
            
            print("Received Metadata from",pcu.SerialNumber)
    #        self.debug=True
            rpacket = self.recv_packet()
            
            pMeta = enphase_unpack_v6_metadata(rpacket)
            pcu.RecordFWMetadata(pMeta)
    
# --------------------- DMIR Upload
            
    def UpdateDMIR(self,dmir_fname,bank,pcus,max_retries=5):
        
        print("Uploading DMIR from file",dmir_fname)

        with open(dmir_fname) as fwp:
            lines = fwp.readlines()
            bcd_data = ''
            for line in lines:
                if line.strip().startswith("#"):
                    continue
                bcd_data += line.replace("\n","")

        missing_hunks = None
        for retries in range(max_retries):
        
            packet, missing_hunks = enphase_pack_v6_start_DMIR_upgrade(bcd_data,bank,missing_hunks)
            print("Sending Start Upgrade packet...")
            print("Sent",self.send_packet(packet),"bytes...")
    
            time.sleep(2)
            
#            input("Press C to continue ")
            
            seq = -1
            while True:
                seq = get_next_missing_hunk(missing_hunks,seq)
                if seq < 0:
                    break
                packet = enphase_pack_v6_hunk(bcd_data,seq)
#                print(len(packet))
                print("Retry:",retries,"Sending Hunk packet #",seq)
                print("Sent",self.send_packet(packet),"bytes...")
        
            missing_hunks = None
            
            for pcu in pcus:
                
                domainAddr,logicalID = pcu.GetUnicastAddressing()
                packet = enphase_pack_v6_poll_missing_hunks(domainAddr,logicalID,mde_id=mde_id_DMIR_finish_and_check)
                
                print("Sent",self.send_packet(packet),"bytes...")
                print("Receive Missing Hunks from",pcu.SerialNumber)
                rpacket = self.recv_packet()
#                output_bytes("Missing hunks",rpacket)
                missing_hunks = enphase_unpack_v6_missing_hunks(rpacket,missing_hunks)
            if missing_hunks is None:
                break
     
    def Discover(self,expected_pcus=16,max_retries_without_success=3):
        pcus = []
        found_sns = []
        retries_without_success = 0
        while retries_without_success < max_retries_without_success:
                print("Searching...")
                print("Sent",self.send_packet(self.search_packet),"bytes...")
                print("Retries:",retries_without_success)
                
                try:
                    self.change_timeout(1.5)
                    rpacket = self.recv_packet()
    #                print("Unpacking...")
                    devInfoStruct,tm,status = enphase_unpack_deviceInfo_tlv(rpacket)
                    
                    sn = unpack_bcd(devInfoStruct.devInfo.devSerialNum.byteArr)
                    print("Discovered PCU: ",sn)
                    if sn in found_sns:
                        retries_without_success += 1
                        continue
                    found_sns.append(sn)
#                    
                    pcu = PCU(self,sn,devInfoStruct.devInfo)
#                    pcu.devPartNum=unpack_partnum(devInfo.devInfo.devPartNum)
#                    pcu.devAssemblyNum=unpack_partnum(devInfo.devInfo.devAssemblyNum)
#                    pcu.asicId=unpack_bcd(devInfo.devInfo.asicId.asicId) # = Type.Struct(tAsicId)
#                    pcu.pv_name=unpack_chars(devInfo.devInfo.moduleEntry.pv_name)
#                    pcu.pv_model=unpack_chars(devInfo.devInfo.moduleEntry.pv_model)
#                    pcu.pv_manuf=unpack_chars(devInfo.devInfo.moduleEntry.pv_manuf)
#                    pcu.devType=devInfo.devInfo.devType
#                    pcu.grpMask=devInfo.devInfo.grpMask
#                    pcu.rxEmuSsi=devInfo.devInfo.rxEmuSsi
#                    pcu.flags=devInfo.devInfo.flags
                    retries_without_success = 0
                    pcus.append(pcu)
                    if len(pcus) >= expected_pcus:
                        break
                    
                    # Silence the PCU for next try
                    
                    sq_packet = enphase_pack_squelch(pcu.SerialNumber,expected_pcus*SQUELCH_DELAY_PER_PCU)
                    
                    print("Squelching",pcu.SerialNumber,"for",expected_pcus*SQUELCH_DELAY_PER_PCU,"seconds.")
                    print("Sent",self.send_packet(sq_packet),"bytes...")
              
                except Exception as ex:
                    if type(ex) is PLCTimoutException:
                        retries_without_success += 1
                        winsound.Beep(frequency,duration*3)

                    else:
                        raise ex
                finally:
                    self.restore_timeout()
                    
        return pcus
    
                   


def output_bytes(title,bs):
        print("\n",title,"---------------------------")
        i = 0
        for c in bs:
            if i % 16 == 0:
                print("")
            i += 1
            print("%02x " %c,end="")
        print("\n",title,"---------------------------")
        print(len(bs),"bytes.")
    
    
def count_zeroes(byte_array):
    count = 0
    for c in byte_array:
        if c==0:
            count += 1
    return count
        
if __name__ == "__dodo__":
    import random
    
    plc = plc_channel("COM1");
    
    try:
    
        for i in range(10):
            ibytes = b''
            r = random.randint(1,252)
            for j in range(r):
               ibytes += random.randint(0,255).to_bytes(1,byteorder="little")

            plc.send_packet(ibytes)
            obytes = plc.recv_packet()
            if ibytes != obytes:
                    print()
                    print()
                    print(ibytes.hex())
                    print("Are the arrays equal?",ibytes==obytes,"length=",len(ibytes),"Zeroes=",count_zeroes(ibytes))
                    print(obytes.hex())
            else:
                print(".",end="")
                
    finally:
       
        plc.close()
        



        
class PCU(metaclass=OrderedClassMembers):
    
    def __init__(self,channel,SerialNumber,devInfo=None):
        self.channel = channel
        # Device Information
        self.SerialNumber = SerialNumber
        self.devInfo = devInfo
        self.fwMetaData = None
        self.fwInfo = None
        self.telemetry1 = None
        self.telemetry2 = None
        self.telemetry3 = None
        
        
    def Associate(self,DomainAddr,LogicalId):
        self.DomainAddr = DomainAddr
        self.LogicalId = LogicalId
        
    def Disassociate(self):
        pass
        
    def UpdateTelemetry(self):
        domainAddr,logicalId = self.GetUnicastAddressing()
        packet = enphase_generic_poll(domainAddr,logicalId,mde_id_read_telemetry_level1)
        print("Sent",self.channel.send_packet(packet),"bytes...")
        print("Receive packet..")
        rpacket = self.channel.recv_packet()
        self.telemetry1 = enphase_unpack_v6_telemetry1(rpacket)
        
        packet = enphase_generic_poll(domainAddr,logicalId,mde_id_read_telemetry_level2)
        print("Sent",self.channel.send_packet(packet),"bytes...")
        print("Receive packet..")
        rpacket = self.channel.recv_packet()
        self.telemetry2 = enphase_unpack_v6_telemetry2(rpacket)
    
    def UpdateConditions(self):
        pass
    
    def UpdateCounters(self):
        pass
    
    def PushValue(self,pfCode):
        pass
    
    def ECDC_write(self,indata):
        domainAddr,logicalId = self.GetUnicastAddressing()
        ecdc_packet = enphase_pack_ECDC(domainAddr,logicalId,indata)
        self.channel.send_packet(ecdc_packet)
        
    def ECDC_read(self):
        rpacket = self.channel.recv_packet()
        return enphase_extract_payload(rpacket)
    
    def UpdateFWInfo(self):
        domainAddr,logicalId = self.GetUnicastAddressing()
        packet = enphase_pack_v6_fw_info(domainAddr,logicalId)
#        print("v6_fw_info:\n",packet.hex())
        print("Requesting PCU",self.SerialNumber,"version...")
        print("Sent",self.channel.send_packet(packet),"bytes...")

        print("Receive packet..")
#        ch.debug=True
        rpacket = self.channel.recv_packet()
        pRspFwInfo = enphase_unpack_v6_fwinfo(rpacket)
        self.fwInfo = pRspFwInfo
        print("\nfw_bank0_crc_is_bad_b:",pRspFwInfo.fw_bank0_crc_is_bad_b)
        print("fw_bank1_crc_is_bad_b:",pRspFwInfo.fw_bank1_crc_is_bad_b)
        print("fw_bank0_is_selected_b:",pRspFwInfo.fw_bank0_is_selected_b)
        print("fw_bank1_is_selected_b:",pRspFwInfo.fw_bank1_is_selected_b)
        print("\nActive bank:")
        print("SCM_hash:",pRspFwInfo.active_bank.SCM_hash.bytes)
        print("has_mods:",pRspFwInfo.active_bank.has_mods)
        print("arena_info.part_number:",pRspFwInfo.active_bank.arena_info.part_number.bcd)
        print("arena_info.revision:",pRspFwInfo.active_bank.arena_info.revision)
        print("arena_info.version.major:",pRspFwInfo.active_bank.arena_info.version.major)
        print("arena_info.version.model:",pRspFwInfo.active_bank.arena_info.version.model)
        print("arena_info.version.revision:",pRspFwInfo.active_bank.arena_info.version.revision)
        print("\nInactive bank:")
        print("SCM_hash:",pRspFwInfo.inactive_bank.SCM_hash.bytes)
        print("has_mods:",pRspFwInfo.inactive_bank.has_mods)
        print("arena_info.part_number:",pRspFwInfo.inactive_bank.arena_info.part_number.bcd)
        print("arena_info.revision:",pRspFwInfo.inactive_bank.arena_info.revision)
        print("arena_info.version.major:",pRspFwInfo.inactive_bank.arena_info.version.major)
        print("arena_info.version.model:",pRspFwInfo.inactive_bank.arena_info.version.model)
        print("arena_info.version.revision:",pRspFwInfo.inactive_bank.arena_info.version.revision)

    
    # Utility functions
    
    def GetUnicastAddressing(self):
        if (self.DomainAddr is None) or (self.DomainAddr == "000000000000"):
            return self.SerialNumber,0x3FFF
        return self.DomainAddr,self.LogicalID
    
    def RecordFWMetadata(self,pMeta):
        self.fwMetaData = pMeta
        
    def DisplayProperties(self):
        for attr in self.__ordered__:
            val = getattr(self,attr)
            if isinstance(val,(bool,int,str,float)):
                print(attr,"=",val)
            if type(val) is tuple:
                print("\n**********************************\nREGION:",val[0],"\n**********************************")
                

        
    # PROPERTIES
    
    @property
    def devinfo_t(self):
        return "Device Information","struct"

    
    @property
    def DomainAddr(self):
        if self.devInfo is None:
            return None
        dAddr = unpack_bcd(self.devInfo.domainAddr)
        return dAddr
        
    @property
    def LogicalID(self):
        if self.devInfo is None:
            return None
        lid = self.devInfo.modAddr
        return lid
    
    @property
    def devPartNum(self):
        if self.devInfo is None:
            return None
        pNbr = unpack_partnum(self.devInfo.devPartNum)
        return pNbr
    
    @property
    def devAssemblyNum(self):
        if self.devInfo is None:
            return None
        pAblyNbr = unpack_partnum(self.devInfo.devAssemblyNum)
        return pAblyNbr
       
    @property
    def asicId(self):
        if self.devInfo is None:
            return None
        aId = unpack_bcd(self.devInfo.asicId.asicId)
        return aId

    @property
    def pv_name(self):
        if self.devInfo is None:
            return None
        pvName = unpack_chars(self.devInfo.moduleEntry.pv_name)
        return pvName

    @property
    def pv_model(self):
        if self.devInfo is None:
            return None
        pvModel = unpack_chars(self.devInfo.moduleEntry.pv_model)
        return pvModel

    @property
    def pv_manuf(self):
        if self.devInfo is None:
            return None
        pvManuf = unpack_chars(self.devInfo.moduleEntry.pv_manuf)
        return pvManuf
    
    @property
    def devType(self):
        if self.devInfo is None:
            return None
        pdevType = self.devInfo.devType
        return pdevType
    
    @property
    def grpMask(self):
        if self.devInfo is None:
            return None
        pgrpMask = self.devInfo.grpMask
        return pgrpMask
    
    @property
    def rxEmuSsi(self):
        if self.devInfo is None:
            return None
        prxEmuSsi = self.devInfo.rxEmuSsi
        return prxEmuSsi
    
    @property
    def flags(self):
        if self.devInfo is None:
            return None
        pflags = self.devInfo.flags
        return pflags
    
         # Telemetry Data
    @property
    def inverter_t(self):
        return "inverter_t","struct"
    
    @property
    def V(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.V
        return prop_raw
    
    @property
    def A(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.A
        return prop_raw
    
    @property
    def W(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.W
        return prop_raw
    
    @property
    def VA(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.VA
        return prop_raw
    
    @property
    def VAr(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.VAr
        return prop_raw
    
    @property
    def Hz(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.Hz
        return prop_raw
    
    @property
    def DCV(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.DCV
        return prop_raw
    
    @property
    def DCA(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.DCA
        return prop_raw
    
    @property
    def DCW(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.DCW
        return prop_raw
    
    @property
    def Temperature(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.Temperature
        return prop_raw
    
    @property
    def PF_True(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.PF.true_pf
        return prop_raw
    
    @property
    def PF_Displacement(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.PF.displacement
        return prop_raw
    
    @property
    def PF_Distortion(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.PF.distortion
        return prop_raw
    
    @property
    def Operating_State(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.Operating_State
        if prop_raw < 1 or prop_raw > 8:
            return None
        return ["OFF","SLEEPING","STARTING","MPPT","THROTTLED","SHUTTING_DOWN","FAULT","STANDBY"][prop_raw-1]
    
    @property
    def Enphase_Operating_State(self):
        if self.telemetry1 is None:
            return None
        prop_raw = self.telemetry1.telemetry_level1.Enphase_Operating_State
        if prop_raw >= 0 and prop_raw <= 10:
            return ["IDLE","STARTING","DISCOVERY","THROTTLED","MPPT","BURSTING","CONSTANT_VIN","STANDBY","PROTECTION_FAULT","GRID_FAULT","DCR"][prop_raw]
        if prop_raw >=99 and prop_raw <= 101:
            return ["UNKNOWN_STATE","DMIR_INIT_FAILED","HOST_SHUTOFF"][prop_raw-99]
        return None

    @property
    def events_t(self):
        return "events_t","struct"
    
    
    @property
    def GND_FAULT(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.GND_FAULT
        return ev_raw
    
    @property
    def DC_OV(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.DC_OV
        return ev_raw
    
    @property
    def AC_DISCONNECT_OPEN(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.AC_DISCONNECT_OPEN
        return ev_raw
    
    @property
    def DC_DISCONNECT_OPEN(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.DC_DISCONNECT_OPEN
        return ev_raw
    
    @property
    def GRID_DISCONNECT(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.GRID_DISCONNECT
        return ev_raw
    
    @property
    def CABINET_OPEN(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.CABINET_OPEN
        return ev_raw
    
    @property
    def MANUAL_SHUTDOWN(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.MANUAL_SHUTDOWN
        return ev_raw
    
    @property
    def OVER_TEMP(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.OVER_TEMP
        return ev_raw
    
    @property
    def OVER_FREQ(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.OVER_FREQ
        return ev_raw
    
    @property
    def UNDER_FREQ(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.UNDER_FREQ
        return ev_raw
    
    @property
    def AC_OVER_VOLT(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.AC_OVER_VOLT
        return ev_raw
    
    @property
    def AC_UNDER_VOLT(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.AC_UNDER_VOLT
        return ev_raw
    
    @property
    def BLOWN_FUSE(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.BLOWN_FUSE
        return ev_raw
    
    @property
    def UNDER_TEMP(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.UNDER_TEMP
        return ev_raw
    
    @property
    def MEMORY_ERR(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.MEMORY_ERR
        return ev_raw
    
    @property
    def HARDWARE_FAILURE(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Events.HARDWARE_FAILURE
        return ev_raw

    @property
    def enphase_events_t(self):
        return "enphase_events_t","struct"
    
    
    @property
    def EV_SPO(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.SPO
        return ev_raw
    
    @property
    def EV_SSO(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.SSO
        return ev_raw
    
    @property
    def EV_STO(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.STO
        return ev_raw
    
    @property
    def EV_UO(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.UO
        return ev_raw
    
    @property
    def EV_DCA(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.DCA
        return ev_raw
    
    @property
    def EV_HSD(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.HSD
        return ev_raw
    
    @property
    def EV_ETC(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.ETC
        return ev_raw
    
    @property
    def EV_VOV(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.VOV
        return ev_raw
    
    @property
    def EV_MCO(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.MCO
        return ev_raw
    
    @property
    def EV_FLT9(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.FLT9
        return ev_raw
    
    @property
    def EV_FLT10(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.FLT10
        return ev_raw
    
    @property
    def EV_PSE1(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.PSE1
        return ev_raw
    
    @property
    def EV_PSE2(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.PSE2
        return ev_raw
    
    @property
    def EV_ADC_SAT(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.ADC_SAT
        return ev_raw
    
    @property
    def EV_VIN_OV(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.VIN_OV
        return ev_raw
    
    @property
    def EV_VIN_UV(self):
        if self.telemetry1 is None:
            return None
        ev_raw = self.telemetry1.telemetry_level1.Enphase_Events.VIN_UV
        return ev_raw

    @property
    def measurement_status_t(self):
        return "measurement_status_t","struct"

    @property
    def ActWhs_Q14(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActWhs.Q14
        return prop_raw

    @property
    def ActWhs_Q23(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActWhs.Q14
        return prop_raw
        
    @property
    def ActVArhs_Q12(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVArhs.Q12
        return prop_raw

    @property
    def ActVArhs_Q34(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVArhs.Q34
        return prop_raw


    @property
    def ActVAhs_Q1(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVAhs.Q1
        return prop_raw

        
    @property
    def ActVAhs_Q2(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVAhs.Q2
        return prop_raw
        
    @property
    def ActVAhs_Q3(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVAhs.Q3
        return prop_raw
        
    @property
    def ActVAhs_Q4(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.ActVAhs.Q4
        return prop_raw
        
    @property
    def VArAval(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.VArAval
        return prop_raw

    @property
    def WAval(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.WAval
        return prop_raw
    
    # StSetLimMsk
    @property
    def limits_mask(self):
        return "limits_mask","struct"

    @property
    def StSetLimMsk(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.StSetLimMsk
        return prop_raw
    
    @property
    def LIMIT_FROM_WMax(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 0)) != 0
    
    @property
    def LIMIT_FROM_VAMax(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 1)) != 0
    
    @property
    def LIMIT_FROM_VArAval(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 2)) != 0
    
    @property
    def LIMIT_FROM_VArMaxQ1(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 3)) != 0
    
    @property
    def LIMIT_FROM_VArMaxQ2(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 4)) != 0
    
    @property
    def LIMIT_FROM_VArMaxQ3(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 5)) != 0
    
    @property
    def LIMIT_FROM_VArMaxQ4(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 6)) != 0
    
    @property
    def LIMIT_FROM_PFMinQ1(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 7)) != 0
    
    @property
    def LIMIT_FROM_PFMinQ2(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 8)) != 0
    
    @property
    def LIMIT_FROM_PFMinQ3(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 9)) != 0
    
    @property
    def LIMIT_FROM_PFMinQ4(self):
        if self.telemetry2 is None:
            return None
        return (self.StSetLimMsk & (1 << 10)) != 0
    
# StActCtl
    @property
    def using_mask(self):
        return "using_mask","struct"
        
    @property
    def StActCtl(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.StActCtl
        return prop_raw
    
    @property
    def USING_FixedW(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 0)) != 0
    
    @property
    def USING_FixedVAR(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 1)) != 0
    
    @property
    def USING_FixedPF(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 2)) != 0
    
    @property
    def USING_Volt_VAr(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 3)) != 0
    
    @property
    def USING_Freq_Watt_Param(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 4)) != 0
    
    @property
    def USING_Freq_Watt_Curve(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 5)) != 0
    
    @property
    def USING_Dyn_Reactive_Current(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 6)) != 0
    
    @property
    def USING_LVRT(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 7)) != 0
    
    @property
    def USING_HVRT(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 8)) != 0
    
    @property
    def USING_Watt_PF(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 9)) != 0
    
    @property
    def USING_Volt_Watt(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 10)) != 0
    
    @property
    def USING_Scheduled(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 11)) != 0
    
    @property
    def USING_LFRT(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 12)) != 0
    
    @property
    def USING_HFRT(self):
        if self.telemetry2 is None:
            return None
        return (self.StActCtl & (1 << 13)) != 0

#  RtSt
    @property
    def ride_through_mask(self):
        return "ride_through_mask","struct"
    
    @property
    def RtSt(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.RtSt
        return prop_raw

    @property
    def LVRT_ACTIVE(self):
        if self.telemetry2 is None:
            return None
        return (self.RtSt & (1 << 0)) != 0
        
    @property
    def HVRT_ACTIVE(self):
        if self.telemetry2 is None:
            return None
        return (self.RtSt & (1 << 1)) != 0
        
    @property
    def LFRT_ACTIVE(self):
        if self.telemetry2 is None:
            return None
        return (self.RtSt & (1 << 2)) != 0
        
    @property
    def HFRT_ACTIVE(self):
        if self.telemetry2 is None:
            return None
        return (self.RtSt & (1 << 3)) != 0
    
    @property
    def times_and_resistance(self):
        return "time_and_isolation_resistance","struct"
    

    @property
    def Tms(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.Tms
        return prop_raw

    @property
    def Ris(self):
        if self.telemetry2 is None:
            return None
        prop_raw = self.telemetry2.telemetry_level2.Ris
        return prop_raw

        # Signal Strength
    @property
    def signal_strength(self):
        return "signal_strength","struct"
    
    
    @property
    def RxRSSI(self):
        return None
    
    @property
    def TxRSSI(self):
        return None
        # Read/Write properties
        
def format_smp_firmware(spm_version):
    fw = str(spm_version.fwrevision >> 16)+"."
    fw += str((spm_version.fwrevision >> 8) & 0xFF)
    fw += "."+str((spm_version.fwrevision) & 0xFF)
    sn = unpack_chars(spm_version.long_serial)
    return fw,sn
    
        
def tohex(c):
    if c == 'a':
        return 10
    if c == 'b':
        return 11
    if c == 'c':
        return 12
    if c == 'd':
        return 13
    if c == 'e':
        return 14
    if c == 'f':
        return 15
    return int(c)
    
        
def cvt(token):
    return (tohex(token[0]) << 4 | tohex(token[1])).to_bytes(1,"big")
        
if __name__ == "__frodo__":
    
        y =  "aa 06 00 00 80 25 01 01 00 00 00 00 00 00 c0 00 b4 7f 00 1b 07 22 01 01 5b 91 89 ea 00 01 01 01 01 00 0b 00 00 00 00 00 00 00 00 a7 39".split()
        print(y)
        arr = b''
        for b in y:
            arr += cvt(b)
            
        print(arr.hex())
        print("%04x" % crc16ccitt(arr[:-2],len(arr[:-2]),0xFFFF))
        
if __name__ == "__main__":
    ch = plc_channel("COM12")
    try:
        spm_version = ch.GetSPMVersionInfo()
        fw,sn = format_smp_firmware(spm_version)
        print("\nFirmware version:",fw)
        print("Serial number:",sn,"\n")
        
#        ch.UpgradeSPMFirmware("e-asic_firmware_for_mime2.bin")
        pcus = ch.Discover(1)#expected_pcus=3)
        print("My list: ",pcus[0].SerialNumber,"\n\n\n")
#        
#        ch.UpdateFW("procload_IQ8_01_79d4c710-mods.swe","procload-metadata_IQ8_01_79d4c710-mods.tch",pcus)
#        ch.UpdateDMIR("param_asic_IQ8Plus_01_US_PV-72_DVT_p000-00000_r00_v1.1.0_d0_79d4c710-mods.tch",MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE,pcus)
#        ch.UpdateDMIR("param_sunspec_IQ8Plus_01_US_PV-72_DVT_p000-00000_r00_v1.1.0_d0_79d4c710-mods.tch",MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE,pcus)
#        pcus[0].UpdateFWInfo()
        
        pcus[0].UpdateTelemetry()
        
        print()
        pcus[0].DisplayProperties()
        print()
        
        
        
    finally:
        print("\n\nClosing channel")
        ch.close()

            
        

