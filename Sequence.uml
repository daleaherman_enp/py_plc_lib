@startuml


skinparam titleBorderRoundCorner 15
skinparam titleBorderThickness 2
skinparam titleBorderColor red
skinparam titleBackgroundColor Aqua-CadetBlue

title PLC Tools\nSequence Diagram


participant "User Script" as User
participant "PLC Tools" as PLCT
participant "Dev Board" as Dev
participant MicroInverterArray
participant MicroInverter

== Initialization ==

User -> PLCT : Create PLC Channel
alt Success
	PLCT -> User : Return PLC Channel Object
else Failed
    PLCT -> User : Raise Exception
end

== Discovery ==

User -> PLCT: Discover
loop
	PLCT -> Dev : Broadcast Search command
	Dev -> MicroInverterArray : Relay to PLC Bus
	alt A MicroInverter responds
		MicroInverter -> Dev : Return MicroInverter Device Info
		Dev -> PLCT : Relay response from PLC Bus
		note left: PLC Tools adds MicroInverter to list
		PLCT -> Dev : Squelch MicroInverter for n seconds
		Dev -> MicroInverterArray : Relay to PLC Bus
	else Timeout
		PLCT -> User : Return MicroInverter List
	end
end

== Updating Backing Variables ==

User -> PLCT : Fetch Telemetry for MicroInverter
PLCT -> Dev : Fetch Telemetry for MicroInverter
Dev -> MicroInverterArray : Relay to PLC Bus
alt Success
	MicroInverter -> Dev : Telemetry Info
	Dev -> PLCT : Relay response from PLC Bus
	note left: PLC Tools records telemetry\nin private backing variables
else Failed
	PLCT -> User : raise Exception
	note left: PLC Tools nulls-out backing variables
end

== Getting/Setting Properties ==

User -> PLCT : Get Property
PLCT -> User : Returns backing variable or None (null)

User -> PLCT : Set Property
alt Property Validates
	PLCT -> User : Acknowledged
else Invalid Property
	PLCT -> User : raise Exception
end

== Pushing Information to MicroInverters ==

User -> PLCT : Push
alt Push Valid
	PLCT -> Dev : Unicast push of requested data
	Dev -> MicroInverterArray : Relay to PLC Bus
	note right: Unicast messages are in the blind.\nNo response from MicroInverter.
else Push Invalid
	PLCT -> User : raise Exception
end

== Finalizing ==

User -> PLCT : Close PLC Channel
PLCT -> User : Acknowledged
note left: PLC channel is no longer valid.
	
	


@enduml