# ---------------------------------------------------------
#
# This file is auto-generated from C header file: enphase_packet.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	tIntervalLongData

class tIntervalLongData(Struct):
	_format = Format.LittleEndian
	joulesProduced = Type.UnsignedLongLong
	joulesUsed = Type.UnsignedLongLong
	sumLeadingVAr = Type.UnsignedLongLong
	sumLaggingVAr = Type.UnsignedLongLong

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

__PVSLIB_ENPHASE_PACKET_H = True
ENPHASE_MAX_PACKET = 768
