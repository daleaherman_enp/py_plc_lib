# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_counters.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	tPlcCounters

class tPlcCounters(Struct):
	_format = Format.LittleEndian
	allRxFrames = Type.UnsignedShort
	bcastRxFrames = Type.UnsignedShort
	ackRxFrames = Type.UnsignedShort
	forMeRxFrames = Type.UnsignedShort
	rxFrameErrors = Type.UnsignedShort
	rxOverRuns = Type.UnsignedShort
	collisions = Type.UnsignedShort
	ackNoResponseCnt = Type.UnsignedShort
	frameTooLongCnt = Type.UnsignedShort
	txUnderRuns = Type.UnsignedShort
	txOverRuns = Type.UnsignedShort
	txFrames = Type.UnsignedShort
	unexpectedChipResponses = Type.UnsignedShort
	abnormalLineBusyEvents = Type.UnsignedShort
	reservedNibbleErrors = Type.UnsignedShort
	invalidProtocolId = Type.UnsignedShort


#	tAuroraDetailedCounters

class tAuroraDetailedCounters(Struct):
	_format = Format.LittleEndian
	totBaudsCnt = Type.UnsignedLong
	corrHdrSymbCnt = Type.UnsignedLong
	uncorrRsPyldSymbCnt = Type.UnsignedLong
	corrPyldSymbCnt = Type.UnsignedLong
	pyldSymbCnt = Type.UnsignedLong
	cryptKey0PktCnt = Type.UnsignedShort
	cryptKey1PktCnt = Type.UnsignedShort
	snr = Type.UnsignedShort
	fill = Type.UnsignedShort
	ravPreamblesCnt = Type.UnsignedShort
	okHdrCrcCnt = Type.UnsignedShort
	badHdrCrcCnt = Type.UnsignedShort
	goodHdrsCnt = Type.UnsignedShort
	uncorrHdrsCnt = Type.UnsignedShort
	pyldCrcErrCnt = Type.UnsignedShort
	macTxPktOutCnt = Type.UnsignedShort
	macRxOvflwCnt = Type.UnsignedShort
	macRxPktOutCnt = Type.UnsignedShort
	macRxPktInCnt = Type.UnsignedShort


#	tControllerCnts

class tControllerCnts(Struct):
	_format = Format.LittleEndian
	rxMsgs = Type.UnsignedShort
	txMsgs = Type.UnsignedShort
	fpMsgs = Type.UnsignedShort
	paMsgs = Type.UnsignedShort
	txFull = Type.UnsignedShort
	badMsgHdr = Type.UnsignedShort
	badProto = Type.UnsignedShort
	badScopePt = Type.UnsignedShort
	otherModId = Type.UnsignedShort
	checksumBad = Type.UnsignedShort
	ignoredOtherDomain = Type.UnsignedShort
	otherDomainScanMsgs = Type.UnsignedShort
	otherDomainDirected = Type.UnsignedShort
	badDir = Type.UnsignedShort
	repeatedRxMsgs = Type.UnsignedShort
	fill3 = Type.UnsignedShort


#	tMcpSsiMeasures

class tMcpSsiMeasures(Struct):
	_format = Format.LittleEndian
	idleLineSsi = Type.UnsignedShort
	rxEmuSsi = Type.UnsignedShort
	squelch = Type.UnsignedShort
	flagsAndPlcIdx = Type.UnsignedShort

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

_mcp_counters_h_ = True
PACKING = True
cMcpSsiFlagsMsk = 0xffc0
cMcpSsiFlagsShf = 6
cMcpSsiFlagInDbuV = 0x200
cMcpSsiFlagFsk4Msk = 0x100
cMcpSsiFlagImgUpgEnh1 = 0x080
cMcpSsiFlagPlcSec = 0x040
cMcpSsiPlcIdxMsk = 0x003f
cMcpSsiPlcIdxShf = 0
cMcpSsiPlcIdxOriginal = 0
cMcpSsiPlcIdxRaven1Resi = 1
cMcpSsiPlcIdxRaven1Commercial = 2
cMcpSsiPlcIdxAurora2Resi = 3
cMcpSsiPlcIdxHeron2Asic = 4
cMcpSsiPlcIdxNumOf = 5
