# ---------------------------------------------------------
#
# This file is auto-generated from C header file: agf_structs.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

cAgfSsFreqHzScale = (256)
cAgfScaleBy1000 = 1000
cAgfSsVoltToMVScale = (cAgfScaleBy1000)
cAgfVwScaleTimeConstant = (0x400000)
cAgfVw52ScaleNegTimeConstant = (0x800000)
cAgfInv2TimeoutPeriodMask = 0x00FFFFFF
cAgfTrans6Points = 6
cAgfTrans10Points = 10
cAgfTrans2Values = 2
cAgfTrans2Points = 2
cAgfTrans8Points = 8
cAgfTrans2Curves = 2
cAgfTransPtPair = 2

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	tAGFQueryMsg

class tAGFQueryMsg(Struct):
	_format = Format.LittleEndian
	functionType = Type.Long


#	tAgfCurvePoint

class tAgfCurvePoint(Struct):
	_format = Format.LittleEndian
	value = Type.Long[cAgfTrans2Values]


#	tAgfInverterCurvePoint

class tAgfInverterCurvePoint(Struct):
	_format = Format.LittleEndian
	point = Type.Struct(tAgfCurvePoint)[cAgfTrans2Points]


#	tAgfInverterCurve

class tAgfInverterCurve(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(tAgfInverterCurvePoint)[cAgfTrans10Points]


#	tAgfInverterCurveVRT

class tAgfInverterCurveVRT(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(tAgfInverterCurvePoint)[cAgfTrans8Points]


#	tAgfInverterCurveWVAR

class tAgfInverterCurveWVAR(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(tAgfCurvePoint)[cAgfTrans10Points]


#	tAgfInverterCurvePair

class tAgfInverterCurvePair(Struct):
	_format = Format.LittleEndian
	curve = Type.Struct(tAgfInverterCurve)[cAgfTrans2Curves]


#	tAgfInverterCurvePairVRT

class tAgfInverterCurvePairVRT(Struct):
	_format = Format.LittleEndian
	curve = Type.Struct(tAgfInverterCurveVRT)[cAgfTrans2Curves]


#	tAgfVVARMsg

class tAgfVVARMsg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	startPoint = Type.Long
	stopPoint = Type.Long
	curves = Type.Struct(tAgfInverterCurvePair)
	maxSlopeDecrease = Type.Long
	maxSlopeIncrease = Type.Long
	curveType = Type.Long
	typeBMin = Type.Long
	typeBMax = Type.Long


#	tAgfFRTMsg

class tAgfFRTMsg(Struct):
	_format = Format.LittleEndian
	curves = Type.Struct(tAgfInverterCurvePair)
	maxTime = Type.Long
	time = Type.Long


#	tAgfVRTMsg

class tAgfVRTMsg(Struct):
	_format = Format.LittleEndian
	curves = Type.Struct(tAgfInverterCurvePairVRT)
	maxTime = Type.Long


#	tAgfFPFMsg

class tAgfFPFMsg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	lockInVoltage = Type.Long
	powerFactor = Type.Long
	lockOutVoltage = Type.Long
	startpoint = Type.Long


#	tAgfPRLMsg

class tAgfPRLMsg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	limit = Type.Long


#	tAgfSSMsg

class tAgfSSMsg(Struct):
	_format = Format.LittleEndian
	mode = Type.Long
	reconnectFrequencyLow = Type.Long
	reconnectFrequencyHigh = Type.Long
	reconnectVoltageLow = Type.Long
	reconnectVoltageHigh = Type.Long
	reconnectAiTime = Type.Long
	reconnectPowerRampRate = Type.Long
	shortAiFailTime = Type.Long
	shortAiTime = Type.Long
	aiTime = Type.Long


#	tAgfVWMsg

class tAgfVWMsg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	curves = Type.Struct(tAgfInverterCurvePair)
	maxSlopeDecrease = Type.Long
	maxSlopeIncrease = Type.Long
	curveType = Type.Long
	typeBMin = Type.Long
	typeBMax = Type.Long
	timeConstant = Type.Long


#	tAgfVW52Msg

class tAgfVW52Msg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	curves = Type.Struct(tAgfInverterCurvePair)
	maxSlopeDecrease = Type.Long
	maxSlopeIncrease = Type.Long
	curveType = Type.Long
	typeBMin = Type.Long
	typeBMax = Type.Long
	timeConstant = Type.Long


#	tAgfINV2Msg

class tAgfINV2Msg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long
	powerMax = Type.Long
	rampRate = Type.Long
	timeWindow = Type.Long
	timeoutPeriod = Type.Long


#	tAgfWPMsg

class tAgfWPMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	wstr = Type.Long
	pfstr = Type.Long
	wstop = Type.Long
	pfstop = Type.Long
	slope = Type.Long
	lockInVoltage = Type.Long
	lockOutVoltage = Type.Long


#	tAgfTVMsg

class tAgfTVMsg(Struct):
	_format = Format.LittleEndian
	isEnabled = Type.Long


#	tAgfFWMsg

class tAgfFWMsg(Struct):
	_format = Format.LittleEndian
	modeSelection = Type.Long
	frequencyStart = Type.Long
	frequencyStopHigh = Type.Long
	frequencyStopLow = Type.Long
	frequencySlope = Type.Long
	frequencyMaxTimeSlope = Type.Long
	frequencyTimeSlope = Type.Long
	frequencyStopTime = Type.Long
	frequencyStartDelay = Type.Long


#	tAgfFW22Msg

class tAgfFW22Msg(Struct):
	_format = Format.LittleEndian
	modeSelection = Type.Long
	frequencyStart = Type.Long
	frequencyStop = Type.Long
	frequencyStopLow = Type.Long
	frequencySlope = Type.Long
	frequencyMaxTimeSlope = Type.Long
	frequencyTimeSlope = Type.Long
	frequencyStopTime = Type.Long
	frequencyStartDelay = Type.Long


#	tAgfPLPMsg

class tAgfPLPMsg(Struct):
	_format = Format.LittleEndian
	maxVar = Type.Long
	maxWatt = Type.Long


#	tAgfACAVEMsg

class tAgfACAVEMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	aveHigh = Type.Long


#	tAgfISLNDMsg

class tAgfISLNDMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	tone = Type.Long
	harmonics_diff = Type.Long
	harmonics_increase = Type.Long
	harmonics_max = Type.Long
	fundamental_diff = Type.Long
	freq_deviation = Type.Long


#	tAgfIACMsg

class tAgfIACMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	fast = Type.Long
	slow = Type.Long


#	tAgfVECTMsg

class tAgfVECTMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	trip_angle = Type.Long
	trip_time = Type.Long
	trip_angle_islanding = Type.Long


#	tAgfROCOFMsg

class tAgfROCOFMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	cycles = Type.Long
	constant = Type.Long


#	tAgfWVARMsg

class tAgfWVARMsg(Struct):
	_format = Format.LittleEndian
	enable = Type.Long
	curves = Type.Struct(tAgfInverterCurveWVAR)
	maxSlopeDecrease = Type.Long
	maxSlopeIncrease = Type.Long
	timeConstant = Type.Long


#	tAgfCookieData

class tAgfCookieData(Struct):
	_format = Format.LittleEndian
	type = Type.UnsignedByte
	version = Type.UnsignedByte
	md5 = Type.UnsignedByte[8]


#	tAgfCookieMsg

class tAgfCookieMsg(Struct):
	_format = Format.LittleEndian
	count = Type.UnsignedByte
	fillBytes = Type.UnsignedByte[3]
	cookie = Type.Struct(tAgfCookieData)[1]


#	tAgfTransPcuCurveData

class tAgfTransPcuCurveData(Struct):
	_format = Format.LittleEndian
	hCurveLNCount = Type.Long
	hCurveLN = Type.Float[cAgfTrans10Points*cAgfTransPtPair]
	hPointMask = Type.UnsignedShort
	lCurveLNCount = Type.Long
	lCurveLN = Type.Float[cAgfTrans10Points*cAgfTransPtPair]
	lPointMask = Type.UnsignedShort


#	tAgfPts

class tAgfPts(Struct):
	_format = Format.LittleEndian
	type = Type.UnsignedByte
	fill = Type.UnsignedByte[3]
	recOpd = Type.UnsignedByte[8]
	cd = Type.Struct(tAgfTransPcuCurveData)


#	tAgfCurvePts

class tAgfCurvePts(Struct):
	_format = Format.LittleEndian
	pts = Type.Struct(tAgfPts)


#	uAdcRecords

class uAdcRecords(Union):
	_format = Format.LittleEndian
	agfCookie = Type.Struct(tAgfCookieMsg)
	agfVVAR = Type.Struct(tAgfVVARMsg)
	agfFRT = Type.Struct(tAgfFRTMsg)
	agfVRT = Type.Struct(tAgfVRTMsg)
	agfFPF = Type.Struct(tAgfFPFMsg)
	agfPRL = Type.Struct(tAgfPRLMsg)
	agfSS = Type.Struct(tAgfSSMsg)
	agfVW = Type.Struct(tAgfVWMsg)
	agfINV2 = Type.Struct(tAgfINV2Msg)
	agfWP = Type.Struct(tAgfWPMsg)
	agfTV = Type.Struct(tAgfTVMsg)
	agfFW = Type.Struct(tAgfFWMsg)
	agfPLP = Type.Struct(tAgfPLPMsg)
	agfISLND = Type.Struct(tAgfISLNDMsg)
	agfIAC = Type.Struct(tAgfIACMsg)
	agfVECT = Type.Struct(tAgfVECTMsg)
	agfROCOF = Type.Struct(tAgfROCOFMsg)
	agfACAVE = Type.Struct(tAgfACAVEMsg)
	agfCurvePts = Type.Struct(tAgfCurvePts)
	agfVW52 = Type.Struct(tAgfVW52Msg)
	agfFW22 = Type.Struct(tAgfFW22Msg)
	agfWVAR = Type.Struct(tAgfWVARMsg)

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

AGF_STRUCTS_H_ = True
cAgfFuncEnable = 0x01
cAgfFuncDisable = 0x00
cAgfScaleBy10 = 10
cAgf24BitDataMask = 0xFFFFFF
cAgfRegionNA = 0
cAgfRegionEU = 1
cAgfTransPtTriple = 3
cAgfTransPtQuad = 4
cAgfTransCurveHigh = 0
cAgfTransCurveLow = 1
cAgfEnvoyDataMask = 0xF0000000
cAgfVvarEnable = 0x00000001
cAgfVvarReactivePriority = 0x01000000
cAgfVv11MinQxPt = (-1.0)
cAgfVv11MaxQxPt = (1.0)
cAgfVv11DefaultStopPoint = (350.0)
cAgfVVARVoltageZero = (0.0)
cAgfVVARVoltageMax = (600.0)
cAgfVvarFlagsHeco = 0x80000000
cAgfVvarFlagsP1547 = 0x00000002
cAgfVvarHecoCurveTypeMask = 0xff000000
cAgfVvarHecoTimeConstMask = 0x0000ffff
cAgfVvarHecoCurveTypeMax = 255
cAgfVvarHecoCurveTypeShift = 24
cAgfVvarHecoMinTimeShift = 0
cAgfVvarHecoMinTimeConst = 0.0
cAgfVvarHecoMaxTimeConst = 655.35
cAgfVvarHecoTimeConstScale = 100
cAgfVvarP1547MinVRef = 0.9
cAgfVvarP1547MaxVRef = 1.1
cAgfVvarP1547VRefScale = 1000
cAgfVvarP1547MinVRefTimeConst = 0
cAgfVvarP1547MaxVRefTimeConst = 65535
cAgfFrtHighLowTimeMin = (0)
cAgfFrtHighLowTimeMax = (255)
cAgfFrtHighLowTimeDefault = (6)
cVRTCessationBit = 0x01000000
cAgfLockVoltageDefault = (0.0)
cAgfLockVoltageMax = (600.0)
cAgfFpfScale = (0x400000)
cAgfFpfMaxPositive = (1.0)
cAgfFpfMinPositive = (0.068)
cAgfFpfMaxNegitive = (-0.068)
cAgfFpfMinNegitive = (-1.0)
cAgfFpfNegMask = (0xFF000000)
cAgfFpfReactivePriority = 0x01000000
cAgfFpfStartPointScale = 256
cAgfFpfStartPointMin = (0.0)
cAgfFpfStartPointMax = (100.0)
cAgfFpfStartPointDefault = (0.0)
cAgfSsRcFreqLowDefaultNA = (59.3*cAgfSsFreqHzScale)
cAgfSsRcFreqHighDefaultNA = (60.5*cAgfSsFreqHzScale)
cAgfSsRcFreqLowMinNA = (50*cAgfSsFreqHzScale)
cAgfSsRcFreqLowMaxNA = (60*cAgfSsFreqHzScale)
cAgfSsRcFreqHighMinNA = (60*cAgfSsFreqHzScale)
cAgfSsRcFreqHighMaxNA = (67*cAgfSsFreqHzScale)
cAgfSsRcFreqLowDefaultEU = (45*cAgfSsFreqHzScale)
cAgfSsRcFreqHighDefaultEU = (55*cAgfSsFreqHzScale)
cAgfSsRcFreqLowMinEU = (40*cAgfSsFreqHzScale)
cAgfSsRcFreqLowMaxEU = (50*cAgfSsFreqHzScale)
cAgfSsRcFreqHighMinEU = (50*cAgfSsFreqHzScale)
cAgfSsRcFreqHighMaxEU = (57*cAgfSsFreqHzScale)
cAgfSsRcVoltLowDefaultNA = (106*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighDefaultNA = (127*cAgfSsVoltToMVScale)
cAgfSsRcVoltLowMinNA = (96*cAgfSsVoltToMVScale)
cAgfSsRcVoltLowMaxNA = (240*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighMinNA = (120*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighMaxNA = (288*cAgfSsVoltToMVScale)
cAgfSsRcVoltLowDefaultEU = (184*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighDefaultEU = (276*cAgfSsVoltToMVScale)
cAgfSsRcVoltLowMinEU = (184*cAgfSsVoltToMVScale)
cAgfSsRcVoltLowMaxEU = (240*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighMinEU = (230*cAgfSsVoltToMVScale)
cAgfSsRcVoltHighMaxEU = (288*cAgfSsVoltToMVScale)
cAgfSsRcAiDefault = (18300)
cAgfSsRcAiMin = (120)
cAgfSsRcAiMax = (32767)
cAgfSsRcPRRDefault = (1500)
cAgfSsRcPRRMin = (0)
cAgfSsRcPRRMax = (60000)
cAgfSsUseAiTime = (0x00000001)
cAgfSsUseAtPwrUp = (0x00000002)
cAgfSsUseAfterVoltTrip = (0x00000004)
cAgfSsUseAfterFreqTrip = (0x00000008)
cAgfSsUseAfterDcInjTrip = (0x00000010)
cAgfSsAiDefault = (18300)
cAgfSsAiMin = (120)
cAgfSsAiMax = (32767)
cAgfSsAiShortFailTime = (3)
cAgfSsAiShortFailTimeCycMin = (0)
cAgfSsAiShortFailTimeCycMax = (255)
cAgfSsAiShortTime = (5)
cAgfSsAiShortTimeCycMin = (120)
cAgfSsAiShortTimeCycMax = (32767)
cAgfSsReconnectAiTimeNsrb = (5)
cAgfSsAiTimeNsrb = (5)
cAgfSsAiShortTimeNsrb = (5)
cAgfVwFlagsEnabled = 0x00000001
cAgfVwFlagsHeco = 0x80000000
cAgfVwFlagsPriority = 0x00010000
cAgfVwFlagsPreDisturb = 0x01000000
cAgfVwHecoMaxTimeConst = 655.35
cAgfVwHecoTimeConstScale = 100
cAgfVw51MinPwrPct = (0.0)
cAgfVw51MaxPwrPct = (1.0)
cAgfVw51DefaultTimeConstant = (cAgfVwScaleTimeConstant)
cAgfVw52DefaultTimeConstant = (cAgfVwScaleTimeConstant|cAgfVw52ScaleNegTimeConstant)
cAgfVwMaxTimeConstant = (cAgfVwScaleTimeConstant)
#cAgfVwMinTimeConstant = (cAgfVw52ScaleTimeConstant)
cAgfVwMinTimeConstantSec = (0.0)
cAgfVw52MinPwrPct = (-1.0)
cAgfVw52MaxPwrPct = (0.0)
cAgfInv2FlagsEnableMask = 0x00000001
cAgfInv2FlagsEnabled = 0x00000001
cAgfInv2FlagsDisabled = 0x00000000
cAgfInv2FlagsRampMask = 0x00000002
cAgfInv2FlagsRamp = 0x00000000
cAgfInv2FlagsStep = 0x00000002
cAgfInv2FlagsPowerCmdMask = 0x00000004
cAgfInv2FlagsAbsolute = 0x00000000
cAgfInv2FlagsRelativeDown = 0x00000004
cAgfInv2FlagsPowerTypeMask = 0x00000008
cAgfInv2FlagsReal = 0x00000000
cAgfInv2FlagsApparent = 0x00000008
cAgfInv2FlagsOff = 0x00000010
cAgfInv2FlagsOffNone = 0x00000000
cAgfInv2FlagsOffAll = 0x00000010
cAgfInv4FlagsSleepModeMask = 0x00000020
cAgfInv4FlagsSleepModeOff = 0x00000000
cAgfInv4FlagsSleepModeOn = 0x00000020
cAgfInv2PowerMaxScale = (256)
cAgfInv2RelPcuPowerMax = (100)
cAgfInv2RelPcuPowerMin = (0)
cAgfInv2RelPcuPowerDefault = (0)
cAgfInv2AbsPcuPowerMax = (100)
cAgfInv2AbsPcuPowerMin = (0)
cAgfInv2AbsPcuPowerDefault = (0)
cAgfInv2RelAcbPowerMax = (100)
cAgfInv2RelAcbPowerMin = (0)
cAgfInv2RelAcbPowerDefault = (0)
cAgfInv2AbsAcbPowerMax = (100)
cAgfInv2AbsAcbPowerMin = (-100)
cAgfInv2AbsAcbPowerDefault = (0)
cAgfInv2TimeoutPeriodMin = 0
cAgfInv2TimeoutPeriodMax = cAgfInv2TimeoutPeriodMask
cAgfWp41Enable = 0x00000001
cAgfWpReactivePriority = 0x01000000
cAgfWp41Scale = 0x00400000
cAgfWp41PfMin = 0.707
cAgfWp41PfMax = 1.0
cAgfWp41RatedmWatts = 285000
cAgfWp41WattMinPercentage = 0.0
cAgfWp41WattMaxPercentage = 100.0
cAgfFwEnable = 0x00000001
cAgfFwHystereis = 0x00000002
cAgfFwSlopeSelection = 0x00000004
cAgfFwFlagsHeco = 0x80000000
cAgfFwFlagsPriority = 0x00010000
cAgfFwStartDelayDefault = 0
cAgfFwStartDelayMin = 0
cAgfFwStartDelayMax = 255
cAgfFwStartDelayMask = 0x000000FF
cAgfFwUnderFreqMask = 0x00FFFFFF
cAgfFwUnderFreqDataMask = 0xFFFFFF00
cAgfFwUnderFreqShift = 8
cAgfFwUnderFreqMin = 1.0
cAgfFwUnderFreqMax = 500.0
cAgfFwUnderFreqDefault = 33.3
cAgfFwUnderFreqScale = 65.536
cAgfFwHecoStopTimeMax = 1000
cAgfFwHecoStopTimeMask = 0x0000FFFF
cAgfFwHecoTimeConstMask = 0xFFFF0000
cAgfFwHecoTimeConstShift = 16
cAgfFwHecoMinTimeConst = 0.0
cAgfFwHecoMaxTimeConst = 655.35
cAgfFwHecoTimeConstScale = 100
cAgfPLPScale = 0x00010000
cAgfACAVEEnable = 0x00000001
cAgfACAVEDefaultNA = 264000
cAgfACAVEDefaultEU = 277000
cAgfACAVEHighMin = 208000
cAgfACAVEHighMax = 300000
cAgfISLNDEnable = 0x00000001
cAgfISLNDJapanMode = 0x00000002
cAgfISLNDOffForDetection = 0x00000004
cAgfISLNDToneAmpMask = 0xFF000000
cAgfISLNDToneAmpShift = 24
cAgfISLNDToneAmpDefault = 4
cAgfISLNDToneAmpMin = 0
cAgfISLNDToneAmpMax = 16
cAgfISLNDToneMaxMask = 0x00FF0000
cAgfISLNDToneMaxShift = 16
cAgfISLNDToneMaxDefault = 8
cAgfISLNDToneMaxMin = 0
cAgfISLNDToneMaxMax = 16
cAgfISLNDToneCycMask = 0x0000FF00
cAgfISLNDToneCycShift = 8
cAgfISLNDToneCycDefault = 10
cAgfISLNDToneCycMin = 10
cAgfISLNDToneCycMax = 255
cAgfISLNDToneWaitMask = 0x000000FF
cAgfISLNDToneWaitShift = 0
cAgfISLNDToneWaitDefault = 10
cAgfISLNDToneWaitMin = 10
cAgfISLNDToneWaitMax = 255
cAgfISLNDHarmDiffDefault = 1899
cAgfISLNDHarmDiffMin = 0
cAgfISLNDHarmDiffMax = 20000
cAgfISLNDHarmMaxDefault = 3000
cAgfISLNDHarmMaxMin = 2000
cAgfISLNDHarmMaxMax = 20000
cAgfISLNDHarmIncreDefault = 400
cAgfISLNDHarmIncreMin = 100
cAgfISLNDHarmIncreMax = 1000
cAgfISLNDFundDiffDefault = 200000
cAgfISLNDFundDiffMin = 0
cAgfISLNDFundDiffMax = 200000
cAgfISLNDFreqDevDefault = 0.047
cAgfISLNDFreqDevMin = 0.01
cAgfISLNDFreqDevMax = 0.1
cAgfISLNDFreqDevScale = 256
cAgfIACDisableDCInj = 0x00000000
cAgfIACEnableSlowDCInj = 0x00000001
cAgfIACEnableFastDCInj = 0x00000002
cAgfIACFastMin = 0
cAgfIACFastMax = 32767
cAgfIACFastDefaultEU = 4096
cAgfIACFastDefaultNA = 0
cAgfIACSlowMin = 0
cAgfIACSlowMax = 4096
cAgfIACSlowDefaultEU = 256
cAgfIACSlowDefaultNA = 0
cAgfIACNsrbMilliAmpPerPgd = 0x00000001
cAgfIACNsrbPgdLimit = 24
cAgfIACNsrbMilliAmpMax = 0xb3b0
cAgfIACNsrbMilliAmpMin = 0xa
cAgfIACNsrbMilliAmpDefault = 0x3e8
cAgfVECTEnable = 0x00000001
cAgfVECTReconnectOnTrip = 0x00000002
cAgfVECTNotRegularTrip = 0x00000004
cAgfVECTAngleDefault = 0
cAgfVECTAngleMin = 0
cAgfVECTAngleMax = 255
cAgfVECTAngleIslndDefault = 12
cAgfVECTAngleIslndMin = 0
cAgfVECTAngleIslndMax = 255
cAgfVECTTimeCycDefault = 6
cAgfVECTTimeCycMin = 1
cAgfVECTTimeCycMax = 12
cAgfROCOFEnable = 0x00000001
cAgfROCOFReconnectTrip = 0x00000002
cAgfROCOFTimeCycleMin = 0
cAgfROCOFTimeCycleMax = 600
cAgfROCOFTimeCycleDefault = 5
cAgfROCOFConstantScale = 33554
cAgfROCOFConstantMin = 0.05
cAgfROCOFConstantMax = 5.0
cAgfROCOFConstantDefault = 1.0
cAgfWVARPtMinCnt = cAgfTrans6Points
cAgfWVARPtMaxCnt = cAgfTrans10Points
cAgfWVAREnable = 0x00000001
cAgfWVARConstantScale = 33554
cAgfWVARConstantMin = 0.05
cAgfWVARConstantMax = 5.0
cAgfWVARConstantDefault = 1.0
cAgfWVARMinPowerPt = (-1.0)
cAgfWVARMaxPowerPt = 1.0
cAgfWVAREnablePtCntMask = 0x00000F00
cAgfWVAREnablePtCntShift = 8
cAgfWVARCurveScale = 0x00400000
cagfCurvePtDataType = 0
cAgfVvarRecTypeNotUsed = 0
cAgfVvarRecTypeVV11 = 1
cAgfVvarRecTypeVV12 = 2
cAgfVvarRecTypeVV13 = 3
cAgfVvarRecTypeVV14 = 4
cAgfVvarRecTypeNumOf = 5
