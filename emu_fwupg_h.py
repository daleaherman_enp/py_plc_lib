# ---------------------------------------------------------
#
# This file is auto-generated from C header file: emu_fwupg.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	t_img_meta

class t_img_meta(Struct):
	_format = Format.LittleEndian
	img_offset = Type.Long
	img_length = Type.Long
	img_type = Type.Char[24]
	img_part_num = Type.Char[64]
	img_file_ext = Type.Char[4]
	file_md5_hash = Type.Char[32]
	img_compat = Type.Long
	img_file_name = Type.Char[80]
	img_validated_md5 = Type.Byte


#	t_hardwarePN

class t_hardwarePN(Struct):
	_format = Format.LittleEndian
	hw_pn = Type.Char[14]

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

_emu_fwupg_h_ = True
SUP_OK = 0
SUP_BAD = -1
SUP_NOT_OPEN = -2
SUP_END_LIST = -3
INV_OK = 0
INV_BAD = -1
INV_NOT_OPEN = -2
MCP_OK = 0
MCP_BAD = -1
MCP_DEV_NOT_FOUND = -2
MCP_IMG_EOF = -3
MCP_IMG_NXT = -4
