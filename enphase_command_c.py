# ---------------------------------------------------------
#
# This file is auto-generated from C header file: enphase_command.c 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

V6_MDE_BITMAP_SIZE = (64)

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	tMcpAddressHdr

class tMcpAddressHdr(Struct):
	_format = Format.LittleEndian
	domainAddr = Type.UnsignedByte[6]
	modAddr = Type.UnsignedShort


#	tPollCmdRequestV6

class tPollCmdRequestV6(Struct):
	_format = Format.LittleEndian
	msg_id = Type.UnsignedByte
	mde_bitmap = Type.UnsignedByte[V6_MDE_BITMAP_SIZE]

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

ENPH_MODULE_ADDRESS_MASK = 0x3FFF
ENPH_ADDRESSING_MODE_MASK = 0xC000
ENPH_ADDRESS_MODE_UNICAST = 0x0000
ENPH_ADDRESS_MODE_BCAST_DIRECTED = 0x4000
ENPH_ADDRESS_MODE_BCAST_MODZERO = 0x8000
ENPH_ADDRESS_MODE_BCAST_DOMAINZERO = 0xC000
