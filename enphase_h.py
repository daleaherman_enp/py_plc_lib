# ---------------------------------------------------------
#
# This file is auto-generated from C header file: enphase.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format


# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	AsicIdNumber

class AsicIdNumber(Struct):
	_format = Format.LittleEndian
	asic_id = Type.UnsignedLong[2]


cSerialNumBytesLen = 6
#	SerialNumber

class SerialNumber(Struct):
	_format = Format.LittleEndian
	byte_arr_ = Type.UnsignedByte[cSerialNumBytesLen]


#	SerialNumPacked

class SerialNumPacked(Struct):
	_format = Format.LittleEndian
	word1 = Type.UnsignedLong
	word2 = Type.UnsignedShort

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

_enphase_h_ = True
cPartNumIdLen = 4
cPartNumInStrFmtLen = 24
cenPhaseMacOuiByte1 = 0x00
cenPhaseMacOuiByte2 = 0x1d
cenPhaseMacOuiByte3 = 0xc0
cSerialNumChars = (cSerialNumBytesLen*2)
cDomainNumBytesLen = 6
cSerialNumStrLen = 25
cMacAddrStrLen = 18
cIpAddrStrLen = 16
cAsicPnumStringByteOffset3 = 0x26
cManuNumProgParts = 4
cL2DomainAddrStrLen = 13
ENP_OK = 0
ENP_ERROR = 1
ok = 0
error = 1
None_ = 0
Pcu = 1
Pcu2 = 2
Cmu = 3
Emu = 4
Pmu = 5
Amu = 6
Therm = 7
Rgm = 8
Zbrptr = 9
Eim = 10
Acb = 11
NumOf = 12
len128 = 0
len256 = 1
