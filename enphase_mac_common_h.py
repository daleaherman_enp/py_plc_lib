# ---------------------------------------------------------
#
# This file is auto-generated from C header file: enphase_mac_common.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format
from enphase_h import SerialNumber

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

MAX_DEVICES = 200
cMaxMcastGroupsPerDevice = 8

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	MsgHdr

class MsgHdr(Struct):
	_format = Format.LittleEndian
	length = Type.UnsignedByte


#	McpMsgHdr

class McpMsgHdr(Struct):
	_format = Format.LittleEndian
	embed_MsgHdr = Type.Struct(MsgHdr)
	len_seq_encr = Type.UnsignedByte
	msgCsum = Type.UnsignedShort


#	McastMembershipArray

class McastMembershipArray(Struct):
	_format = Format.LittleEndian
	group_list = Type.UnsignedByte[cMaxMcastGroupsPerDevice]


#	AdjacentDomainInfo

class AdjacentDomainInfo(Struct):
	_format = Format.LittleEndian
	domain_addr = Type.Struct(SerialNumber)
	last_heard = Type.Long


#	PlcFrameStats

class PlcFrameStats(Struct):
	_format = Format.LittleEndian
	totalRxFrames = Type.UnsignedShort
	goodRxFrames = Type.UnsignedShort
	txFrames = Type.UnsignedShort
	txAborted = Type.UnsignedByte
	hdrCrcErrors = Type.UnsignedByte
	hdrRsErrors = Type.UnsignedByte
	pyldCrcErrors = Type.UnsignedByte
	pyldRsErrors = Type.UnsignedByte
	rxOverRuns = Type.UnsignedByte
	corrHdrSymbCnt = Type.UnsignedShort
	corrPyldSymbCnt = Type.UnsignedShort
	rxAckTimeout = Type.UnsignedByte
	rxRespTimeout = Type.UnsignedByte
	txRetransmissionDrop = Type.UnsignedByte
	snr = Type.UnsignedShort
	txFramesWithErrCnt = Type.UnsignedShort
	txFramesWithoutErrCnt = Type.UnsignedShort


#	PlcProtocolStats

class PlcProtocolStats(Struct):
	_format = Format.LittleEndian
	totalRxBytes = Type.UnsignedShort
	totalTxBytes = Type.UnsignedShort
	noackreqRxFrames = Type.UnsignedShort
	ackreqRxFrames = Type.UnsignedShort
	appProtId = Type.UnsignedShort
	macCtrlProtId = Type.UnsignedShort
	legacyProtId = Type.UnsignedShort
	invalidProtocolId = Type.UnsignedShort
	forMeRxFrames = Type.UnsignedShort
	txAckNpp = Type.UnsignedShort
	rxFrameOursNoAck = Type.UnsignedShort
	txEndedWaitAck = Type.UnsignedShort
	txEndedWaitResp = Type.UnsignedShort
	txPreemption = Type.UnsignedShort
	txNppWaitNext = Type.UnsignedShort
	txFrames = Type.UnsignedShort
	rxFrames = Type.UnsignedShort
	totalRxNpp0 = Type.UnsignedShort
	totalRxNpp1 = Type.UnsignedShort
	totalRxNpp0Ack = Type.UnsignedShort
	txStartTimeout = Type.UnsignedByte
	txTransmittingTimeout = Type.UnsignedByte
	txNppTimeout = Type.UnsignedByte
	rxEndOfFrameTimeout = Type.UnsignedByte
	rxEndOfFrameError = Type.UnsignedByte
	txAppMsgs = Type.UnsignedShort
	txMacMsgs = Type.UnsignedShort
	txLegacyMsgs = Type.UnsignedShort
	rxMsgs = Type.UnsignedShort
	rxAppMsgs = Type.UnsignedShort
	rxMacMsgs = Type.UnsignedShort
	rxLegacyMsgs = Type.UnsignedShort
	encryptMsgsTxed = Type.UnsignedShort
	encryptMsgsRxed = Type.UnsignedShort
	checksumBad = Type.UnsignedByte
	reassIdxFindFails = Type.UnsignedByte
	badMsgLen = Type.UnsignedByte
	repeatedRxMsgs = Type.UnsignedByte
	appBufBusy = Type.UnsignedByte
	secSecureIgnoredUnencryptMsgs = Type.UnsignedByte
	noSessionErrors = Type.UnsignedByte
	wrongMsgID = Type.UnsignedByte


#	plcRemoteDevStats

class plcRemoteDevStats(Struct):
	_format = Format.LittleEndian
	txAppMsgs = Type.UnsignedShort
	rxAppMsgs = Type.UnsignedShort
	txFrames = Type.UnsignedShort
	rxFrames = Type.UnsignedShort
	totalRxNpp0Ack = Type.UnsignedShort
	rxAckTimeout = Type.UnsignedShort
	txAckNpp = Type.UnsignedShort
	hdrCrcErrors = Type.UnsignedShort
	hdrRsErrors = Type.UnsignedShort
	pyldCrcErrors = Type.UnsignedShort
	pyldRsErrors = Type.UnsignedShort
	txRetransmissionDrop = Type.UnsignedShort
	txAborted = Type.UnsignedShort
	rxOverRuns = Type.UnsignedShort
	txFramesWithoutErrCnt = Type.UnsignedShort
	txFramesWithErrCnt = Type.UnsignedShort


#	PlcCoordinatorStats

class PlcCoordinatorStats(Struct):
	_format = Format.LittleEndian

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

__ENPHASE_MAC_COMMON_H__ = True
cMaxMsgLen = 512
MIN_DEVICE_ID = 1
MAX_DEVICE_ID = MAX_DEVICES
MAX_MCAST_GROUPS = 16
cMaxLevelsQosScheduler = 5
cMaxFramePldLength = 128
cMaxAdjacentDomains = 8
cDeviceDiscoveryTimeoutMs = 1000
cResponseMacTimeoutMs = 50
cAckMacTimeoutMs = 60
cRxTimeoutsToGoOffline = 3
GROUP_ID_BROADCAST = 0
cMaxRetransmitAttempts = 3
cMaxRetxAttemptsAfterAbort = 8
app_debug = 22
app_debug_bc = 23
mac_debug = 251
app_msg_id_numof = 252
Urgent = 1
Mac = 2
Polling = 3
Bulk = 4
PhyMode = 10
Discovery = 9
ErrorTxFailure = 0
OkResponseRecd = 1
OkTxComplete = 2
ErrorTimeout = 3
OtherDomain = 4
first = 0
middle = 1
last = 2
app = 0
mac_ctrl = 1
legacy = 2
