# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mac_dev_if.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	TxMsgRequest

class TxMsgRequest(Struct):
	_format = Format.LittleEndian
	length = Type.UnsignedLong
	acknowledged = Type.Byte

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

__MAC_DEV_IF_H__ = True
