# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_mde_device.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

DEVMGR_LEN_ASSEMBLY_PART_NUM = 8
DEVMGR_LEN_BOARD_PART_NUM = 8
DEVMGR_LEN_ASIC_PART_NUM = 8
DEVMGR_LEN_SERIAL_NUM = 6
DEVMGR_LEN_DOMAIN = 6
ARENA_PART_NUM_BCD_LEN = 4
SCM_HASH_HEX_LEN = 4

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	MDE_payload_manu_device_info_t

class MDE_payload_manu_device_info_t(Struct):
	_format = Format.LittleEndian
	assembly_part_number = Type.UnsignedByte[DEVMGR_LEN_ASSEMBLY_PART_NUM]
	board_part_number = Type.UnsignedByte[DEVMGR_LEN_BOARD_PART_NUM]
	ASIC_part_number = Type.UnsignedByte[DEVMGR_LEN_ASIC_PART_NUM]
	serial_number = Type.UnsignedByte[DEVMGR_LEN_SERIAL_NUM]
	device_type = Type.UnsignedShort
	configuration = Type.UnsignedShort


#	MDE_shutoff_control_t

class MDE_shutoff_control_t(Struct):
	_format = Format.LittleEndian
	shutoff_requested_b = Type.UnsignedByte


#	MDE_shutoff_enable_t

class MDE_shutoff_enable_t(Struct):
	_format = Format.LittleEndian
	shutoff_is_allowed_b = Type.UnsignedByte


#	MDE_theft_detection_enable_t

class MDE_theft_detection_enable_t(Struct):
	_format = Format.LittleEndian
	enable_b = Type.UnsignedByte
	disconnection_timeout_mins = Type.UnsignedLong


#	MDE_payload_device_info_t

class MDE_payload_device_info_t(Struct):
	_format = Format.LittleEndian
	Kr_inverse = Type.Short
	Ka_U1d_ref_inverse = Type.Short
	domain = Type.UnsignedByte[DEVMGR_LEN_DOMAIN]


#	MDE_set_RT_clock_request_t

class MDE_set_RT_clock_request_t(Struct):
	_format = Format.LittleEndian
	seconds_since_epoch = Type.UnsignedLongLong


#	canonical_version_t

class canonical_version_t(Struct):
	_format = Format.LittleEndian
	major = Type.UnsignedByte
	model = Type.UnsignedByte
	revision = Type.UnsignedByte


#	arena_part_number_t

class arena_part_number_t(Struct):
	_format = Format.LittleEndian
	bcd = Type.UnsignedByte[ARENA_PART_NUM_BCD_LEN]


#	scm_hash_t

class scm_hash_t(Struct):
	_format = Format.LittleEndian
	bytes = Type.UnsignedByte[SCM_HASH_HEX_LEN]


#	arena_info_t

class arena_info_t(Struct):
	_format = Format.LittleEndian
	part_number = Type.Struct(arena_part_number_t)
	revision = Type.UnsignedByte
	version = Type.Struct(canonical_version_t)


#	logical_bank_information_t

class logical_bank_information_t(Struct):
	_format = Format.LittleEndian
	SCM_hash = Type.Struct(scm_hash_t)
	has_mods = Type.UnsignedByte
	arena_info = Type.Struct(arena_info_t)


#	MDE_FW_information_t

class MDE_FW_information_t(Struct):
	_format = Format.LittleEndian
	fw_bank0_crc_is_bad_b = Type.UnsignedByte
	fw_bank1_crc_is_bad_b = Type.UnsignedByte
	fw_bank0_is_selected_b = Type.UnsignedByte
	fw_bank1_is_selected_b = Type.UnsignedByte
	active_bank = Type.Struct(logical_bank_information_t)
	inactive_bank = Type.Struct(logical_bank_information_t)


#	MDE_DMIR_record_t

class MDE_DMIR_record_t(Struct):
	_format = Format.LittleEndian
	bank = Type.UnsignedByte
	area_type = Type.UnsignedByte
	data_model_id = Type.UnsignedByte
	status = Type.UnsignedByte
	DMIR_SCM_hash = Type.Struct(scm_hash_t)
	arena_info = Type.Struct(arena_info_t)


#	MDE_DMIR_status_t

class MDE_DMIR_status_t(Struct):
	_format = Format.LittleEndian
	num_records = Type.UnsignedByte


#	MDE_payload_device_capabilities_t

class MDE_payload_device_capabilities_t(Struct):
	_format = Format.LittleEndian
	capability_1 = Type.UnsignedLong
	capability_2 = Type.UnsignedLong
	capability_3 = Type.UnsignedLong
	capability_4 = Type.UnsignedLong


#	dev_control_MDE_t

class dev_control_MDE_t(Struct):
	_format = Format.LittleEndian
	num_commands = Type.UnsignedByte
	cmd_data_bytes = Type.UnsignedByte


#	reset_cmd_args_t

class reset_cmd_args_t(Struct):
	_format = Format.LittleEndian
	reset_type = Type.UnsignedByte


#	force_bank_cmd_args_t

class force_bank_cmd_args_t(Struct):
	_format = Format.LittleEndian
	bank_to_choose = Type.UnsignedByte

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

APP_COMMON_SERVICES_INC_MCP_MDE_DEVICE_H_ = True
DISCONNECTION_TIMEOUT_NONE = (0)
DEVICE_TYPE_INVALID = 0
DEVICE_TYPE_PV = 2
DEVICE_TYPE_BATT = 3
DEV_CONTROL_CMD_FORCE_BANK = 0
DEV_CONTROL_CMD_RESET = 1
RESET_NONE = 0
RESET_TYPE_MCU_CORE = 1
RESET_TYPE_ASIC = 2
