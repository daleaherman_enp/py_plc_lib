# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_mde_image.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

MDE_IMAGE_CONTRACT_MAX_BYTES = 500
MDE_IMAGE_PAGE_HUNK_BYTES = (256)

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	legacy_metadata_t

class legacy_metadata_t(Struct):
	_format = Format.BigEndian
	crc16_meta = Type.UnsignedShort
	fill_0 = Type.UnsignedShort
	timeH = Type.UnsignedLong
	timeL = Type.UnsignedLong
	arena_bcd = Type.UnsignedByte[8]
	image_type = Type.UnsignedByte
	compatibility = Type.UnsignedByte
	fill_1 = Type.UnsignedShort
	md5_hash = Type.UnsignedByte[16]
	data_length = Type.UnsignedLong
	crc16_data = Type.UnsignedShort
	fill_2 = Type.UnsignedShort


#	fixed_header_for_image_start_t

class fixed_header_for_image_start_t(Struct):
	_format = Format.LittleEndian
	MDE_upgrade_start_type = Type.UnsignedByte
	EAC_contract_length_bytes = Type.UnsignedShort


#	mde_payload_EAC_request_image_start_t

class mde_payload_EAC_request_image_start_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(fixed_header_for_image_start_t)
	EAC_hunks_contract = Type.UnsignedByte[MDE_IMAGE_CONTRACT_MAX_BYTES]


#	fixed_model_start_header_t

class fixed_model_start_header_t(Struct):
	_format = Format.LittleEndian
	MDE_upgrade_start_type = Type.UnsignedByte
	MDE_FW_bank = Type.UnsignedByte
	MDE_model_area_type = Type.UnsignedByte
	MDE_data_model_ID = Type.UnsignedByte
	EAC_contract_length_bytes = Type.UnsignedShort


#	mde_payload_EAC_request_model_start_t

class mde_payload_EAC_request_model_start_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(fixed_model_start_header_t)
	EAC_hunks_contract = Type.UnsignedByte[MDE_IMAGE_CONTRACT_MAX_BYTES]


#	mde_payload_EAC_request_write_image_hunk_t

class mde_payload_EAC_request_write_image_hunk_t(Struct):
	_format = Format.LittleEndian
	hunk_sequence_number = Type.UnsignedShort
	hunk_data = Type.UnsignedByte[MDE_IMAGE_PAGE_HUNK_BYTES]


#	mde_payload_EAC_request_FW_image_meta_t

class mde_payload_EAC_request_FW_image_meta_t(Struct):
	_format = Format.LittleEndian
	firmware_bank = Type.UnsignedByte


#	mde_payload_EAC_request_model_meta_data_t

class mde_payload_EAC_request_model_meta_data_t(Struct):
	_format = Format.LittleEndian
	MDE_model_area_type = Type.UnsignedByte
	MDE_image_bank = Type.UnsignedByte
	MDE_data_model_ID = Type.UnsignedByte


#	mde_payload_PLD_response_read_ANY_metadata_t

class mde_payload_PLD_response_read_ANY_metadata_t(Struct):
	_format = Format.LittleEndian
	meta_data = Type.Struct(legacy_metadata_t)


#	mde_payload_EAC_request_write_metadata_t

class mde_payload_EAC_request_write_metadata_t(Struct):
	_format = Format.LittleEndian
	firmware_bank = Type.UnsignedByte
	meta_data = Type.Struct(legacy_metadata_t)


#	fixed_header_for_image_check_t

class fixed_header_for_image_check_t(Struct):
	_format = Format.LittleEndian
	image_check_status = Type.UnsignedByte
	contract_length = Type.UnsignedShort


#	mde_payload_PLD_response_to_image_check_t

class mde_payload_PLD_response_to_image_check_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(fixed_header_for_image_check_t)
	EAC_hunks_contract = Type.UnsignedByte[MDE_IMAGE_CONTRACT_MAX_BYTES]

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

APP_COMMON_SERVICES_INC_MCP_MDE_IMAGE_H_ = True
MDE_METADATA_SIZE_BYTES = ((legacy_metadata_t()._struct_size))
MDE_FLASH_UPGRADE_START_ERASE_AREA = 0
MDE_FLASH_UPGRADE_START_NO_ERASE = 1
MDE_FLASH_UPGRADE_START_NUM_OF = 2
DM_AREA_TYPE_INIT_RECORD = 0
DM_AREA_TYPE_CALIBRATION = 1
DM_AREA_TYPE_XML_SOURCE = 2
DM_AREA_TYPE_DEVICE_INFO = 3
DM_AREA_TYPE_NUM_OF = 4
DM_AREA_TYPE_INVALID = -1
MDE_BANK_SELECT_FIRMWARE_BANK_0 = 0
MDE_BANK_SELECT_FIRMWARE_BANK_1 = 1
MDE_BANK_FIRMWARE_NUMBER_OF_PHYSICAL_BANKS = 2
MDE_BANK_SELECT_FIRMWARE_BANK_ACTIVE_NOW = 20
MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE = 21
MDE_BANK_FIRMWARE_BANK_INVALID = -1
IMAGE_CHECK_STATUS_ONE_OR_MORE_HUNKS_ARE_MISSING = 0
IMAGE_CHECK_STATUS_GOT_ALL_HUNKS = 1
IMAGE_CHECK_STATUS_NO_PROGRAMMING_IN_SESSION = 2
