# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_mde_telemetry.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format
from sunspec_define_h import measurement_status_t,inverter_t

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	MDE_data_telemetry_level1_t

class MDE_data_telemetry_level1_t(Struct):
	_format = Format.LittleEndian
	telemetry_version = Type.UnsignedByte
	telemetry_level1 = Type.Struct(inverter_t)


#	MDE_data_telemetry_level2_t

class MDE_data_telemetry_level2_t(Struct):
	_format = Format.LittleEndian
	telemetry_version = Type.UnsignedByte
	telemetry_level2 = Type.Struct(measurement_status_t)

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

SUBMODULES_EAC_MCP_MDE_DEFINITIONS_MCP_MDE_TELEMETRY_H_ = True
