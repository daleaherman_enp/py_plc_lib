# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcp_v6.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format
from mde_ids_h import MDE_POLLING_BITMAP_LEN_BYTES
from enphase_mac_common_h import McpMsgHdr

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

PLC_MSG_LEN_MAX = 512

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	app_msg_header

class app_msg_header(Struct):
	_format = Format.LittleEndian
	embed_McpMsgHdr = Type.Struct(McpMsgHdr)
	msg_id = Type.UnsignedByte

MCP_V6_POLL_MESSAGE_BODY_SIZE = (PLC_MSG_LEN_MAX-((app_msg_header()._struct_size)+(MDE_POLLING_BITMAP_LEN_BYTES*(1))))
MCP_V6_POLL_RESPONSE_MESSAGE_BODY_SIZE = (PLC_MSG_LEN_MAX-((app_msg_header()._struct_size)+1))
MCP_V6_POLL_ACK_MESSAGE_BODY_SIZE = (PLC_MSG_LEN_MAX-((app_msg_header()._struct_size)))
MCP_V6_MULTICAST_MESSAGE_BODY_SIZE = (PLC_MSG_LEN_MAX-((app_msg_header()._struct_size)))

#	mcp_v6_poll_message_t

class mcp_v6_poll_message_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(app_msg_header)
	mde_bitmap = Type.UnsignedByte[MDE_POLLING_BITMAP_LEN_BYTES]
	body = Type.UnsignedByte[MCP_V6_POLL_MESSAGE_BODY_SIZE]


#	mcp_v6_poll_response_message_t

class mcp_v6_poll_response_message_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(app_msg_header)
	final = Type.UnsignedByte
	body = Type.UnsignedByte[MCP_V6_POLL_RESPONSE_MESSAGE_BODY_SIZE]


#	mcp_v6_poll_ack_message_t

class mcp_v6_poll_ack_message_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(app_msg_header)
	body = Type.UnsignedByte[MCP_V6_POLL_ACK_MESSAGE_BODY_SIZE]


#	mcp_v6_multicast_message_t

class mcp_v6_multicast_message_t(Struct):
	_format = Format.LittleEndian
	header = Type.Struct(app_msg_header)
	body = Type.UnsignedByte[MCP_V6_MULTICAST_MESSAGE_BODY_SIZE]

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

APP_COMMON_SERVICES_INC_MCP_V6_H_ = True
cMcpMsgIdNone = 0
cMcpMsgIdAckInterval = 1
cMcpMsgIdAckDevInfo = 2
cMcpMsgIdAckSimple = 3
cMcpMsgIdPoll = 4
cMcpMsgIdScan = 5
cMcpMsgIdDomainCtl = 6
cMcpMsgIdInterval = 7
cMcpMsgIdDevInfo = 8
cMcpMsgIdImgInventory = 9
cMcpMsgIdImgInfo = 10
cMcpMsgIdImgReq = 11
cMcpMsgIdImgRsp = 12
cMcpMsgIdDebug = 13
cMcpMsgIdTripPointInfo = 14
cMcpMsgIdAckTripPointInfo = 15
cMcpMsgIdPlcCfg = 16
cMcpMsgIdSecInfo = 17
cMcpMsgIdAckSecInfo = 18
cMcpMsgIdMsgErr = 19
cMcpMsgIdAgfAdcData = 20
cMcpMsgIdAgfAdcInfo = 21
cMcpMsgIdAgfAdcReq = 22
cMcpMsgIdIntervalIe = 23
cMcpMsgIdAckIntervalIe = 24
cMcpMsgIdEventIe = 25
cMcpMsgIdAckEventIe = 26
cMcpMsgIdMsgCapIe = 27
cMcpMsgIdFastpath = 28
cMcpMsgIdPaCapture = 29
cMcpMsgIdPaInfo = 30
cMcpMsgIdDevInfoIe = 31
cMcpMsgIdNonSecure = 32
cMcpMsgIdSysConfig = 33
cMcpMsgIdPollCmdTlv = 34
cMcpMsgIdPollRspTlv = 35
cMcpMsgIdAgfRecords = 36
cMcpMsgIdExpAgfRecordTlv = 37
cMcpMsgIdAssociation = 38
cMcpMsgIdMfg = 39
cMcpMsgIdPollContainer = 50
cMcpMsgIdPollResponseContainer = 51
cMcpMsgIdPollAckContainer = 52
cMcpMsgIdMulticastContainer = 53
cMcpMsgIdNumOf = 54
