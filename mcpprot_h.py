# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mcpprot.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format


# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	Struct_Anon_1_t

class Struct_Anon_1_t(Struct):
	_format = Format.LittleEndian
	msgSrcEmu = Type.UnsignedByte
	modAddr = Type.UnsignedShort
	domainAddr = Type.Struct(SerialNumber)


#	tL2TxFrameInfo

class tL2TxFrameInfo(Struct):
	_format = Format.LittleEndian
	protocolId = Type.UnsignedByte
	scopeInd = Type.UnsignedByte
	protoVer = Type.UnsignedByte
	reassPattern = Type.Struct(Struct_Anon_1_t)


#	tL2RxFrameInfo

class tL2RxFrameInfo(Struct):
	_format = Format.LittleEndian
	embed_tL2TxFrameInfo = Type.Struct(tL2TxFrameInfo)
	ackCtrl = Type.UnsignedByte
	segMsgDataLen = Type.UnsignedByte
	firstSeg = Type.Byte
	segsRemaining = Type.UnsignedByte


#	tAppMsg

class tAppMsg(Struct):
	_format = Format.LittleEndian
	hdr = Type.Struct(MsgHdr)
	anonymous = Type.UnsignedByte[cAppMsgLenWPadIV-sizeof(MsgHdr)]
	msgLen = Type.UnsignedShort
	msg_hdr_type = Type.UnsignedByte


#	tTxAppMsg

class tTxAppMsg(Struct):
	_format = Format.LittleEndian
	embed_tAppMsg = Type.Struct(tAppMsg)
	l2 = Type.Struct(tL2TxFrameInfo)
	resp_tmout_type = Type.UnsignedByte
	contention_priority_window = Type.UnsignedByte
	acknowledged = Type.Byte


#	tRxAppMsg

class tRxAppMsg(Struct):
	_format = Format.LittleEndian
	embed_tAppMsg = Type.Struct(tAppMsg)
	l2 = Type.Struct(tL2RxFrameInfo)

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

_mcpprot_h_ = True
MATCH_DOMAIN_EQUALS_SERIAL_DEVICE_ID = (0x3FFF)
cAppMsgLenWPadIV = 512
cAppMsgLenWoMcpHeader = (cAppMsgLenWPadIV-(McpMsgHdr()._struct_size))
cAppMsgLen = 496
AckOkRsp = 0
AckFailRsp = 1
UnAckReq = 2
AckReq = 3
EnphaseApp = 0
EnphaseMacCtrl = 1
EnphaseDomainCtrl = 2
EnphaseLegacy = 7
cL2AddrInfoMsgSrcEmu = 1
cL2AddrInfoMsgSrcMod = 0
Directed = 0
McastMod = 1
BcastCoord = 2
BcastAll = 3
no_response = 0
normal = 1
extended = 2
immediate_high = 0
discovery = 1
coordinator_discovery = 2
init_access = 3
regular_continuation = 4
