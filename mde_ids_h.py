# ---------------------------------------------------------
#
# This file is auto-generated from C header file: mde_ids.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

MDE_MAX_IDS = (256)

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	MDE_header_t

class MDE_header_t(Struct):
	_format = Format.LittleEndian
	mde_id = Type.UnsignedByte
	mde_len = Type.UnsignedShort

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

APP_COMMON_SERVICES_INC_MDE_IDS_H_ = True
MDE_POLLING_BITMAP_LEN_BYTES = int(MDE_MAX_IDS/8)
mde_id_none = 0
mde_id_read_manufacturing_info = 1
mde_id_read_device_capabilities = 2
mde_id_read_device_nameplate = 3
mde_id_read_battery_nameplate = 4
mde_id_read_device_info = 5
mde_id_read_plc_stats = 6
mde_id_device_control = 7
mde_id_shutoff_enable = 8
mde_id_shutoff_control = 9
mde_id_theft_detection_enable = 10
mde_id_read_flash_log = 11
mde_id_get_flash_log_size = 12
mde_id_read_telemetry_level1 = 14
mde_id_read_telemetry_level2 = 15
mde_id_read_telemetry_level3 = 16
mde_id_conditions = 17
mde_id_set_secondary_control = 18
mde_id_set_tertiary_control = 19
mde_id_FW_read_metadata = 20
mde_id_FW_upgrade_start = 21
mde_id_FW_upgrade_write_page = 22
mde_id_FW_upgrade_finish_and_check = 23
mde_id_FW_write_metadata = 24
mde_id_DMIR_read_metadata = 30
mde_id_DMIR_upgrade_start = 31
mde_id_DMIR_write_page = 32
mde_id_DMIR_finish_and_check = 33
mde_id_DM_runtime_object_write = 35
mde_id_DM_AGF_cache_write = 36
mde_id_Flash_read = 40
mde_id_Flash_write = 41
mde_id_ECDC_payload = 42
mde_id_RAM_read = 43
mde_id_RAM_write = 44
mde_id_immediate_controls = 45
mde_id_get_firmware_information = 46
mde_id_get_all_DMIR_information = 47
mde_id_set_real_time_clock = 48
mde_id_write_ACB_battery0_data = 50
mde_id_fixed_numof = 51
mde_id_dynamic_start = 52
