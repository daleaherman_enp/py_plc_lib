# ---------------------------------------------------------
#
# This file is auto-generated from C header file: plc_poll.c 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format
from sunspec_define_h import inverter_t

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	tMCPV6Hdr

class tMCPV6Hdr(Struct):
	_format = Format.LittleEndian
	offset = Type.UnsignedByte[4]
	msg_id = Type.UnsignedByte
	final_flag = Type.UnsignedByte


#	tTlvHdr

class tTlvHdr(Struct):
	_format = Format.LittleEndian
	type = Type.UnsignedByte
	len = Type.UnsignedShort


#	tMDETelemtryL1Data

class tMDETelemtryL1Data(Struct):
	_format = Format.LittleEndian
	telemetry_version = Type.UnsignedByte
	inv = Type.Struct(inverter_t)


#	tTLVL1TelemetryPkt

class tTLVL1TelemetryPkt(Struct):
	_format = Format.LittleEndian
	mcpHdr = Type.Struct(tMCPV6Hdr)
	tlvHdr = Type.Struct(tTlvHdr)
	tel = Type.Struct(tMDETelemtryL1Data)

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

MAX_STR_STORAGE = 64
LINE_MEAS_CLOCK_FREQ = 50000000
