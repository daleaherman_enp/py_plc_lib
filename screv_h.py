# ---------------------------------------------------------
#
# This file is auto-generated from C header file: screv.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	sSC_revision_v2

class sSC_revision_v2(Struct):
	_format = Format.LittleEndian
	build = Type.UnsignedShort
	major = Type.UnsignedShort
	fwrevision = Type.UnsignedLong
	hwrevision = Type.UnsignedLong
	long_serial = Type.UnsignedByte[24]
	short_serial = Type.UnsignedLong


#	sSCUpgradeData_t

class sSCUpgradeData_t(Struct):
	_format = Format.LittleEndian
	address = Type.UnsignedLong
	no_of_valid_bytes = Type.UnsignedLong
	data_bytes = Type.UnsignedByte[128]


#	sSCCmdUpgrade_t

class sSCCmdUpgrade_t(Struct):
	_format = Format.LittleEndian
	dev = Type.UnsignedByte
	data = Type.Struct(sSCUpgradeData_t)

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

