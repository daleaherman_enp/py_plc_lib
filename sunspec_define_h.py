# ---------------------------------------------------------
#
# This file is auto-generated from C header file: sunspec_define.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------

MAX_CURVE_LENGTH = (10)
NUM_CURVES = (4)
NUM_BATTERIES = (4)

# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	time_freq_pair_t

class time_freq_pair_t(Struct):
	_format = Format.LittleEndian
	Tms = Type.UnsignedShort
	Hz = Type.Long


#	time_volt_pair_t

class time_volt_pair_t(Struct):
	_format = Format.LittleEndian
	Tms = Type.UnsignedShort
	V = Type.UnsignedShort


#	volt_VAr_pair_t

class volt_VAr_pair_t(Struct):
	_format = Format.LittleEndian
	V = Type.Short
	VAr = Type.Short


#	freq_watt_pair_t

class freq_watt_pair_t(Struct):
	_format = Format.LittleEndian
	Hz = Type.UnsignedShort
	W = Type.Short


#	watt_VAr_pair_t

class watt_VAr_pair_t(Struct):
	_format = Format.LittleEndian
	W = Type.Short
	VAr = Type.Short


#	time_freq_curve_t

class time_freq_curve_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(time_freq_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	CrvNam = Type.Char[16]


#	time_volt_curve_t

class time_volt_curve_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(time_volt_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	CrvNam = Type.Char[16]


#	volt_VAr_curve_t

class volt_VAr_curve_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(volt_VAr_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	CrvNam = Type.Char[16]


#	freq_watt_curve_t

class freq_watt_curve_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(volt_VAr_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	CrvNam = Type.Char[16]


#	watt_VAr_curve_t

class watt_VAr_curve_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(watt_VAr_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	CrvNam = Type.Char[16]


#	capability_t

class capability_t(Struct):
	_format = Format.LittleEndian
	IAC_Fast = Type.UnsignedShort
	IAC_Slow = Type.UnsignedShort


#	primary_t

class primary_t(Struct):
	_format = Format.LittleEndian
	XZ = Type.Short
	RZ = Type.Short
	Zg = Type.UnsignedShort
	Kr_inverse = Type.Short
	Ka_U1d_ref_inverse = Type.Short


#	secondary_t

class secondary_t(Struct):
	_format = Format.LittleEndian
	omega_bias = Type.Short
	u1d_bias = Type.Short
	omega_prime_bias = Type.Short
	u1d_prime_bias = Type.Short
	Ia_bias = Type.Short
	Ir_bias = Type.Short


#	tertiary_t

class tertiary_t(Struct):
	_format = Format.LittleEndian
	omega_prime_bias = Type.Short
	u1d_prime_bias = Type.Short


#	enphase_control_t

class enphase_control_t(Struct):
	_format = Format.LittleEndian
	ACAvg_Limit = Type.UnsignedShort
	ACAvg_Ena = Type.UnsignedByte
	RoCoF_RampRate = Type.UnsignedShort
	RoCoF_Delay = Type.UnsignedShort
	RoCoF_Ena = Type.UnsignedByte
	VShift_Angle = Type.UnsignedShort
	VShift_Delay = Type.UnsignedShort
	VShift_AngleIslanding = Type.UnsignedShort
	VShift_Ena = Type.UnsignedByte
	Islanding_ToneAmplitude = Type.UnsignedShort
	Islanding_ToneMaxAmplitude = Type.UnsignedShort
	Islanding_ToneMaxTime = Type.UnsignedShort
	Islanding_Delay = Type.UnsignedShort
	Islanding_HarmonicsDiff = Type.UnsignedShort
	Islanding_HarmonicsIncrease = Type.UnsignedShort
	Islanding_HarmonicsMax = Type.UnsignedShort
	Islanding_FundamentalDiff = Type.UnsignedShort
	Islanding_FreqDeviation = Type.UnsignedLong
	Islanding_Ena = Type.UnsignedByte
	IAC_Ena = Type.UnsignedByte
	primary = Type.Struct(primary_t)
	secondary = Type.Struct(secondary_t)
	tertiary = Type.Struct(tertiary_t)
	TheftPrev_Ena = Type.UnsignedByte
	FirmwareMode = Type.UnsignedByte


#	watt_VAr_t

class watt_VAr_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	DeptRef = Type.UnsignedByte
	Crvs = Type.Struct(watt_VAr_curve_t)[NUM_CURVES]
	RmpPt1Tms = Type.UnsignedShort
	RmpDecTmm = Type.UnsignedShort
	RmpIncTmm = Type.UnsignedShort


#	power_factors_t

class power_factors_t(Struct):
	_format = Format.LittleEndian
	true_pf = Type.Short
	displacement = Type.Short
	distortion = Type.Short


#	events_t

class events_t(Struct):
	_format = Format.LittleEndian
	GND_FAULT = Type.UnsignedByte
	DC_OV = Type.UnsignedByte
	AC_DISCONNECT_OPEN = Type.UnsignedByte
	DC_DISCONNECT_OPEN = Type.UnsignedByte
	GRID_DISCONNECT = Type.UnsignedByte
	CABINET_OPEN = Type.UnsignedByte
	MANUAL_SHUTDOWN = Type.UnsignedByte
	OVER_TEMP = Type.UnsignedByte
	OVER_FREQ = Type.UnsignedByte
	UNDER_FREQ = Type.UnsignedByte
	AC_OVER_VOLT = Type.UnsignedByte
	AC_UNDER_VOLT = Type.UnsignedByte
	BLOWN_FUSE = Type.UnsignedByte
	UNDER_TEMP = Type.UnsignedByte
	MEMORY_ERR = Type.UnsignedByte
	HARDWARE_FAILURE = Type.UnsignedByte


#	enphase_events_t

class enphase_events_t(Struct):
	_format = Format.LittleEndian
	SPO = Type.UnsignedByte
	SSO = Type.UnsignedByte
	STO = Type.UnsignedByte
	UO = Type.UnsignedByte
	DCA = Type.UnsignedByte
	HSD = Type.UnsignedByte
	ETC = Type.UnsignedByte
	VOV = Type.UnsignedByte
	MCO = Type.UnsignedByte
	FLT9 = Type.UnsignedByte
	FLT10 = Type.UnsignedByte
	PSE1 = Type.UnsignedByte
	PSE2 = Type.UnsignedByte
	ADC_SAT = Type.UnsignedByte
	VIN_OV = Type.UnsignedByte
	VIN_UV = Type.UnsignedByte


#	inverter_t

class inverter_t(Struct):
	_format = Format.LittleEndian
	A = Type.UnsignedShort
	V = Type.UnsignedShort
	W = Type.Short
	Hz = Type.UnsignedLong
	VA = Type.Short
	VAr = Type.Short
	PF = Type.Struct(power_factors_t)
	DCA = Type.Short
	DCV = Type.UnsignedShort
	DCW = Type.Short
	Temperature = Type.Short
	Operating_State = Type.UnsignedByte
	Enphase_Operating_State = Type.UnsignedByte
	Events = Type.Struct(events_t)
	Enphase_Events = Type.Struct(enphase_events_t)


#	VArRtgs_t

class VArRtgs_t(Struct):
	_format = Format.LittleEndian
	Q1 = Type.Short
	Q2 = Type.Short
	Q3 = Type.Short
	Q4 = Type.Short


#	PFRtgs_t

class PFRtgs_t(Struct):
	_format = Format.LittleEndian
	Q1 = Type.Short
	Q2 = Type.Short
	Q3 = Type.Short
	Q4 = Type.Short


#	nameplate_t

class nameplate_t(Struct):
	_format = Format.LittleEndian
	DER_Type = Type.UnsignedByte
	WRtg = Type.UnsignedShort
	VARtg = Type.UnsignedShort
	VArRtgs = Type.Struct(VArRtgs_t)
	ARtg = Type.UnsignedShort
	PFRtgs = Type.Struct(PFRtgs_t)


#	VArMaxs_t

class VArMaxs_t(Struct):
	_format = Format.LittleEndian
	Q1 = Type.Short
	Q2 = Type.Short
	Q3 = Type.Short
	Q4 = Type.Short


#	PFMins_t

class PFMins_t(Struct):
	_format = Format.LittleEndian
	Q1 = Type.UnsignedShort
	Q2 = Type.UnsignedShort
	Q3 = Type.UnsignedShort
	Q4 = Type.UnsignedShort


#	basic_settings_t

class basic_settings_t(Struct):
	_format = Format.LittleEndian
	WMax = Type.UnsignedShort
	VRef = Type.UnsignedShort
	VRefOfs = Type.UnsignedShort
	HzRef = Type.UnsignedLong
	VMax = Type.UnsignedShort
	VMin = Type.UnsignedShort
	VAMax = Type.UnsignedShort
	VArMax = Type.Struct(VArMaxs_t)
	WGra = Type.UnsignedShort
	PFMin = Type.Struct(PFMins_t)
	VArAct = Type.UnsignedByte
	ClcTotVA = Type.UnsignedByte
	MaxRmpRte = Type.UnsignedShort
	ECPNomHz = Type.UnsignedLong
	ConnPh = Type.UnsignedByte


#	time_VA_pair_t

class time_VA_pair_t(Struct):
	_format = Format.LittleEndian
	Tms = Type.UnsignedShort
	VA = Type.UnsignedShort


#	time_A_pair_t

class time_A_pair_t(Struct):
	_format = Format.LittleEndian
	Tms = Type.UnsignedShort
	A = Type.UnsignedShort


#	powers_t

class powers_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(time_VA_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	absolute_max = Type.UnsignedShort


#	currents_t

class currents_t(Struct):
	_format = Format.LittleEndian
	points = Type.Struct(time_A_pair_t)[MAX_CURVE_LENGTH]
	ActPt = Type.UnsignedByte
	absolute_max = Type.UnsignedShort


#	overload_t

class overload_t(Struct):
	_format = Format.LittleEndian
	ModEna = Type.UnsignedByte
	preference = Type.UnsignedByte
	NPt = Type.UnsignedByte
	powers = Type.Struct(powers_t)
	currents = Type.Struct(currents_t)


#	ActWhs_t

class ActWhs_t(Struct):
	_format = Format.LittleEndian
	Q14 = Type.UnsignedLong
	Q23 = Type.UnsignedLong


#	ActVArhs_t

class ActVArhs_t(Struct):
	_format = Format.LittleEndian
	Q12 = Type.UnsignedLong
	Q34 = Type.UnsignedLong


#	ActVAhs_t

class ActVAhs_t(Struct):
	_format = Format.LittleEndian
	Q1 = Type.UnsignedLong
	Q2 = Type.UnsignedLong
	Q3 = Type.UnsignedLong
	Q4 = Type.UnsignedLong


#	measurement_status_t

class measurement_status_t(Struct):
	_format = Format.LittleEndian
	ActWhs = Type.Struct(ActWhs_t)
	ActVArhs = Type.Struct(ActVArhs_t)
	ActVAhs = Type.Struct(ActVAhs_t)
	VArAval = Type.Short
	WAval = Type.Short
	StSetLimMsk = Type.UnsignedShort
	StActCtl = Type.UnsignedShort
	Tms = Type.UnsignedLong
	RtSt = Type.UnsignedShort
	Ris = Type.UnsignedShort


#	immediate_controls_t

class immediate_controls_t(Struct):
	_format = Format.LittleEndian
	Conn_WinTms = Type.UnsignedShort
	Conn_RvrtTms = Type.UnsignedShort
	Conn = Type.UnsignedByte
	WMaxLimPct = Type.Short
	WMaxLimPct_WinTms = Type.UnsignedShort
	WMaxLimPct_RvrtTms = Type.UnsignedShort
	WMaxLimPct_RmpTms = Type.UnsignedShort
	WMaxLim_Ena = Type.UnsignedByte
	OutPFSet = Type.Short
	OutPFSet_WinTms = Type.UnsignedShort
	OutPFSet_RvrtTms = Type.UnsignedShort
	OutPFSet_RmpTms = Type.UnsignedShort
	OutPFSet_Ena = Type.UnsignedByte
	VArWMaxPct = Type.Short
	VArMaxPct = Type.Short
	VArAvalPct = Type.Short
	VArPct_WinTms = Type.UnsignedShort
	VArPct_RvrtTms = Type.UnsignedShort
	VArPct_RmpTms = Type.UnsignedShort
	VArPct_Mod = Type.UnsignedByte
	VArPct_Ena = Type.UnsignedByte


#	static_volt_var_t

class static_volt_var_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	ActPt = Type.UnsignedByte
	DeptRef = Type.UnsignedByte
	Crvs = Type.Struct(volt_VAr_curve_t)[NUM_CURVES]
	RmpPt1Tms = Type.UnsignedShort
	RmpDecTmm = Type.UnsignedShort
	RmpIncTmm = Type.UnsignedShort


#	freq_watt_param_t

class freq_watt_param_t(Struct):
	_format = Format.LittleEndian
	WGra = Type.Short
	HzStr = Type.Long
	HzStop = Type.Long
	HysEna = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	HzStopWGra = Type.UnsignedShort


#	dynamic_reactive_current_t

class dynamic_reactive_current_t(Struct):
	_format = Format.LittleEndian
	ArGraMod = Type.UnsignedByte
	ArGraSag = Type.UnsignedShort
	ArGraSwell = Type.UnsignedShort
	ModEna = Type.UnsignedByte
	FilTms = Type.UnsignedShort
	DbVMin = Type.UnsignedShort
	DbVMax = Type.UnsignedShort
	BlkZnV = Type.UnsignedShort
	HysBlkZnV = Type.UnsignedShort
	BlkZnTmms = Type.UnsignedShort
	HoldTmms = Type.UnsignedShort


#	lvrt_t

class lvrt_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	Crvs = Type.Struct(time_volt_curve_t)[NUM_CURVES]


#	hvrt_t

class hvrt_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	Crvs = Type.Struct(time_volt_curve_t)[NUM_CURVES]


#	lfrt_t

class lfrt_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	ActPt = Type.UnsignedByte
	Crvs = Type.Struct(time_freq_curve_t)[NUM_CURVES]


#	hfrt_t

class hfrt_t(Struct):
	_format = Format.LittleEndian
	ActCrv = Type.UnsignedByte
	ModEna = Type.UnsignedByte
	WinTms = Type.UnsignedShort
	RvrtTms = Type.UnsignedShort
	RmpTms = Type.UnsignedShort
	NCrv = Type.UnsignedByte
	NPt = Type.UnsignedByte
	ActPt = Type.UnsignedByte
	Crvs = Type.Struct(time_freq_curve_t)[NUM_CURVES]


#	pv_t

class pv_t(Struct):
	_format = Format.LittleEndian
	VMax = Type.UnsignedShort
	VMin = Type.UnsignedShort
	AMax = Type.UnsignedShort
	AMin = Type.UnsignedShort


#	battery_data_t

class battery_data_t(Struct):
	_format = Format.LittleEndian
	Name = Type.Char[16]
	AHRtg = Type.UnsignedShort
	WHRtg = Type.UnsignedShort
	WChaRteMax = Type.UnsignedShort
	WDisChaRteMax = Type.UnsignedShort
	DisChaRte = Type.UnsignedShort
	SoCMax = Type.UnsignedShort
	SoCMin = Type.UnsignedShort
	Typ = Type.UnsignedShort
	VMax = Type.UnsignedShort
	VMin = Type.UnsignedShort
	VMinFloat = Type.UnsignedShort
	AChaMax = Type.Short
	AChaMin = Type.Short
	ADisChaMax = Type.Short
	ADisChaMin = Type.Short


#	battery_t

class battery_t(Struct):
	_format = Format.LittleEndian
	ActBatt = Type.UnsignedByte
	SocRsvMax = Type.UnsignedShort
	SoCRsvMin = Type.UnsignedShort
	SoC = Type.UnsignedShort
	DoD = Type.UnsignedShort
	SoH = Type.UnsignedShort
	NCyc = Type.UnsignedShort
	ChaSt = Type.UnsignedShort
	LocRemCtl = Type.UnsignedShort
	AlmRst = Type.UnsignedByte
	State = Type.UnsignedShort
	WarrDt = Type.UnsignedShort
	Evt1 = Type.UnsignedLong
	Evt2 = Type.UnsignedLong
	V = Type.UnsignedShort
	A = Type.Short
	W = Type.UnsignedShort
	ReqInvState = Type.UnsignedShort
	ReqW = Type.UnsignedShort
	SetOp = Type.UnsignedShort
	SetInvState = Type.UnsignedShort
	batteries = Type.Struct(battery_data_t)[NUM_BATTERIES]


#	mppt_t

class mppt_t(Struct):
	_format = Format.LittleEndian
	Udc_startup = Type.UnsignedShort
	Udc_shutdown = Type.UnsignedShort
	current_loop_Kp = Type.Short
	current_loop_Ki = Type.Short
	current_loop_Kff = Type.Short
	current_loop_max_integral = Type.Short
	voltage_loop_gain = Type.Short
	Udc_error_window = Type.UnsignedShort
	Udc_min_curr_rampdown = Type.Short
	epsilon = Type.UnsignedShort
	input_capacitance = Type.UnsignedShort
	wdc_burst_th_lo = Type.Short
	wdc_burst_th_hi = Type.Short
	Icap_max = Type.Short
	burst_lockout_time = Type.UnsignedLong
	max_Uerr_for_burst = Type.Short
	Ia_exit_standby = Type.Short


#	charge_control_t

class charge_control_t(Struct):
	_format = Format.LittleEndian
	current_loop_Kp = Type.Short
	current_loop_Ki = Type.Short
	current_loop_Kff = Type.Short
	current_loop_max_integral = Type.Short


#	pll_params_t

class pll_params_t(Struct):
	_format = Format.LittleEndian
	omega_center = Type.Long
	limit_center = Type.UnsignedShort
	k_theta = Type.Long


#	discovery_params_t

class discovery_params_t(Struct):
	_format = Format.LittleEndian
	U1d_ref_safe = Type.Short
	i1d_discovery_th_hi = Type.Short
	i1d_discovery_th_lo = Type.Short
	startup_seq_t0 = Type.Short
	startup_seq_t1 = Type.Short


#	cyclo_pump_params_t

class cyclo_pump_params_t(Struct):
	_format = Format.LittleEndian
	min_pump_time = Type.Short
	priming_angle = Type.Short


#	grid_params_t

class grid_params_t(Struct):
	_format = Format.LittleEndian
	Uac_max_for_start = Type.Short
	U1d_stable_threshold = Type.Short
	omega_stable_threshold = Type.Short
	grid_gone_threshold = Type.Short
	grid_stable_time = Type.Short
	U1d_start_threshold = Type.Short
	U1d_ref_envelope = Type.Short


#	upper_limits_t

class upper_limits_t(Struct):
	_format = Format.LittleEndian
	Ia_input_current_limted = Type.Short
	Ia_overload = Type.Short
	Ir_overload = Type.Short
	Ia_ramprate_limited = Type.Short
	Ia = Type.Short
	Ir = Type.Short


#	lower_limits_t

class lower_limits_t(Struct):
	_format = Format.LittleEndian
	Ia_input_current_limted = Type.Short
	Ia_overload = Type.Short
	Ir_overload = Type.Short
	Ia = Type.Short
	Ir = Type.Short


#	limits_t

class limits_t(Struct):
	_format = Format.LittleEndian
	Idc_max = Type.Short
	Ia_rampup_max = Type.Short
	Ir_rampup_max = Type.Short
	Ia_max_ramp_rate = Type.Short
	Upper = Type.Struct(upper_limits_t)
	Lower = Type.Struct(lower_limits_t)


#	telemetry_t

class telemetry_t(Struct):
	_format = Format.LittleEndian
	V_gate_driver = Type.UnsignedShort
	pf_convention = Type.UnsignedByte


#	enphase_inverter_t

class enphase_inverter_t(Struct):
	_format = Format.LittleEndian
	MPPT = Type.Struct(mppt_t)
	ChargeControl = Type.Struct(charge_control_t)
	PLL = Type.Struct(pll_params_t)
	Discovery = Type.Struct(discovery_params_t)
	Cycloconverter = Type.Struct(cyclo_pump_params_t)
	Grid = Type.Struct(grid_params_t)
	Limits = Type.Struct(limits_t)
	Telemetry = Type.Struct(telemetry_t)
	startup_time = Type.Short
	min_shutdown_time = Type.Short
	sin_of_delta = Type.UnsignedLong
	cos_of_delta = Type.UnsignedLong
	rampup_time = Type.Short
	grid_fault_lockout_time = Type.Short
	zero_cross_angle = Type.Short
	grid_good_reqrd_time = Type.Long

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

__SUNSPEC_DEFINE_H = True
LIMIT_FROM_WMax = ((1<<0))
LIMIT_FROM_VAMax = ((1<<1))
LIMIT_FROM_VArAval = ((1<<2))
LIMIT_FROM_VArMaxQ1 = ((1<<3))
LIMIT_FROM_VArMaxQ2 = ((1<<4))
LIMIT_FROM_VArMaxQ3 = ((1<<5))
LIMIT_FROM_VArMaxQ4 = ((1<<6))
LIMIT_FROM_PFMinQ1 = ((1<<7))
LIMIT_FROM_PFMinQ2 = ((1<<8))
LIMIT_FROM_PFMinQ3 = ((1<<9))
LIMIT_FROM_PFMinQ4 = ((1<<10))
USING_FixedW = ((1<<0))
USING_FixedVAR = ((1<<1))
USING_FixedPF = ((1<<2))
USING_Volt_VAr = ((1<<3))
USING_Freq_Watt_Param = ((1<<4))
USING_Freq_Watt_Curve = ((1<<5))
USING_Dyn_Reactive_Current = ((1<<6))
USING_LVRT = ((1<<7))
USING_HVRT = ((1<<8))
USING_Watt_PF = ((1<<9))
USING_Volt_Watt = ((1<<10))
USING_Scheduled = ((1<<11))
USING_LFRT = ((1<<12))
USING_HFRT = ((1<<13))
RT_LVRT_ACTIVE = ((1<<0))
RT_HVRT_ACTIVE = ((1<<1))
RT_LFRT_ACTIVE = ((1<<2))
RT_HFRT_ACTIVE = ((1<<3))
BATTERY_EXPERIENCING_COMMUNICATION_ERROR = ((1<<0))
BATTERY_EXPERIENCING_OVER_TEMP_ALARM = ((1<<1))
BATTERY_EXPERIENCING_OVER_TEMP_WARNING = ((1<<2))
BATTERY_EXPERIENCING_UNDER_TEMP_ALARM = ((1<<3))
BATTERY_EXPERIENCING_UNDER_TEMP_WARNING = ((1<<4))
BATTERY_EXPERIENCING_OVER_CHARGE_CURRENT_ALARM = ((1<<5))
BATTERY_EXPERIENCING_OVER_CHARGE_CURRENT_WARNING = ((1<<6))
BATTERY_EXPERIENCING_OVER_DISCHARGE_CURRENT_ALARM = ((1<<7))
BATTERY_EXPERIENCING_OVER_DISCHARGE_CURRENT_WARNING = ((1<<8))
BATTERY_EXPERIENCING_OVER_VOLT_ALARM = ((1<<9))
BATTERY_EXPERIENCING_OVER_VOLT_WARNING = ((1<<10))
BATTERY_EXPERIENCING_UNDER_VOLT_ALARM = ((1<<11))
BATTERY_EXPERIENCING_UNDER_VOLT_WARNING = ((1<<12))
BATTERY_EXPERIENCING_UNDER_SOC_MIN_ALARM = ((1<<13))
BATTERY_EXPERIENCING_UNDER_SOC_MIN_WARNING = ((1<<14))
BATTERY_EXPERIENCING_OVER_SOC_MAX_ALARM = ((1<<15))
BATTERY_EXPERIENCING_OVER_SOC_MAX_WARNING = ((1<<16))
BATTERY_EXPERIENCING_VOLTAGE_IMBALANCE_WARNING = ((1<<17))
BATTERY_EXPERIENCING_TEMPERATURE_IMBALANCE_ALARM = ((1<<18))
BATTERY_EXPERIENCING_TEMPERATURE_IMBALANCE_WARNING = ((1<<19))
BATTERY_EXPERIENCING_CONTACTOR_ERROR = ((1<<20))
BATTERY_EXPERIENCING_FAN_ERROR = ((1<<21))
BATTERY_EXPERIENCING_GROUND_FAULT = ((1<<22))
BATTERY_EXPERIENCING_OPEN_DOOR_ERROR = ((1<<23))
BATTERY_EXPERIENCING_CURRENT_IMBALANCE_WARNING = ((1<<24))
BATTERY_EXPERIENCING_OTHER_ALARM = ((1<<25))
BATTERY_EXPERIENCING_OTHER_WARNING = ((1<<26))
BATTERY_EXPERIENCING_RESERVED_1 = ((1<<27))
BATTERY_EXPERIENCING_CONFIGURATION_ALARM = ((1<<28))
BATTERY_EXPERIENCING_CONFIGURATION_WARNING = ((1<<29))
ACAvg_Enabled = 0
ACAvg_Disabled = 1
ACAvg_NUM_OF = 2
RoCoF_Enabled = 0
RoCoF_Disabled = 1
RoCoF_NUM_OF = 2
VShift_Enabled = 0
VShift_Reconnect = 1
VShift_2s_Delay = 2
VShift_NUM_OF = 3
Islanding_Enabled = 0
Islanding_Japan_Mode = 1
Islanding_NUM_OF = 2
IAC_Enabled_SLOW = 0
IAC_Enabled_FAST = 1
IAC_NUM_OF = 2
TheftPrev_Enabled = 0
TheftPrev_Disabled = 1
TheftPrev_NUM_OF = 2
OVERLOAD_IS_ENABLED = 0
OVERLOAD_IS_DISABLED = 1
OVERLOAD_IS_NUM_OF = 2
WATT_VAR_Enabled = 0
WATT_VAR_Disabled = 1
WATT_VAR_NUM_OF = 2
VOLT_VAR_Enabled = 0
VOLT_VAR_Disabled = 1
VOLT_VAR_NUM_OF = 2
DEP_PERCENT_OF_WMax = 1
DEP_PERCENT_OF_VArMax = 2
DEP_PERCENT_OF_VArAval = 3
DEP_NUM_OF = 3
PF_SIGN_CONVENTION_IS_UNSIGNED = 0
PF_SIGN_CONVENTION_IS_EEI = 1
PF_SIGN_CONVENTION_IS_IEC = 2
PF_SIGN_CONVENTION_IS_NUM_OF = 3
PREFER_REAL_POWER = 0
PREFER_REACTIVE_POWER = 1
PREFER_POWER_FACTOR = 2
PREFER_NUM_OF = 3
OP_ST_OFF = 1
OP_ST_Sleeping = 2
OP_ST_Starting = 3
OP_ST_Mppt = 4
OP_ST_Throttled = 5
OP_ST_Shutting_down = 6
OP_ST_Fault = 7
OP_ST_Standby = 8
OP_ST_NUM_OF = 8
EN_OP_ST_Idle = 0
EN_OP_ST_Starting = 1
EN_OP_ST_Discovery = 2
EN_OP_ST_Throttled = 3
EN_OP_ST_Mppt = 4
EN_OP_ST_Bursting = 5
EN_OP_ST_Constant_Vin = 6
EN_OP_ST_Standby = 7
EN_OP_ST_Protection_Fault = 8
EN_OP_ST_Grid_Fault = 9
EN_OP_ST_Unknown_state = 99
EN_OP_ST_DMIR_Init_Failed = 100
EN_OP_ST_Host_Shutoff = 101
EN_OP_ST_NUM_OF = 13
DER_IS_PV = 4
DER_IS_PV_plus_Storage = 82
DER_IS_Storage = 90
DER_IS_NUM_OF = 3
VArAct_SWITCH = 1
VArAct_MAINTAIN = 2
VArAct_NUM_OF = 2
ClcTotVA_VECTOR = 1
ClcTotVA_ARITHMETIC = 2
ClcTotVA_NUM_OF = 2
PHASE_A = 1
PHASE_B = 2
PHASE_C = 3
PHASE_NUM_OF = 3
PHASE_CONN_DISCONNECT = 1
PHASE_CONN_CONNECT = 2
PHASE_CONN_NUM_OF = 2
WMaxLim_IS_DISABLED = 0
WMaxLim_IS_ENABLED = 1
WMaxLim_IS_NUM_OF = 2
FIXED_PF_IS_DISABLED = 0
FIXED_PF_IS_ENABLED = 1
FIXED_PF_IS_NUM_OF = 2
VAr_LIM_MODE_NONE = 0
VAr_LIM_MODE_WMax = 1
VAr_LIM_MODE_VArMax = 2
VAr_LIM_MODE_VarAval = 3
VAr_LIM_MODE_NUM_OF = 4
VAr_LIM_IS_DISABLED = 0
VAr_LIM_IS_ENABLED = 1
VAr_LIM_IS_NUM_OF = 2
HYSTERSIS_IS_DISABLED = 1
HYSTERSIS_IS_ENABLED = 0
HYSTERSIS_IS_NUM_OF = 2
FREQ_WATT_PARAM_IS_DISABLED = 1
FREQ_WATT_PARAM_IS_ENABLED = 0
FREQ_WATT_PARAM_IS_NUM_OF = 2
TRENDS_TOWARDS_EDGE = 0
TRENDS_TOWARDS_CENTER = 1
TRENDS_TOWARDS_NUM_OF = 2
DYNAMIC_REACTIVE_CURRENT_ENABLED = 0
DYNAMIC_REACTIVE_CURRENT_DISABLED = 1
DYNAMIC_REACTIVE_CURRENT_NUM_OF = 2
RT_ENABLED = 0
RT_DISABLED = 1
RT_NUM_OF = 2
USING_CURVE1 = 0
USING_CURVE2 = 1
USING_CURVE3 = 2
USING_CURVE4 = 3
USING_NUM_OF = 4
BATTERY_IS_OFF = 1
BATTERY_IS_EMPTY = 2
BATTERY_IS_DISCHARGING = 3
BATTERY_IS_CHARGING = 4
BATTERY_IS_FULL = 5
BATTERY_IS_HOLDING = 6
BATTERY_IS_TESTING = 7
BATTERY_IS_NUM_OF = 7
BATTERY_CONTROLLED_FROM_REMOTE = 0
BATTERY_CONTROLLED_FROM_LOCAL = 1
BATTERY_CONTROLLED_FROM_NUM_OF = 2
BATTERY_TYPE_ISNOT_APPLICABLE_UNKNOWN = 0
BATTERY_TYPE_ISLEAD_ACID = 1
BATTERY_TYPE_ISNICKEL_METAL_HYDRATE = 2
BATTERY_TYPE_ISNICKEL_CADMIUM = 3
BATTERY_TYPE_ISLITHIUM_ION = 4
BATTERY_TYPE_ISCARBON_ZINC = 5
BATTERY_TYPE_ISZINC_CHLORIDE = 6
BATTERY_TYPE_ISALKALINE = 7
BATTERY_TYPE_ISRECHARGEABLE_ALKALINE = 8
BATTERY_TYPE_ISSODIUM_SULFUR = 9
BATTERY_TYPE_ISFLOW = 10
BATTERY_TYPE_ISOTHER = 99
BATTERY_TYPE_ISNUM_OF = 12
BATTERY_STATE_ISDISCONNECTED = 1
BATTERY_STATE_ISINITIALIZING = 2
BATTERY_STATE_ISCONNECTED = 3
BATTERY_STATE_ISSTANDBY = 4
BATTERY_STATE_ISSOC_PROTECTION = 5
BATTERY_STATE_ISSUSPENDING = 6
BATTERY_STATE_ISFAULT = 99
BATTERY_STATE_ISNUM_OF = 7
USING_BATTERY_1 = 0
USING_BATTERY_2 = 1
USING_BATTERY_3 = 2
USING_BATTERY_4 = 3
USING_BATTERY_NUM_OF = 4
REQUEST_INVERTER_TOSTART = 1
REQUEST_INVERTER_TOSTOP = 2
REQUEST_INVERTER_TONUM_OF = 2
SET_BATTERY_TOCONNECT = 1
SET_BATTERY_TODISCONNECT = 2
SET_BATTERY_TONUM_OF = 2
SET_INVERTER_TOSTOPPED = 1
SET_INVERTER_TOSTANDBY = 2
SET_INVERTER_TOSTARTED = 3
SET_INVERTER_TONUM_OF = 3
FIRMWARE_IN_PRODUCTION_MODE = 0
FIRMWARE_IN_MANUFACTURING_MODE = 1
FIRMWARE_IN_NUM_OF = 2
