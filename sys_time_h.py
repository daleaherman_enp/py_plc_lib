# ---------------------------------------------------------
#
# This file is auto-generated from C header file: sys_time.h 
#
# ---------------------------------------------------------

from embeddedstructs import Struct,Union,Type,Format

# ---------------------------------------------------------
#
# CONSTANTS 
#
# ---------------------------------------------------------


# ---------------------------------------------------------
#
# STRUCTS
#
# ---------------------------------------------------------


#	timeval

class timeval(Struct):
	_format = Format.LittleEndian
	tv_sec = Type.Long
	tv_usec = Type.Long

# ---------------------------------------------------------
#
# CONSTANTS NOT USED IN THIS FILE
#
# ---------------------------------------------------------

_SYS_TIME_H_ = True
