# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 13:33:56 2018

@author: dvt3
"""
from IQ8_PLC_Helpers import enphase_pack_poll_command_search,enphase_unpack_deviceInfo_tlv,unpack_bcd,unpack_chars,unpack_partnum,\
                unpack_millijoules, \
                enphase_pack_v6_telemetry_1,enphase_unpack_v6_telemetry,enphase_pack_v6_fw_info,enphase_unpack_v6_fwinfo,\
                enphase_generic_poll,enphase_pack_v6_start_fw_upgrade,enphase_pack_v6_hunk,\
                enphase_pack_v6_poll_missing_hunks,enphase_unpack_v6_missing_hunks,get_next_missing_hunk,\
                enphase_pack_v6_request_metadata,enphase_unpack_v6_metadata,\
                enphase_pack_v6_write_metadata,enphase_pack_v6_start_DMIR_upgrade
from mcp_mde_image_h import MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE
from PLCTools import plc_channel,output_bytes #,PLCTimeoutException
from mde_ids_h import mde_id_DMIR_finish_and_check
import time

# test
#	idb = Type.UnsignedByte[cPartNumIdLen]
#	manuRev = Type.UnsignedByte
#	dMajor = Type.UnsignedByte
#	dMinor = Type.UnsignedByte
#	dMaint = Type.UnsignedByte

LINE_MEAS_CLOCK_FREQ	 = 50000000.0

    
def main():
    
    ch = plc_channel("COM12")
    
    try:
    
        packet = enphase_pack_poll_command_search()
#        print(packet.hex())
        print(len(packet))
        print("Sending packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
    #    buf = ch.get_buf()
    #    print(len(buf))
    #    print(buf.hex())
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        print("Receive packet..")
        rpacket = ch.recv_packet()
        print("Unpacking...")
        devInfo,tm,status = enphase_unpack_deviceInfo_tlv(rpacket)
#        print(rpacket.hex())
        print(len(rpacket))
        serialNumber = unpack_bcd(devInfo.devInfo.devSerialNum.byteArr)
        print("devSerialNum=",serialNumber)
        print("devPartNum=",unpack_partnum(devInfo.devInfo.devPartNum))
        print("devAssemblyNum=",unpack_partnum(devInfo.devInfo.devAssemblyNum))
        print("asicId=",unpack_bcd(devInfo.devInfo.asicId.asicId)) # = Type.Struct(tAsicId)
        print("pv_name=",unpack_chars(devInfo.devInfo.moduleEntry.pv_name))
        print("pv_model=",unpack_chars(devInfo.devInfo.moduleEntry.pv_model))
        print("pv_manuf=",unpack_chars(devInfo.devInfo.moduleEntry.pv_manuf))
        print("domainAddr=",unpack_bcd(devInfo.devInfo.domainAddr))# = Type.UnsignedByte[cL2DomainAddrLen]
        print("devType",devInfo.devInfo.devType)
        print("modAddr",devInfo.devInfo.modAddr)
        print("grpMask",devInfo.devInfo.grpMask)
        print("rxEmuSsi",devInfo.devInfo.rxEmuSsi)
        print("flags",devInfo.devInfo.flags)
        print(tm)
        print(status)
        
            
        packet = enphase_pack_v6_request_metadata(serialNumber,0x3FFF)
        print(len(packet))
        print("Sending Metadata request packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        
        print("Receive Metadata response packet..")
#        ch.debug=True
        rpacket = ch.recv_packet()
        output_bytes("MetaData",rpacket)
        
        pMeta = enphase_unpack_v6_metadata(rpacket)
        
        print("crc16_meta:",hex(pMeta.meta_data.crc16_meta))
        print("fill_0:",pMeta.meta_data.fill_0)
        print("timeH:",hex(pMeta.meta_data.timeH))
        print("timeL:",hex(pMeta.meta_data.timeL))
        print("arena_bcd:",end="")
        for d in pMeta.meta_data.arena_bcd:
            print(hex(d),end=' ')
        print()
        print("image_type:",pMeta.meta_data.image_type)
        print("compatibility:",pMeta.meta_data.compatibility)
        print("fill_1:",pMeta.meta_data.fill_1)
        print("md5_hash:",end='')#,pMeta.meta_data.md5_hash)
        for d in pMeta.meta_data.md5_hash:
            print(hex(d),end=' ')
        print()
        print("data_length:",pMeta.meta_data.data_length)
        print("crc16_data:",hex(pMeta.meta_data.crc16_data))
        print("fill_2:",pMeta.meta_data.fill_2)
        
#        raise Exception("As far as I go")
        
#        packet1 = enphase_pack_v6_telemetry_1(devInfo.devInfo.devSerialNum.byteArr,0x3FFF)
        packet = enphase_generic_poll(serialNumber,0x3FFF,14)
        
#        packet = enphase_pack_poll_request_data_tlv(None, None, cMcpPollFlagInterval,devInfo.devInfo.devSerialNum)
#        print("v6_telemetry_1:\n",packet1.hex())
#        print(len(packet1))
#        print("v6_generic:\n",packet.hex())
        print(len(packet))
        print("Sending packet...")
#        raise Exception("Done!")
#        ch.debug = True
        print("Sent",ch.send_packet(packet),"bytes...")
    #    buf = ch.get_buf()
    #    print(len(buf))
    #    print(buf.hex())
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        print("Receive packet..")
#        ch.debug=True
        rpacket = ch.recv_packet()
        print("Unpacking...")
        pRspIntervalTlv = enphase_unpack_v6_telemetry(rpacket)
        print("telemetry_version:",pRspIntervalTlv.telemetry_version)
        print("Temp=",pRspIntervalTlv.telemetry_level1.Temperature/4.0)
        print("A=",pRspIntervalTlv.telemetry_level1.A)
        print("V=",pRspIntervalTlv.telemetry_level1.V)
        print("W=",pRspIntervalTlv.telemetry_level1.W)
        print("Hz=",pRspIntervalTlv.telemetry_level1.Hz)
        print("VA=",pRspIntervalTlv.telemetry_level1.VA)
        print("VAr=",pRspIntervalTlv.telemetry_level1.VAr)
        print("true_pf=",pRspIntervalTlv.telemetry_level1.PF.true_pf)
        print("displacement=",pRspIntervalTlv.telemetry_level1.PF.displacement)
        print("distortion=",pRspIntervalTlv.telemetry_level1.PF.distortion)
        print("DCA=",pRspIntervalTlv.telemetry_level1.DCA)
        print("DCV=",pRspIntervalTlv.telemetry_level1.DCV/256.0)
        print("DCW=",pRspIntervalTlv.telemetry_level1.DCW)
        print("Operating_State=",pRspIntervalTlv.telemetry_level1.Operating_State)
        print("Enphase_Operating_State=",pRspIntervalTlv.telemetry_level1.Enphase_Operating_State)
        
        packet = enphase_pack_v6_fw_info(serialNumber,0x3FFF)
#        print("v6_fw_info:\n",packet.hex())
        print(len(packet))
        print("Sending packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
    #    buf = ch.get_buf()
    #    print(len(buf))
    #    print(buf.hex())
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        print("Receive packet..")
#        ch.debug=True
        rpacket = ch.recv_packet()
        pRspFwInfo = enphase_unpack_v6_fwinfo(rpacket)
        print("\nfw_bank0_crc_is_bad_b:",pRspFwInfo.fw_bank0_crc_is_bad_b)
        print("fw_bank1_crc_is_bad_b:",pRspFwInfo.fw_bank1_crc_is_bad_b)
        print("fw_bank0_is_selected_b:",pRspFwInfo.fw_bank0_is_selected_b)
        print("fw_bank1_is_selected_b:",pRspFwInfo.fw_bank1_is_selected_b)
        print("\nActive bank:")
        print("SCM_hash:",pRspFwInfo.active_bank.SCM_hash.bytes)
        print("has_mods:",pRspFwInfo.active_bank.has_mods)
        print("arena_info.part_number:",pRspFwInfo.active_bank.arena_info.part_number.bcd)
        print("arena_info.revision:",pRspFwInfo.active_bank.arena_info.revision)
        print("arena_info.version.major:",pRspFwInfo.active_bank.arena_info.version.major)
        print("arena_info.version.model:",pRspFwInfo.active_bank.arena_info.version.model)
        print("arena_info.version.revision:",pRspFwInfo.active_bank.arena_info.version.revision)
        print("\nInactive bank:")
        print("SCM_hash:",pRspFwInfo.inactive_bank.SCM_hash.bytes)
        print("has_mods:",pRspFwInfo.inactive_bank.has_mods)
        print("arena_info.part_number:",pRspFwInfo.inactive_bank.arena_info.part_number.bcd)
        print("arena_info.revision:",pRspFwInfo.inactive_bank.arena_info.revision)
        print("arena_info.version.major:",pRspFwInfo.inactive_bank.arena_info.version.major)
        print("arena_info.version.model:",pRspFwInfo.inactive_bank.arena_info.version.model)
        print("arena_info.version.revision:",pRspFwInfo.inactive_bank.arena_info.version.revision)
        
# --------------------- DMIR Upload
        
        
        dmir_file_name = "dmir-sunspec-60Hz_240V-p540-00000-r00-a3bd1d5-mods.tch"
        
        ans = input("Enter C to start DMIR upload: ")
        if ans.upper() == 'E':
            return

        print("Attempting upload of DMIR in file",dmir_file_name)

        with open(dmir_file_name) as fwp:
            lines = fwp.readlines()
            bcd_data = ''
            for line in lines:
                if line.strip().startswith("#"):
                    continue
                bcd_data += line.replace("\n","")
            
            
        missing_hunks = None
        for retries in range(5):
        
            packet, missing_hunks = enphase_pack_v6_start_DMIR_upgrade(bcd_data,21,missing_hunks)
            print(len(packet))
            print("Sending Start Upgrade packet...")
            print("Sent",ch.send_packet(packet),"bytes...")
    
            print("Receive Start Upgrade ACK..")
            rpacket = ch.recv_packet()
            
            time.sleep(2)
            
#            input("Press C to continue ")
            
            seq = -1
            while True:
                seq = get_next_missing_hunk(missing_hunks,seq)
                if seq < 0:
                    break
                packet = enphase_pack_v6_hunk(bcd_data,seq)
#                print(len(packet))
                print("Retry:",retries,"Sending Hunk packet #",seq)
                print("Sent",ch.send_packet(packet),"bytes...")
                print("Receive Write Hunk ACK..for #",seq)
                rpacket = ch.recv_packet()
#                input("Press C to continue")
        
         
                
            missing_hunks = None
            
            packet = enphase_pack_v6_poll_missing_hunks(serialNumber,0x3FFF,mde_id=mde_id_DMIR_finish_and_check)
            print(len(packet))
            print("Sending Poll Missing packets...")
            print("Sent",ch.send_packet(packet),"bytes...")
            print("Receive Missing Hunks Poll ACK..")
            rpacket = ch.recv_packet()
            
            print("Receive Missing Hunks response packet..")
    #        ch.debug=True
            rpacket = ch.recv_packet()
            output_bytes("Missing hunks",rpacket)
            missing_hunks = enphase_unpack_v6_missing_hunks(rpacket,missing_hunks)
            if missing_hunks is None:
                break
        

        
        
# --------------------- FW Upload        
        
        fw_file_name = "procload-IQ8-3035da7b-mods.swe"
        
        ans = input("Enter C to start FW upload: ")
        if ans.upper() == 'E':
            return
        
        print("Attempting upload of FW in file",fw_file_name)
        
 #        if True:
        
        with open(fw_file_name) as fwp:
            bcd_data = fwp.read().replace("\n","")
            
        missing_hunks = None
        for retries in range(5):
        
            packet, missing_hunks = enphase_pack_v6_start_fw_upgrade(bcd_data,missing_hunks)
            print(len(packet))
            print("Sending Start Upgrade packet...")
            print("Sent",ch.send_packet(packet),"bytes...")
    
            print("Receive Start Upgrade ACK..")
            rpacket = ch.recv_packet()
            
            time.sleep(2)
            
#            input("Press C to continue ")
            
            seq = -1
            while True:
                seq = get_next_missing_hunk(missing_hunks,seq)
                if seq < 0:
                    break
                packet = enphase_pack_v6_hunk(bcd_data,seq)
#                print(len(packet))
                print("Retry:",retries,"Sending Hunk packet #",seq)
                print("Sent",ch.send_packet(packet),"bytes...")
                print("Receive Write Hunk ACK..for #",seq)
                rpacket = ch.recv_packet()
#                input("Press C to continue")
        
         
                
            missing_hunks = None
            
            packet = enphase_pack_v6_poll_missing_hunks(serialNumber,0x3FFF)
            print(len(packet))
            print("Sending Poll Missing packets...")
            print("Sent",ch.send_packet(packet),"bytes...")
            print("Receive Missing Hunks Poll ACK..")
            rpacket = ch.recv_packet()
            
            print("Receive Missing Hunks response packet..")
    #        ch.debug=True
            rpacket = ch.recv_packet()
            output_bytes("Missing hunks",rpacket)
            missing_hunks = enphase_unpack_v6_missing_hunks(rpacket,missing_hunks)
            if missing_hunks is None:
                break
        
# ----------------------  METADATA Upload        

        md_file_name = "procload-IQ8-3035da7b-mods-metadata.tch"
        
        ans = input("Enter C to start MetaData upload: ")
        if ans.upper() == 'E':
            return
        print("Attempting upload of MetaData in file",md_file_name)

        with open(md_file_name) as fwp:
            lines = fwp.readlines()
            bcd_data = ''
            for line in lines:
                if line.strip().startswith("#"):
                    continue
                bcd_data += line.replace("\n","")
            
            
        packet = enphase_pack_v6_write_metadata(bcd_data)
        print(len(packet))
        output_bytes("MetaData SENT",packet)
        print("Sending Metadata Upgrade packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
        print("Receive Metadata Upgrade ACK..")
        rpacket = ch.recv_packet()
        
        packet = enphase_pack_v6_request_metadata(serialNumber,0x3FFF,bank=MDE_BANK_SELECT_FIRMWARE_BANK_INACTIVE)
        print(len(packet))
        print("Sending Metadata request packet...")
        print("Sent",ch.send_packet(packet),"bytes...")
        print("Receive ACK..")
        rpacket = ch.recv_packet()
        
        print("Receive Metadata response packet..")
#        ch.debug=True
        rpacket = ch.recv_packet()
        output_bytes("MetaData",rpacket)
        
        pMeta = enphase_unpack_v6_metadata(rpacket)
        
        print("crc16_meta:",hex(pMeta.meta_data.crc16_meta))
        print("fill_0:",pMeta.meta_data.fill_0)
        print("timeH:",hex(pMeta.meta_data.timeH))
        print("timeL:",hex(pMeta.meta_data.timeL))
        print("arena_bcd:",end="")
        for d in pMeta.meta_data.arena_bcd:
            print(hex(d),end=' ')
        print()
        print("image_type:",pMeta.meta_data.image_type)
        print("compatibility:",pMeta.meta_data.compatibility)
        print("fill_1:",pMeta.meta_data.fill_1)
        print("md5_hash:",end='')#,pMeta.meta_data.md5_hash)
        for d in pMeta.meta_data.md5_hash:
            print(hex(d),end=' ')
        print()
        print("data_length:",pMeta.meta_data.data_length)
        print("crc16_data:",hex(pMeta.meta_data.crc16_data))
        print("fill_2:",pMeta.meta_data.fill_2)
#        
        
#        missing_hunks = None
#        for retries in range(5):
#        
#            packet, missing_hunks = enphase_pack_v6_start_fw_upgrade(bcd_data,missing_hunks)
#            print(len(packet))
#            print("Sending Start Upgrade packet...")
#            print("Sent",ch.send_packet(packet),"bytes...")
#    
#            print("Receive Start Upgrade ACK..")
#            rpacket = ch.recv_packet()
#            
#            time.sleep(2)
#            
##            input("Press C to continue ")
#            
#            seq = -1
#            while True:
#                seq = get_next_missing_hunk(missing_hunks,seq)
#                if seq < 0:
#                    break
#                packet = enphase_pack_v6_hunk(bcd_data,seq)
##                print(len(packet))
#                print("Retry:",retries,"Sending Hunk packet #",seq)
#                print("Sent",ch.send_packet(packet),"bytes...")
#                print("Receive Write Hunk ACK..for #",seq)
#                rpacket = ch.recv_packet()
##                input("Press C to continue")
#        
#         
#                
#            missing_hunks = None
#            
#            packet = enphase_pack_v6_poll_missing_hunks(devInfo.devInfo.devSerialNum.byteArr,0x3FFF)
#            print(len(packet))
#            print("Sending Poll Missing packets...")
#            print("Sent",ch.send_packet(packet),"bytes...")
#            print("Receive Missing Hunks Poll ACK..")
#            rpacket = ch.recv_packet()
#            
#            print("Receive Missing Hunks response packet..")
#    #        ch.debug=True
#            rpacket = ch.recv_packet()
#            output_bytes("Missing hunks",rpacket)
#            missing_hunks = enphase_unpack_v6_missing_hunks(rpacket,missing_hunks)
#            if missing_hunks is None:
#                break

        

        
    except Exception as ex:
        raise ex
    finally:
        ch.close()
        

if __name__ == "__main__":
    main()

